/* begin dune-ash */

/* Define to the version of dune-ash */
#define DUNE_ASH_VERSION "${DUNE_ASH_VERSION}"

/* Define to the major version of dune-ash */
#define DUNE_ASH_VERSION_MAJOR ${DUNE_ASH_VERSION_MAJOR}

/* Define to the minor version of dune-ash */
#define DUNE_ASH_VERSION_MINOR ${DUNE_ASH_VERSION_MINOR}

/* Define to the revision of dune-ash */
#define DUNE_ASH_VERSION_REVISION ${DUNE_ASH_VERSION_REVISION}

/* end dune-ash */
