find_package( OpenGL REQUIRED )
include_directories( ${OPENGL_INCLUDE_DIR} )
list( INSERT DUNE_DEFAULT_LIBS 0 ${OPENGL_LIBRARY} )

find_package( SDL REQUIRED )
include_directories( ${SDL_INCLUDE_DIR} )
list( INSERT DUNE_DEFAULT_LIBS 0 ${SDL_LIBRARY} )

find_package( SDL_image REQUIRED )
include_directories( ${SDL_IMAGE_INCLUDE_DIRS} )
list( INSERT DUNE_DEFAULT_LIBS 0 ${SDL_IMAGE_LIBRARIES} )

find_package( SDL_ttf REQUIRED )
include_directories( ${SDL_TTF_INCLUDE_DIRS} )
list( INSERT DUNE_DEFAULT_LIBS 0 ${SDL_TTF_LIBRARIES} )
