AC_DEFUN([DUNE_SDL_IMAGE_PATH],[
  AC_PREREQ([2.5.0])
  AC_LANG_PUSH([C++])

  AC_ARG_WITH([sdl_image], AC_HELP_STRING([--with-sdl_image=PATH], [directory containing libSDL_image]))

  HAVE_SDL_IMAGE="no"
  SDL_IMAGE_SUMMARY="no"

  ac_save_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
  AS_IF([test "x$with_sdl_image" != "x"],
        [PKG_CONFIG_PATH="$with_sdl_image:$with_sdl_image/lib/pkgconfig:$PKG_CONFIG_PATH"])
  AC_MSG_CHECKING([for SDL_image])
  AS_IF([PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --exists SDL_image],[
    HAVE_SDL_IMAGE="yes"
    SDL_IMAGE_VERSION="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --modversion SDL_image`"
    SDL_IMAGE_CPPFLAGS="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --cflags SDL_image` -DENABLE_SDL_IMAGE=1"
    SDL_IMAGE_LIBS="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --libs SDL_image`"
  ])
  AC_MSG_RESULT([$HAVE_SDL_IMAGE])
  PKG_CONFIG_PATH="$ac_save_PKG_CONFIG_PATH"

  AS_IF([test "$HAVE_SDL_IMAGE" != "no"],[
    AC_DEFINE([HAVE_SDL_IMAGE], [ENABLE_SDL_IMAGE], [Was SDL_image found and SDL_IMAGE_CPPFLAGS used?])
    AC_SUBST([SDL_IMAGE_CPPFLAGS])
    AC_SUBST([SDL_IMAGE_LIBS])
    DUNE_ADD_ALL_PKG([SDL_image], [\${SDL_IMAGE_CPPFLAGS}], [], [\${SDL_IMAGE_LIBS}])
    SDL_IMAGE_SUMMARY="version $SDL_IMAGE_VERSION"
  ])
  AM_CONDITIONAL([SDL_IMAGE], [test "$HAVE_SDL_IMAGE" != "no"])

  DUNE_ADD_SUMMARY_ENTRY([SDL_IMAGE], [$SDL_IMAGE_SUMMARY])

  AC_LANG_POP()
])
