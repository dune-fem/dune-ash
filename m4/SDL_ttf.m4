AC_DEFUN([DUNE_SDL_TTF_PATH],[
  AC_PREREQ([2.5.0])
  AC_LANG_PUSH([C++])

  AC_ARG_WITH([sdl_ttf], AC_HELP_STRING([--with-sdl_ttf=PATH], [directory containing libSDL_ttf]))

  HAVE_SDL_TTF="no"
  SDL_TTF_SUMMARY="no"

  ac_save_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
  AS_IF([test "x$with_sdl_ttf" != "x"],
        [PKG_CONFIG_PATH="$with_sdl_ttf:$with_sdl_ttf/lib/pkgconfig:$PKG_CONFIG_PATH"])
  AC_MSG_CHECKING([for SDL_ttf])
  AS_IF([PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --exists SDL_ttf],[
    HAVE_SDL_TTF="yes"
    SDL_TTF_VERSION="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --modversion SDL_ttf`"
    SDL_TTF_CPPFLAGS="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --cflags SDL_ttf` -DENABLE_SDL_TTF=1"
    SDL_TTF_LIBS="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --libs SDL_ttf`"
  ])
  AC_MSG_RESULT([$HAVE_SDL_TTF])
  PKG_CONFIG_PATH="$ac_save_PKG_CONFIG_PATH"

  AS_IF([test "$HAVE_SDL_TTF" != "no"],[
    AC_DEFINE([HAVE_SDL_TTF], [ENABLE_SDL_TTF], [Was SDL_TTF found and SDL_TTF_CPPFLAGS used?])
    AC_SUBST([SDL_TTF_CPPFLAGS])
    AC_SUBST([SDL_TTF_LIBS])
    DUNE_ADD_ALL_PKG([SDL_TTF], [\${SDL_TTF_CPPFLAGS}], [], [\${SDL_TTF_LIBS}])
    SDL_TTF_SUMMARY="version $SDL_TTF_VERSION"
  ])
  AM_CONDITIONAL([SDL_TTF], [test "$HAVE_SDL_TTF" != "no"])

  DUNE_ADD_SUMMARY_ENTRY([SDL_TTF], [$SDL_TTF_SUMMARY])

  AC_LANG_POP()
])
