AC_DEFUN([DUNE_SDL_PATH],[
  AC_PREREQ([2.5.0])
  AC_LANG_PUSH([C++])

  AC_ARG_WITH([sdl], AC_HELP_STRING([--with-sdl=PATH], [directory containing SDL]))

  HAVE_SDL="no"
  SDL_SUMMARY="no"

  ac_save_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
  AS_IF([test "x$with_sdl" != "x"],
        [PKG_CONFIG_PATH="$with_sdl:$with_sdl/lib/pkgconfig:$PKG_CONFIG_PATH"])
  AC_MSG_CHECKING([for SDL])
  AS_IF([PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --exists sdl],[
    HAVE_SDL="yes"
    SDL_VERSION="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --modversion sdl`"
    SDL_CPPFLAGS="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --cflags sdl` -DENABLE_SDL=1"
    SDL_LIBS="`PKG_CONFIG_PATH=$PKG_CONFIG_PATH pkg-config --libs sdl`"
  ])
  AC_MSG_RESULT([$HAVE_SDL])
  PKG_CONFIG_PATH="$ac_save_PKG_CONFIG_PATH"

  AS_IF([test "$HAVE_SDL" != "no"],[
    AC_DEFINE([HAVE_SDL], [ENABLE_SDL], [Was the SDL found and SDL_CPPFLAGS used?])
    AC_SUBST([SDL_CPPFLAGS])
    AC_SUBST([SDL_LIBS])
    DUNE_ADD_ALL_PKG([SDL], [\${SDL_CPPFLAGS}], [], [\${SDL_LIBS}])
    SDL_SUMMARY="version $SDL_VERSION"
  ])
  AM_CONDITIONAL([SDL], [test "$HAVE_SDL" != "no"])

  DUNE_ADD_SUMMARY_ENTRY([SDL], [$SDL_SUMMARY])

  AC_LANG_POP()
])
