dnl -*- autoconf -*-
# Macros needed to find dune-ash and dependent libraries.  They are called by
# the macros in ${top_src_dir}/dependencies.m4, which is generated by
# "dunecontrol autogen"

# Additional checks needed to build dune-ash
# This macro should be invoked by every module which depends on dune-ash, as
# well as by dune-ash itself
AC_DEFUN([DUNE_ASH_CHECKS],[
  AC_REQUIRE([DUNE_PATH_OPENGL])
  AC_REQUIRE([DUNE_SDL_PATH])
  AC_REQUIRE([DUNE_SDL_IMAGE_PATH])
  AC_REQUIRE([DUNE_SDL_TTF_PATH])
  AC_REQUIRE([DUNE_GLU_PATH])
  AC_REQUIRE([DUNE_ASH_CHECK_GLCLEAR])
])

# Additional checks needed to find dune-ash
# This macro should be invoked by every module which depends on dune-ash, but
# not by dune-ash itself
AC_DEFUN([DUNE_ASH_CHECK_MODULE],
[
  DUNE_CHECK_MODULES([dune-ash],[ash/widget/widget.hh])
])
