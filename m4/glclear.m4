AC_DEFUN([DUNE_ASH_CHECK_GLCLEAR],[
  AC_ARG_ENABLE([glclear],
    AC_HELP_STRING([--disable-glclear], [draw rectangles instead of using glClear]))
  AS_IF([ test "x$enable_glclear" == "xno" ],[
    AC_DEFINE([DUNE_ASH_GLCLEAR], [0], [Allow use of glClear?])
  ],[
    AC_DEFINE([DUNE_ASH_GLCLEAR], [1], [Allow use of glClear?])
  ])
])
