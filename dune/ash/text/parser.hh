#ifndef DUNE_ASH_TEXT_PARSER_HH
#define DUNE_ASH_TEXT_PARSER_HH

#include <iostream>
#include <list>
#include <map>
#include <vector>

#include <dune/common/exceptions.hh>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/text/box.hh>

namespace Dune
{

  namespace Ash
  {

    class TextParser
    {
      typedef TextParser This;

      struct CommandInterface;

      struct DefCommand;            // \def{macro}{substitution}
      struct UndefCommand;          // \undef{macro}
      struct IfDefCommand;          // \ifdef{macro}{then} or \ifdef{macro}{then}{else}
      struct IfEqCommand;           // \ifeq{macro}{macro}{then} or \ifeq{macro}{macro}{then}{else}
      struct AdvanceCommand;        // \advance{macro}
      struct InputCommand;          // \input{filename}

      struct NewLineCommand;        // \newline (or \\)
      struct HorizontalFillCommand; // \hfill
      struct ImageCommand;          // \image{width}{height}{filename}
      struct ButtonCommand;         // \button{eventId}{content}
      struct GlueCommand;           // \glue{content}
      struct MBoxCommand;           // \mbox{content}
      struct AbsoluteCommand;       // \absolute{x}{y}{content}
      struct ParBoxCommand;         // \parbox{width}{content}
      struct ColorCommand;          // \color{literal} or \color{r,g,b}
      struct FontCommand;           // \font{filename}{size}
      struct BoldCommand;           // \bf
      struct ItalicCommand;         // \it
      struct LeadingCommand;        // \leading{factor}

      typedef std::map< std::string, CommandInterface * > CommandMap;

    public:
      typedef std::list< BoxInterface * > BoxList;
      typedef std::map< std::string, std::string > MacroMap;

      TextParser ();
      explicit TextParser ( const std::string &textPath );

    private:
      // prohibit copying and assignment
      TextParser ( const This & );
      const This &operator= ( const This & );

    public:
      ~TextParser ();

      BoxList operator() ( std::istream &input ) const;
      BoxList operator() ( std::istream &input, MacroMap &macros ) const;
      void operator() ( std::istream &input, MacroMap &macros, BoxList &boxes ) const;

    private:
      template< class T >
      static T parseValue ( const std::string &s )
      {
        T value;
        if( !Fem::ParameterParser< T >::parse( s, value ) )
          DUNE_THROW( InvalidStateException, "Invalid Parameter (" << s << ")." );
        return value;
      }

      void setupCommands ();

      static int parseCommand ( std::istream &input, std::string &command, std::vector< std::string > &args );

      std::string textPath ( const std::string &fileName ) const
      {
        return (textPath_.empty() ? fileName : textPath_ + "/" + fileName);
      }

      std::string textPath_;
      CommandMap commands_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_TEXT_PARSER_HH
