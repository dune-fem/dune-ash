#ifndef DUNE_ASH_TEXT_FONTMANAGER_HH
#define DUNE_ASH_TEXT_FONTMANAGER_HH

#include <map>
#include <stack>

#include <dune/common/fvector.hh>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/stringvector.hh>
#include <dune/ash/text/truetypefont.hh>

namespace Dune
{

  namespace Ash
  {

    // FontManager
    // -----------

    struct FontManager
    {
      typedef FieldVector< GLfloat, 4 > Color;
      typedef std::pair< std::string, int > FontId;

    private:
      struct FontState
      {
        FontState( TrueTypeFont *font, const Color &color, int style, GLfloat leading = 0.0f )
        : font_( font ),
          color_( color ),
          style_( style ),
          leading_( leading )
        {}

        TrueTypeFont *font_;
        Color color_;
        int style_;
        GLfloat leading_;
      };

      typedef std::stack< FontState > FontStateStack;
      typedef std::map< FontId, TrueTypeFont * > FontMap;

    public:
      explicit FontManager ( const FieldVector< GLfloat, 2 > &dpu )
      : dpu_( dpu )
      {
        setup();
      }

      explicit FontManager ( GLfloat dpu = 96.0f )
      : dpu_( { dpu, dpu } )
      {
        setup();
      }

      ~FontManager ()
      {
        for( FontMap::iterator it = fontMap_.begin(); it != fontMap_.end(); ++it )
          delete it->second;
      }

      GLfloat space () const { return GLfloat( space_ ) / dotsPerUnit()[ 0 ]; }
      GLfloat lineHeight () const { return GLfloat( lineHeight_ ) / dotsPerUnit()[ 1 ]; }

      const FieldVector< GLfloat, 2 > &dotsPerUnit () const { return dpu_; }

      const Color &color () const { return stateStack_.top().color_; }
      TrueTypeFont &font () const { assert( stateStack_.top().font_ ); return *(stateStack_.top().font_); }
      GLfloat leading () { return stateStack_.top().leading_; }
      const int getStyle () const { return stateStack_.top().style_; }

      void setColor ( const Color &color ) { stateStack_.top().color_ = color; }
      void setFont ( const FontId &fontId );
      void setLeading ( const GLfloat factor ) { stateStack_.top().leading_ = factor * lineHeight(); }
      void setStyle ( int style ) { stateStack_.top().style_ = style; }

      void push ();
      void pop ();
      void reset ();

      static FontId defaultFont ();
      static std::vector< std::string > fontPaths () { return Dune::Fem::Parameter::getValue< StringVector >( "ash.fontpaths" ); } 

      static TrueTypeFont *loadFont ( const std::string &font, int ptSize,
                                      const std::vector< std::string > &paths = fontPaths() );
      static TrueTypeFont *loadFont ( const FontId &fontId, const std::vector< std::string > &paths = fontPaths() );

    private:
      // prohibit copying and assignment
      FontManager ( const FontManager & );
      const FontManager &operator= ( const FontManager & );

      void setup ();

      FontMap fontMap_;
      FontStateStack stateStack_;
      TrueTypeFont *defaultFont_;      
      int space_;
      int lineHeight_;
      FieldVector< GLfloat, 2 > dpu_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_TEXT_FONTMANAGER_HH
