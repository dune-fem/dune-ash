#include <sstream>

#include <config.h>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/text/parser.hh>

namespace Dune
{

  namespace Ash
  {

    struct TextParser::CommandInterface
    {
      virtual ~CommandInterface () {};
      virtual void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes ) = 0;

      void checkNumArgs ( const std::vector< std::string > &args, std::size_t expected )
      {
        if( args.size() != expected )
          DUNE_THROW( InvalidStateException, "Wrong number of arguments (" << args.size() << ", should be " << expected << ")." );
      }
    };



    // \def{macro}{substitution}
    struct TextParser::DefCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 2 );
        macros[ args[ 0 ] ] = args[ 1 ];
      }
    };



    // \undef{macro}
    struct TextParser::UndefCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 1 );
        macros.erase( args[ 0 ] );
      }
    };



    // \ifdef{macro}{then} or \ifdef{macro}{then}{else}
    struct TextParser::IfDefCommand
    : public CommandInterface
    {
      explicit IfDefCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        if( (args.size() < 2) || (args.size() > 3) )
          DUNE_THROW( InvalidStateException, "Wrong number of arguments (" << args.size() << ")." );
        if( macros.find( args[ 0 ] ) != macros.end() )
        {
          std::istringstream input( args[ 1 ] );
          parser_( input, macros, boxes );
        }
        else if( args.size() == 3 )
        {
          std::istringstream input( args[ 2 ] );
          parser_( input, macros, boxes );
        }
      }

    private:
      const TextParser &parser_;
    };


    // \ifeq{macro}{macro}{then} or \ifeq{macro}{macro}{then}{else}
    struct TextParser::IfEqCommand
    : public CommandInterface
    {
      explicit IfEqCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        if( (args.size() < 3) || (args.size() > 4) )
          DUNE_THROW( InvalidStateException, "Wrong number of arguments (" << args.size() << ")." );

        const MacroMap::const_iterator posA = macros.find( args[ 0 ] );
        if( posA == macros.end() )
          DUNE_THROW( InvalidStateException, "Macro " << args[ 0 ] << " not defined." );
        const MacroMap::const_iterator posB = macros.find( args[ 1 ] );
        if( posB == macros.end() )
          DUNE_THROW( InvalidStateException, "Macro " << args[ 1 ] << " not defined." );

        if( posA->second == posB->second )
        {
          std::istringstream input( args[ 2 ] );
          parser_( input, macros, boxes );
        }
        else if( args.size() == 4 )
        {
          std::istringstream input( args[ 3 ] );
          parser_( input, macros, boxes );
        }
      }

    private:
      const TextParser &parser_;
    };



    // \advance{macro}
    struct TextParser::AdvanceCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 1 );
        const MacroMap::iterator pos = macros.find( args[ 0 ] );
        if( pos == macros.end() )
          DUNE_THROW( InvalidStateException, "Macro " << args[ 0 ] << " not defined." );

        std::ostringstream out;
        out << (parseValue< int >( pos->second ) + 1);
        pos->second = out.str();
      }
    };



    // \input{filename}
    struct TextParser::InputCommand
    : public CommandInterface
    {
      explicit InputCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 1 );
        const std::string inputName = parser_.textPath( args[ 0 ] );
        std::ifstream input( inputName.c_str() );
        if( input )
          parser_( input, macros, boxes );
        else
          std::cout << "Error: Unable to open input '" << inputName << "'." << std::endl;
      }

    private:
      const TextParser &parser_;
    };


    // \newline (or \\)
    struct TextParser::NewLineCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 0 );
        boxes.push_back( nullptr );
      }
    };



    // \hfill
    struct TextParser::HorizontalFillCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 0 );
        boxes.push_back( new HorizontalFillBox() );
      }
    };


    // \image{width}{height}{filename}
    struct TextParser::ImageCommand
    : public CommandInterface
    {
      explicit ImageCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 3 );
        const GLfloat width = parseValue< GLfloat >( args[ 0 ] );
        const GLfloat height = parseValue< GLfloat >( args[ 1 ] );
        const std::string fileName = parser_.textPath( args[ 2 ] );
        boxes.push_back( new ImageBox( width, height, fileName ) );
      }

    private:
      const TextParser &parser_;
    };



    // \glue{content}
    struct TextParser::GlueCommand
    : public CommandInterface
    {
      explicit GlueCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 1 );
        std::istringstream input( args[ 0 ] );
        boxes.push_back( new GlueBox( parser_( input, macros ) ) );
      }

    private:
      const TextParser &parser_;
    };



    // \mbox{content}
    struct TextParser::MBoxCommand
    : public CommandInterface
    {
      explicit MBoxCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 1 );
        std::istringstream input( args[ 0 ] );
        boxes.push_back( new MBox( parser_( input, macros ) ) );
      }

    private:
      const TextParser &parser_;
    };



    // \absolute{x}{y}{content}
    struct TextParser::AbsoluteCommand
    : public CommandInterface
    {
      explicit AbsoluteCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        if( (args.size() < 3) || (args.size() > 4) )
          DUNE_THROW( InvalidStateException, "Wrong number of arguments (" << args.size() << ")." );
        const std::size_t i = (args.size() - 3);

        const GLfloat x = parseValue< GLfloat >( args[ i+0 ] );
        const GLfloat y = parseValue< GLfloat >( args[ i+1 ] );
        std::istringstream input( args[ i+2 ] );

        if( i > 0 )
        {
          if( args[ 0 ].length() == 2 )
            boxes.push_back( new AbsoluteBox( { x, y }, parser_( input, macros ), args[ 0 ][ 0 ], args[ 0 ][ 1 ] ) );
          else
            DUNE_THROW( InvalidStateException, "Invalid relative position: '" << args[ 0 ] << "'." );
        }
        else
          boxes.push_back( new AbsoluteBox( { x, y }, parser_( input, macros ) ) );
      }

    private:
      const TextParser &parser_;
    };



    // \parbox{width}{content}
    struct TextParser::ParBoxCommand
    : public CommandInterface
    {
      explicit ParBoxCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 2 );
        const GLfloat width = parseValue< GLfloat >( args[ 0 ] );
        std::istringstream input( args[ 1 ] );
        boxes.push_back( new ParBox( width, parser_( input, macros ) ) );
      }

    private:
      const TextParser &parser_;
    };



    // \button{eventId}{content}
    struct TextParser::ButtonCommand
    : public CommandInterface
    {
      explicit ButtonCommand ( const TextParser &parser )
      : parser_( parser )
      {}

      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 2 );
        const std::string eventId = args[ 0 ];
        std::istringstream input( args[ 1 ] );
        // encapsulate the button content in a mbox to get something on the screen 
        boxes.push_back( new ButtonBox( parser_( input, macros ), eventId ) );
      }

    private:
      const TextParser &parser_;
    };



    // \color{literal} or \color{r,g,b}
    struct TextParser::ColorCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 1 );
        std::istringstream ss( args[ 0 ] );
        std::vector< std::string > values;
        std::string item;
        while( std::getline( ss, item, ',' ) )
          values.push_back( item );

        if ( values.size() == 1 )
          boxes.push_back( new ColorBox( values[ 0 ] ) );
        else if ( values.size() == 3 )
        {
          ColorBox::Color color;
          for( int i = 0; i < 3; ++i )
            color[ i ] = parseValue< GLfloat >( values[ i ] );
          color[ 3 ] = 1.0f;
          boxes.push_back( new ColorBox( color ) );
        }
        else
          DUNE_THROW( InvalidStateException, "Invalid Number of Arguements" );
      }
    };



    // \font{filename}{size}
    struct TextParser::FontCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 2 );
        boxes.push_back( new FontBox( args[ 0 ], parseValue< int >( args[ 1 ] ) ) );
      }
    };


    // \newline (or \\)
    struct TextParser::BoldCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 0 );
        boxes.push_back( new BoldBox() );
      }
    };



    // \newline (or \\)
    struct TextParser::ItalicCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 0 );
        boxes.push_back( new ItalicBox() );
      }
    };

    

    // \leading{factor}
    struct TextParser::LeadingCommand
    : public CommandInterface
    {
      void execute ( const std::vector< std::string > &args, MacroMap &macros, BoxList &boxes )
      {
        checkNumArgs( args, 1 );
        boxes.push_back( new LeadingBox( parseValue< GLfloat >( args[ 0 ] ) ) );
      }
    };



    // Implementation of TextParser
    // ----------------------------

    TextParser::TextParser ()
    : textPath_( "" )
    {
      setupCommands();
    }

    TextParser::TextParser ( const std::string &textPath )
    : textPath_( textPath )
    {
      setupCommands();
    }


    TextParser::~TextParser ()
    {
      for( CommandMap::iterator it = commands_.begin(); it != commands_.end(); ++it )
        delete it->second;
    }


    TextParser::BoxList TextParser::operator() ( std::istream &input ) const
    {
      MacroMap macros;
      return (*this)( input, macros );
    }


    TextParser::BoxList TextParser::operator() ( std::istream &input, MacroMap &macros ) const
    {
      BoxList boxes;
      (*this)( input, macros, boxes );
      return boxes;
    }


    void TextParser::operator() ( std::istream &input, MacroMap &macros, BoxList &boxes ) const
    {
      std::string command, text;
      std::vector< std::string > args;

      int c = input.get();      
      while( input )
      {
        if( (c == ' ') || (c == '\t') || (c == '\n') || (c == '\r') || (c == '\\') )
        {
          // whitespace (or command) -> next box
          if( !text.empty() )
          {
            boxes.push_back( new TextBox( text ) );
            text.clear();
          }

          if( c == '\\' )
          {
            c = parseCommand( input, command, args );

            CommandMap::const_iterator pos = commands_.find( command );
            if( pos != commands_.end() )
              pos->second->execute( args, macros, boxes );
            else
            {
              MacroMap::const_iterator pos = macros.find( command );
              if( pos == macros.end() )
                DUNE_THROW( InvalidStateException, "Invalid command '" << command << "' encountered." );

              if( args.size() > 10 )
                std::cerr << "Warning: Macro " << command << " called with too many arguments." << std::endl;
              std::string body;
              const std::size_t macroLength = pos->second.length();
              for( std::size_t begin = 0; begin < macroLength; )
              {
                std::size_t end = pos->second.find_first_of( '#', begin );
                if( end != pos->second.npos )
                {
                  body += pos->second.substr( begin, end - begin );
                  if( (end+1 < macroLength) || (pos->second[ end+1 ] < '0') || (pos->second[ end+1 ] > '9') )
                  {
                    const std::size_t i = std::size_t( pos->second[ end+1 ] - '0' );
                    if( i < args.size() )
                      body += args[ i ];
                    else
                      DUNE_THROW( InvalidStateException, "Macro " << command << " called with too few arguments." );
                  }
                  else
                    DUNE_THROW( InvalidStateException, "Macro " << command << " invalid." );
                  begin = end+2;
                }
                else
                {
                  body += pos->second.substr( begin );
                  break;
                }
              }
              
              std::istringstream s( body );
              (*this)( s, macros, boxes );
            }
          }
          else
            c = input.get();
        }
        else if( (c >= 0) && (c < 0x100) )
        {
          text += char( c );
          c = input.get();
        }
        else
          DUNE_THROW( InvalidStateException, "Invalid character code (" << c << ")." );
      }

      // push last box (if non-empty)
      if( !text.empty() )
      {
        boxes.push_back( new TextBox( text ) );
        text.clear();
      }
    }


    void TextParser::setupCommands ()
    {
      commands_[ "def" ] = new DefCommand();
      commands_[ "undef" ] = new UndefCommand();
      commands_[ "ifdef" ] = new IfDefCommand( *this );
      commands_[ "ifeq" ] = new IfEqCommand( *this );
      commands_[ "advance" ] = new AdvanceCommand();
      commands_[ "input" ] = new InputCommand( *this );
      commands_[ "newline" ] = new NewLineCommand();
      commands_[ "hfill" ] = new HorizontalFillCommand();
      commands_[ "image" ] = new ImageCommand( *this );
      commands_[ "glue" ] = new GlueCommand( *this );
      commands_[ "mbox" ] = new MBoxCommand( *this );
      commands_[ "absolute" ] = new AbsoluteCommand( *this );
      commands_[ "parbox" ] = new ParBoxCommand( *this );
      commands_[ "button" ] = new ButtonCommand( *this );
      commands_[ "color" ] = new ColorCommand();
      commands_[ "font" ] = new FontCommand();
      commands_[ "bf" ] = new BoldCommand();
      commands_[ "it" ] = new ItalicCommand();
      commands_[ "leading" ] = new LeadingCommand();
    }


    int TextParser::parseCommand ( std::istream &input, std::string &command, std::vector< std::string > &args )
    {
      int c = input.get(); 
      if( c == '\\' )
      {
        command = "newline";
        c = input.get();
      }
      else
      {
        command.clear();
        for( ; (c >= 'a') && (c <= 'z'); c = input.get() )
          command += char( c );
      }

      args.clear();
      while( c == '{' )
      {
        // eat opening brace
        c = input.get();

        // read argument
        int depth = 0;
        std::string arg;
        while( (c != '}') || (depth > 0) )
        {
          if( !input )
            DUNE_THROW( InvalidStateException, "unterminated argument." );
          else if( c == '{' )
            ++depth;
          else if( c == '}' )
            --depth;
          arg += char( c );
          c = input.get();
        }
        args.push_back( arg );

        // eat closing brace
        c = input.get();
      }

      return c;
    }

  } // namespace Ash

} // namespace Dune
