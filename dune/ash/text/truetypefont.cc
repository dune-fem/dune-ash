// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/ash/text/truetypefont.hh>

namespace Dune
{

  namespace Ash
  {

    // SDLTrueTypeFont
    // ---------------

    struct SDLTrueTypeFont
    {
      SDLTrueTypeFont ();
      ~SDLTrueTypeFont () { TTF_Quit(); }

      TTF_Font *openFont ( const std::string &file, int ptsize ) { return TTF_OpenFont( file.c_str(), ptsize ); }
      void closeFont ( TTF_Font *font ) { TTF_CloseFont( font ); }
    };



    // Single Instance of SDLTrueTypeFont
    // ----------------------------------

    SDLTrueTypeFont sdlTrueTypeFont;



    // Implementation of SDLTrueTypeFont
    // ---------------------------------

    SDLTrueTypeFont::SDLTrueTypeFont ()
    {
      if( TTF_Init() == -1 )
      {
        std::cerr << "SDL_TTF: " << TTF_GetError() << ".";
        abort();
      }

      SDL_version vCompiled;
      SDL_TTF_VERSION( &vCompiled );
      const SDL_version *vLinked = TTF_Linked_Version();
      if( vLinked->major != vCompiled.major )
        std::cerr << "Warning: A completely different version of SDL_TTF was used at compile time." << std::endl;
      if( vLinked->minor < vCompiled.minor )
        std::cerr << "Warning: An new version of SDL_TTF was used at compile time." << std::endl;
    }


    // Implementation of TrueTypeFont
    // ------------------------------

    TrueTypeFont::TrueTypeFont ( const std::string &file, int ptSize )
    : font_( sdlTrueTypeFont.openFont( file, ptSize ) )
    {
      if( !font_ )
        DUNE_THROW( IOError, "Unable to load font '" << file << "'." );
    }


    TrueTypeFont::~TrueTypeFont ()
    {
      sdlTrueTypeFont.closeFont( font_ );
    }


    GLfloat TrueTypeFont::render ( const std::string &text, Texture &texture ) const
    {
      SDL_Color fg = {0xff, 0xff, 0xff};
      SDL_Color bg = {0x00, 0x00, 0x00};
      SDL_Surface *surface = TTF_RenderUTF8_Shaded( font_, text.c_str(), fg, bg );
      if( !surface )
        DUNE_THROW( Exception, "Unable to render text." );

      std::array< GLsizei, 2 > size;
      size[ 0 ] = surface->w;
      size[ 1 ] = surface->h;

      texture.load( GL_ALPHA, size, 0, GL_ALPHA, (const GLubyte *)surface->pixels );

      SDL_FreeSurface( surface );
      return (size[ 1 ] > 0 ? GLfloat( size[ 0 ] ) / GLfloat( size[ 1 ] ) : 1.0f);
    }

  } // namespace Ash

} // namespace Dune
