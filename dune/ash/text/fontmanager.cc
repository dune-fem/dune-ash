#include <config.h>

#include <dune/ash/text/fontmanager.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of FontManager
    // -----------------------------

    void FontManager::setFont ( const FontId &fontId )
    {
      const FontMap::iterator pos = fontMap_.find( fontId );
      if( pos == fontMap_.end() )
      {
        stateStack_.top().font_ = loadFont( fontId );
        if( !stateStack_.top().font_ )
        {
          if( !defaultFont_ )
            DUNE_THROW( InvalidStateException, "Unable to load default font '" << fontId.first << "'." );
          else
            std::cerr << "Font '" << fontId.first << "' not found, using default font instead." << std::endl;
          stateStack_.top().font_ = defaultFont_;
        }
        fontMap_.insert( std::make_pair( fontId, stateStack_.top().font_ ) );
      }
      else
        stateStack_.top().font_ = pos->second;
    }


    void FontManager::setup ()
    {
      while( !stateStack_.empty() )
        stateStack_.pop();

      stateStack_.push( FontState( nullptr, { 0.0f, 0.0f, 0.0f, 1.0f }, 0 ) );

      defaultFont_ = nullptr;
      setFont( defaultFont() );
      defaultFont_ = stateStack_.top().font_;

      space_ = font().size( " " )[ 0 ];
      lineHeight_ = font().maxAscent() - font().maxDescent();

      stateStack_.top().style_ = Fem::Parameter::getValue< int >( "ash.fontmanager.defaultstyle", 0 );
      setLeading( 0.2f );
    }


    void FontManager::push () { stateStack_.push( stateStack_.top() ); }


    void FontManager::pop () 
    { 
      if ( !stateStack_.empty() )
        stateStack_.pop();
      if ( stateStack_.empty() )
        setup();  
    }

    void FontManager::reset () { setup(); }


    TrueTypeFont *FontManager::loadFont ( const std::string &font, int ptSize,
                                          const std::vector< std::string > &paths )
    {
      for( std::vector< std::string >::const_iterator it = paths.begin(); it != paths.end(); ++it )
      {
        const std::string fileName = *it + "/" + font + ".ttf";
        if( Fem::fileExists( fileName ) )
          return new TrueTypeFont( fileName, ptSize );
      }
      return nullptr;
    }


    TrueTypeFont *FontManager::loadFont ( const FontId &fontId,
                                          const std::vector< std::string > &paths )
    {
      return loadFont( fontId.first, fontId.second, paths );
    }


    typename FontManager::FontId FontManager::defaultFont ()
    {
      const std::string font = Fem::Parameter::getValue< std::string >( "ash.fontmanager.defaultfont" );
      const int ptSize = Fem::Parameter::getValue< int >( "ash.fontmanager.defaultsize" );
      return FontId( font, ptSize );
    }

  } // namespace Ash

} // namespace Dune
