#ifndef DUNE_ASH_TEXT_BOX_HH
#define DUNE_ASH_TEXT_BOX_HH

#include <list>
#include <iostream>

#include <dune/common/fvector.hh>
#include <dune/common/indent.hh>

#include <dune/ash/image.hh>
#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/opengl/texture.hh>
#include <dune/ash/text/fontmanager.hh>

namespace Dune
{

  namespace Ash
  {

    // ActiveBox
    // ---------

    struct ActiveBox
    {
      typedef FieldVector< GLfloat, 2 > Position;

      ActiveBox ( GLfloat left, GLfloat top, GLfloat right, GLfloat bottom, const std::string eventId )
      : left_( left ), top_( top ), right_( right ), bottom_( bottom ),
        eventId_( eventId )
      {}

      bool contains ( const Position &position ) const
      {
        return (position[ 0 ] >= left_) && (position[ 0 ] <= right_) && (position[ 1 ] >= top_) && (position[ 1 ] <= bottom_);
      }

      const std::string &eventId () const { return eventId_; }

    private:
      GLfloat left_, top_, right_, bottom_;
      std::string eventId_;
    };



    // BoxInterface
    // ------------

    struct BoxInterface
    {
      typedef FieldVector< GLfloat, 2 > Position;

      BoxInterface ( GLfloat width = 0.0f, GLfloat ascent = 0.0f, GLfloat descent = 0.0f )
      : width_( width ), ascent_( ascent ), descent_( descent ),
        fillWeight_( 0.0f ), fill_( 0.0f )
      {}

      virtual ~BoxInterface () {} 

      virtual void align ( FontManager &fontManager ) = 0;
      virtual void fill ( GLfloat fill ) { fill_ = fill; }

      virtual void print ( std::ostream &out, const Indent &indent = Indent() ) const = 0;

      virtual void render ( const Position &position ) const = 0;

      virtual void gatherActive ( const Position &position, std::list< ActiveBox > &active ) const {}

      GLfloat ascent () const { return ascent_; }
      GLfloat descent () const { return descent_; }
      GLfloat width () const { return width_; }
      GLfloat fill () const { return fill_; }
      GLfloat totalWidth () const { return width_ + fill_; }
      GLfloat height () const { return ascent_ - descent_; }

      GLfloat fillWeight () const { return fillWeight_; }

    protected:
      void printSize ( std::ostream &out ) const
      {
        out << "[ " << width() << " x (" << ascent() << ", " << descent() << ") + " << fill() << " ]";
      }

      GLfloat width_, ascent_, descent_;
      GLfloat fillWeight_, fill_;
    };



    // TextBox
    // -------

    struct TextBox
    : public BoxInterface
    {
      typedef FieldVector< GLfloat, 4 > Color;

      explicit TextBox ( const std::string &text )
      : text_( text ),
        font_( nullptr )
      {}

      ~TextBox ();

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;

    private:
      TrueTypeFont &font () const { return *font_; }

      std::string text_;
      Color color_;
      int style_;
      TrueTypeFont *font_;
    };



    // ImageBox
    // --------

    struct ImageBox
    : public BoxInterface
    {
      ImageBox ( GLfloat width, GLfloat height, const std::string &fileName );

      ~ImageBox ();

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;

    private:
      std::string fileName_;
    };



    // MBox
    // ----

    struct MBox
    : public BoxInterface
    {
      typedef std::list< BoxInterface * > BoxList;
      typedef std::list< Position > PositionList;

      MBox ( const BoxList &boxes )
      : boxes_( boxes )
      {}

      ~MBox ();

      void align ( FontManager &fontManager );
      void fill ( GLfloat fill );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;

      void gatherActive ( const Position &position, std::list< ActiveBox > &active ) const;

    protected:
      void printChildren ( std::ostream &out, const Indent &indent ) const;

    private:
      BoxList boxes_;
      GLfloat space_;
    };



    // GlueBox
    // -------

    struct GlueBox
    : public BoxInterface
    {
      typedef std::list< BoxInterface * > BoxList;
      typedef std::list< Position > PositionList;

      GlueBox ( const BoxList &boxes )
      : boxes_( boxes )
      {}

      ~GlueBox ();

      void align ( FontManager &fontManager );
      void fill ( GLfloat fill );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;

      void gatherActive ( const Position &position, std::list< ActiveBox > &active ) const;

    protected:
      void printChildren ( std::ostream &out, const Indent &indent ) const;

    private:
      BoxList boxes_;
    };



    // ButtonBox
    // ---------

    struct ButtonBox
    : public MBox
    {
      ButtonBox ( const BoxList &boxes, const std::string eventId )
      : MBox( boxes ),
        eventId_( eventId )
      {}

      ~ButtonBox ();
    
      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void gatherActive ( const Position &position, std::list< ActiveBox > &active ) const;

    private:
      std::string eventId_;
    };



    // AbsoluteBox
    // -----------

    struct AbsoluteBox
    : public BoxInterface
    {
      typedef MBox::BoxList BoxList;

      AbsoluteBox ( const Position &position, const BoxList &boxes );
      AbsoluteBox ( const Position &position, const BoxList &boxes, char hRel, char vRel );

      ~AbsoluteBox ();

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;

      void gatherActive ( const Position &position, std::list< ActiveBox > &active ) const;

    protected:
      Position origin () const;

    private:
      MBox mBox_;
      Position position_;
      char hRel_, vRel_;
    };



    // ParBox
    // ------

    struct ParBox
    : public BoxInterface
    {
      typedef std::list< BoxInterface * > BoxList;
      typedef std::list< Position > PositionList;

      ParBox ( GLfloat width, const BoxList &boxes )
      : BoxInterface( width ),
        boxes_( boxes )
      {}

      ~ParBox ();

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;

      void gatherActive ( const Position &position, std::list< ActiveBox > &active ) const;

    private:
      BoxList boxes_;
      PositionList positions_;
    };



    // HorizontalFillBox
    // -----------------

    struct HorizontalFillBox
    : public BoxInterface
    {
      HorizontalFillBox () { fillWeight_ = 1.0f; }

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;
    };



    // ColorBox
    // --------

    struct ColorBox
    : public BoxInterface
    {
      typedef FieldVector< GLfloat, 4 > Color;

      explicit ColorBox( const Color &color )
      : color_( color )
      {}

      explicit ColorBox( const std::string &literal )
      {
        Color3 color = Fem::Parameter::getValue< Color3 >( "color." + literal, Color3( 0.0f ) );
        color_ = { color[ 0 ], color[ 1 ], color[ 2 ], 1.0f };
      }

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent()  ) const;

      void render ( const Position &position ) const;

    private:
      typedef FieldVector< GLfloat, 3 > Color3;

      Color color_;
    };



    // FontBox
    // -------

    struct FontBox
    : public BoxInterface
    {
      typedef FontManager::FontId FontId;

      explicit FontBox( const std::string &fileName, int size )
      : fontId_( fileName, size )
      {}

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent()  ) const;

      void render ( const Position &position ) const;

    private:
      FontId fontId_;
    };



    // BoldBox
    // -------

    struct BoldBox
    : public BoxInterface
    {
      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;
    };



    // ItalicBox
    // ---------

    struct ItalicBox
    : public BoxInterface
    {
      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;
    };



    // LeadingBox
    // ----------

    struct LeadingBox
    : public BoxInterface
    {
      explicit LeadingBox ( GLfloat factor )
      : factor_( factor )
      {}

      void align ( FontManager &fontManager );

      void print ( std::ostream &out, const Indent &indent = Indent() ) const;

      void render ( const Position &position ) const;

    private:
      GLfloat factor_;
    };



    // RenderParBox
    // ------------

    struct RenderParBox
    {
      explicit RenderParBox ( const ParBox &parBox )
      : parBox_( parBox )
      {}

      void operator() ()
      {
        parBox_.render( { 0.0f, parBox_.ascent() } );
      }

    private:
      const ParBox &parBox_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_TEXT_BOX_HH
