#include <config.h>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/text/truetypefont.hh>
#include <dune/ash/text/box.hh>

#include <dune/fem/io/io.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of TextBox
    // -------------------------

    TextBox::~TextBox() {}


    void TextBox::align ( FontManager &fontManager )
    {
      // most likely a copy of the desired state is needed instead of a reference
      color_ = fontManager.color();
      font_ = &fontManager.font();
      style_ = fontManager.getStyle();
      
      font().style( style_ );

      FieldVector< GLfloat, 2 > dpu = fontManager.dotsPerUnit();
      FieldVector< int, 2 > size = font().size( text_ );

      width_ = GLfloat( size[ 0 ] ) / dpu[ 0 ]; 
      ascent_ = GLfloat( font().maxAscent() ) / dpu[ 1 ];
      descent_ = GLfloat( font().maxDescent() ) / dpu[ 1 ];
    }


    void TextBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "TextBox( " << text_ << " )";
      printSize( out );
      out << std::endl;
    }


    void TextBox::render ( const Position &position ) const
    {
      if( !font_ )
        DUNE_THROW( InvalidStateException, "Error: Boxes need to be aligned before rendering." );

      FieldVector< GLfloat, 3 > upperLeft, lowerRight;
      upperLeft[ 0 ] = position[ 0 ];
      upperLeft[ 1 ] = position[ 1 ] - ascent();
      upperLeft[ 2 ] = 0.0f;
      lowerRight[ 0 ] = position[ 0 ] + width();
      lowerRight[ 1 ] = position[ 1 ] - descent();
      lowerRight[ 2 ] = 0.0f;

      Texture texture;
      font().style( style_ );
      font().render( text_, texture );
      BillBoard( upperLeft, lowerRight, texture, color_ )();
    }



    // Implementation of ImageBox
    // --------------------------

    ImageBox::ImageBox ( GLfloat width, GLfloat height, const std::string &fileName )
    : BoxInterface( width, height ),
      fileName_( fileName )
    {
      if( (width > 0.0f) && (height > 0.0f) )
        return;

      Texture texture;
      const std::array< GLsizei, 2 > size = loadImage( fileName_, texture );

      if( width > 0.0f )
        ascent_ = width * GLfloat( size[ 1 ] ) / GLfloat( size[ 0 ] );
      else if( height > 0.0f )
        width_ = height * GLfloat( size[ 0 ] ) / GLfloat( size[ 1 ] );
      else
        width_ = ascent_ = 0.0f;
    }

    ImageBox::~ImageBox() {}

    void ImageBox::align ( FontManager &fontManager ) {}


    void ImageBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "ImageBox( " << fileName_ << " )";
      printSize( out );
      out << std::endl;
    }


    void ImageBox::render ( const Position &position ) const
    {
      FieldVector< GLfloat, 3 > upperLeft, lowerRight;
      upperLeft[ 0 ] = position[ 0 ];
      upperLeft[ 1 ] = position[ 1 ] - ascent();
      upperLeft[ 2 ] = 0.0f;
      lowerRight[ 0 ] = position[ 0 ] + width();
      lowerRight[ 1 ] = position[ 1 ] - descent();
      lowerRight[ 2 ] = 0.0f;

      Texture texture;
      loadImage( fileName_, texture );
      BillBoard( upperLeft, lowerRight, texture )();
    }



    // Implementation of GlueBox
    // -------------------------

    GlueBox::~GlueBox()
    {
      for( BoxList::iterator it = boxes_.begin(); it != boxes_.end(); ++it )
        delete *it;
    }


    void GlueBox::align ( FontManager &fontManager )
    {
      // initialize dimensions
      width_ = 0.0f;
      ascent_ = 0.0f;
      descent_ = 0.0f;
      fillWeight_ = 0.0f;

      fontManager.push();

      for( BoxList::const_iterator it = boxes_.begin(); it != boxes_.end(); ++it )
      {
        if( *it )
        {
          (*it)->align( fontManager );

          width_ += (*it)->width();
          ascent_ = std::max( ascent_, (*it)->ascent() );
          descent_ = std::min( descent_, (*it)->descent() );
          fillWeight_ += (*it)->fillWeight();
        }
        else
          std::cerr << "Warning: Ignoring newline in mbox." << std::endl;
      }

      fontManager.pop();
    }


    void GlueBox::fill ( GLfloat fill )
    {
      BoxInterface::fill( fill );

      if( fillWeight() > 0 )
      {
        fill /= fillWeight();
        for( BoxList::const_iterator it = boxes_.begin(); it != boxes_.end(); ++it )
        {
          if( *it )
            (*it)->fill( (*it)->fillWeight()*fill );
        }
      }
    }


    void GlueBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "GlueBox";
      printSize( out );
      out << std::endl;
      printChildren( out, indent );
    }


    void GlueBox::render ( const Position &position ) const
    {
      Position pos( position );
      for( BoxList::const_iterator bit = boxes_.begin(); bit != boxes_.end(); ++bit )
      {
        if( *bit )
        {
          (*bit)->render( pos );
          pos[ 0 ] += (*bit)->totalWidth();
        }
      }
    }


    void GlueBox::gatherActive ( const Position &position, std::list< ActiveBox > &active ) const
    {
      Position pos( position );
      for( BoxList::const_iterator bit = boxes_.begin(); bit != boxes_.end(); ++bit )
      {
        if( *bit )
        {
          (*bit)->gatherActive( pos, active );
          pos[ 0 ] += (*bit)->totalWidth();
        }
      }
    }


    void GlueBox::printChildren ( std::ostream &out, const Indent &indent ) const
    {
      for( BoxList::const_iterator it = boxes_.begin(); it != boxes_.end(); ++it )
      {
        if( *it )
          (*it)->print( out, indent + 1 );
        else
          std::cerr << "Warning: Ignoring newline in mbox." << std::endl;
      }
    }



    // Implementation of MBox
    // ----------------------

    MBox::~MBox()
    {
      for( BoxList::iterator it = boxes_.begin(); it != boxes_.end(); ++it )
        delete *it;
    }


    void MBox::align ( FontManager &fontManager )
    {
      // initialize dimensions
      width_ = 0.0f;
      ascent_ = 0.0f;
      descent_ = 0.0f;
      fillWeight_ = 0.0f;
      space_ = 0.0f;

      fontManager.push();

      for( BoxList::const_iterator it = boxes_.begin(); it != boxes_.end(); ++it )
      {
        if( *it )
        {
          (*it)->align( fontManager );
          
	  if( (*it)->width() > 0.0f )
          {
            width_ += (*it)->width() + space_;
            space_ = fontManager.space();
          }

          ascent_ = std::max( ascent_, (*it)->ascent() );
          descent_ = std::min( descent_, (*it)->descent() );
          fillWeight_ += (*it)->fillWeight();
        }
        else
          std::cerr << "Warning: Ignoring newline in mbox." << std::endl;
      }

      fontManager.pop();
    }


    void MBox::fill ( GLfloat fill )
    {
      BoxInterface::fill( fill );
      if( fillWeight() > 0 )
      {
        fill /= fillWeight();
        for( BoxList::const_iterator it = boxes_.begin(); it != boxes_.end(); ++it )
        {
          if( *it )
            (*it)->fill( (*it)->fillWeight()*fill );
        }
      }
    }


    void MBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "MBox";
      printSize( out );
      out << std::endl;
      printChildren( out, indent );
    }


    void MBox::render ( const Position &position ) const
    {
      Position pos( position );
      for( BoxList::const_iterator bit = boxes_.begin(); bit != boxes_.end(); ++bit )
      {
        if( *bit )
        {
          (*bit)->render( pos );
          pos[ 0 ] += (*bit)->totalWidth();
	  if( (*bit)->width() > 0.0f )
            pos[ 0 ] += space_;
        }
      }
    }


    void MBox::gatherActive ( const Position &position, std::list< ActiveBox > &active ) const
    {
      Position pos( position );
      for( BoxList::const_iterator bit = boxes_.begin(); bit != boxes_.end(); ++bit )
      {
        if( *bit )
        {
          (*bit)->gatherActive( pos, active );
          pos[ 0 ] += (*bit)->totalWidth();
	  if( (*bit)->width() > 0.0f )
            pos[ 0 ] += space_;
        }
      }
    }


    void MBox::printChildren ( std::ostream &out, const Indent &indent ) const
    {
      for( BoxList::const_iterator it = boxes_.begin(); it != boxes_.end(); ++it )
      {
        if( *it )
          (*it)->print( out, indent + 1 );
        else
          std::cerr << "Warning: Ignoring newline in mbox." << std::endl;
      }
    }



    // Implementation of ButtonBox
    // ---------------------------

    ButtonBox::~ButtonBox () {}


    void ButtonBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "ButtonBox( "  << eventId_ << " )";
      printSize( out );
      out << std::endl;
      printChildren( out, indent );
    }


    void ButtonBox::gatherActive ( const Position &position, std::list< ActiveBox > &active ) const
    {
      active.push_back( ActiveBox( position[ 0 ], position[ 1 ] - ascent(), position[ 0 ] + width(), position[ 1 ] - descent(), eventId_ ) );
    }



    // Implementation of AbsoluteBox
    // -----------------------------

    AbsoluteBox::AbsoluteBox ( const Position &position, const BoxList &boxes )
    : mBox_( boxes ),
      position_( position ),
      hRel_( 'l' ), vRel_( 't' )
    {}


    AbsoluteBox::AbsoluteBox ( const Position &position, const BoxList &boxes, char hRel, char vRel )
    : mBox_( boxes ),
      position_( position ),
      hRel_( hRel ), vRel_( vRel )
    {
      if( (hRel != 'l') && (hRel != 'c') && (hRel != 'r') )
        DUNE_THROW( InvalidStateException, "AbsoluteBox: Invalid horizontal relative position: '" << hRel << "'." );
      if( (vRel != 't') && (vRel != 'c') && (vRel != 'l') && (vRel != 'b') )
        DUNE_THROW( InvalidStateException, "AbsoluteBox: Invalid vertical relative position: '" << vRel << "'." );
    }


    AbsoluteBox::~AbsoluteBox () {}


    void AbsoluteBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "AbsoluteBox( "  << position_ << " )";
      printSize( out );
      out << std::endl;
      mBox_.print( out, indent+1 );
    }


    void AbsoluteBox::align ( FontManager &fontManager )
    {
      mBox_.align( fontManager );
    }


    void AbsoluteBox::render ( const Position &position ) const
    {
      mBox_.render( origin() );
    }


    void AbsoluteBox::gatherActive ( const Position &position, std::list< ActiveBox > &active ) const
    {
      mBox_.gatherActive( origin(), active );
    }


    AbsoluteBox::Position AbsoluteBox::origin () const
    {
      Position origin( position_ );

      if( hRel_ == 'c' )
        origin[ 0 ] -= 0.5f*mBox_.totalWidth();
      else if( hRel_ == 'r' )
        origin[ 0 ] -= mBox_.totalWidth();

      if( vRel_ == 't' )
        origin[ 1 ] += mBox_.ascent();
      else if( vRel_ == 'c' )
        origin[ 1 ] += 0.5f*(mBox_.ascent() + mBox_.descent());
      else if( vRel_ == 'b' )
        origin[ 1 ] += mBox_.descent();

      return origin;
    }



    // Implementation of ParBox
    // ------------------------

    ParBox::~ParBox()
    {
      for( BoxList::iterator it = boxes_.begin(); it != boxes_.end(); ++it )
        delete *it;
    }


    void ParBox::align ( FontManager &fontManager )
    {
      fontManager.push();

      positions_.clear();

      ascent_ = 0.0f;
      Position position( 0.0f );

      GLfloat lineWidth = 0.0f;
      BoxList::const_iterator lineBegin = boxes_.begin();
      BoxList::const_iterator lineEnd = boxes_.begin();
      BoxInterface *lastAligned = nullptr;
      while( lineBegin != boxes_.end() )
      {
        // eat any leading newlines (empty lines have height 0 anyway)

        if( !*lineBegin )
        {
          ++lineBegin;
          ++lineEnd;
          positions_.push_back( position );
        }

        // find line (it will be contained in [lineBegin,lineEnd[)

        while( (lineEnd != boxes_.end()) && *lineEnd )
        {
          if( lastAligned != *lineEnd )
          {
            lastAligned = *lineEnd;
            lastAligned->align( fontManager );
          }

          const GLfloat boxWidth = (*lineEnd)->width();
          const GLfloat space = ((lineWidth == 0.0f) || (boxWidth == 0.0f) ? 0.0f : fontManager.space());
          if( (lineWidth + space + boxWidth <= width()) || (lineWidth == 0.0f) )
          {
            lineWidth += space + boxWidth;
            ++lineEnd;
          }
          else
            break;
        }

        // compute height information

        GLfloat lineAscent = 0.0f, lineDescent = 0.0f;
        for( BoxList::const_iterator it = lineBegin; it != lineEnd; ++it )
        {
          lineAscent = std::max( lineAscent, (*it)->ascent() );
          lineDescent = std::min( lineDescent, (*it)->descent() );
        }
        position[ 1 ] += lineAscent;
        if( lineBegin == boxes_.begin() )
          ascent_ = lineAscent;

        // compute fill

        if( lineWidth > width() )
          std::cerr << "Warning: overful parbox (by " << (lineWidth - width()) << ")." << std::endl;
        GLfloat fill = std::max( width() - lineWidth, 0.0f );
        GLfloat fillWeight = 0;
        for( BoxList::const_iterator it = lineBegin; it != lineEnd; ++it )
          fillWeight += (*it)->fillWeight();
        GLfloat spaceFill = fontManager.space();
        if( (fillWeight == 0.0f) && (lineEnd != boxes_.end()) && *lineEnd )
        {
          int count = 0;
          for( BoxList::const_iterator it = lineBegin; it != lineEnd; ++it )
          {
            if( (*it)->width() > 0.0f )
              ++count;
          }
          if( count > 1 )
            spaceFill += fill / GLfloat( count-1 );
        }
        fill = (fillWeight > 0.0f ? fill / fillWeight : 0.0f);
        for( BoxList::const_iterator it = lineBegin; it != lineEnd; ++it )
        {
          (*it)->fill( (*it)->fillWeight()*fill );
          positions_.push_back( position );
          position[ 0 ] += (*it)->totalWidth();
          if( (*it)->width() > 0.0f )
            position[ 0 ] += spaceFill;
        }

        // start next line

        position[ 0 ] = 0.0f;
        position[ 1 ] -= lineDescent - fontManager.leading();
        lineBegin = lineEnd;
        lineWidth = 0.0f;
      }

      assert( positions_.size() == boxes_.size() );
      descent_ = ascent_ - position[ 1 ];

      fontManager.pop();
    }


    void ParBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "ParBox";
      printSize( out );
      out << std::endl;

      for( BoxList::const_iterator it = boxes_.begin(); it != boxes_.end(); ++it )
      {
        if( *it )
          (*it)->print( out, indent + 1 );
        else
          out << (indent+1) << "----------------" << std::endl;
      }
    }


    void ParBox::render ( const Position &position ) const
    {
      if( positions_.size() != boxes_.size() )
        DUNE_THROW( InvalidStateException, "Error: Boxes need to be aligned before rendering." );

      Position origin = position;
      origin[ 1 ] -= ascent();

      PositionList::const_iterator pit = positions_.begin();
      for( BoxList::const_iterator bit = boxes_.begin(); bit != boxes_.end(); ++bit, ++pit )
      {
        if( *bit )
          (*bit)->render( origin + (*pit) );
      }
    }


    void ParBox::gatherActive ( const Position &position, std::list< ActiveBox > &active ) const
    {
      if( positions_.size() != boxes_.size() )
        DUNE_THROW( InvalidStateException, "Error: Boxes need to be aligned before gathering active boxes." );

      Position origin = position;
      origin[ 1 ] -= ascent();

      PositionList::const_iterator pit = positions_.begin();
      for( BoxList::const_iterator bit = boxes_.begin(); bit != boxes_.end(); ++bit, ++pit )
      {
        if( *bit )
          (*bit)->gatherActive( origin + (*pit), active );
      }
    }



    // Implementatio of HorizontalFillBox
    // ----------------------------------

    void HorizontalFillBox::align ( FontManager &fontManager ) {}

    void HorizontalFillBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "HorizontalFillBox()";
      printSize( out );
      out << std::endl;
    }

    void HorizontalFillBox::render ( const Position &position ) const {}



    // Implementation of ColorBox
    // --------------------------

    void ColorBox::align ( FontManager &fontManager ) { fontManager.setColor( color_ ); }


    void ColorBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "ColorBox( " << color_ << ")";
      printSize( out );
      out << std::endl;
    }


    void ColorBox::render ( const Position &position ) const {} // nothing to do here



    // Implementation of FontBox
    // -------------------------

    void FontBox::align ( FontManager &fontManager ) { fontManager.setFont( fontId_ ); }


    void FontBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "FontBox( " << fontId_.first << ", " << fontId_.second << ")";
      printSize( out );
      out << std::endl;
    }


    void FontBox::render ( const Position &position ) const {}



    // Implementatio of BoldBox
    // ------------------------

    void BoldBox::align ( FontManager &fontManager ) { fontManager.setStyle( TTF_STYLE_BOLD );  }

    void BoldBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "BoldBox()";
      printSize( out );
      out << std::endl;
    }

    void BoldBox::render ( const Position &position ) const {}



    // Implementatio of ItalicBox
    // --------------------------

    void ItalicBox::align ( FontManager &fontManager ) { fontManager.setStyle( TTF_STYLE_ITALIC );  }

    void ItalicBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "ItalicBox()";
      printSize( out );
      out << std::endl;
    }

    void ItalicBox::render ( const Position &position ) const {}



    // Implementatio of LeadingBox
    // ---------------------------

    void LeadingBox::align ( FontManager &fontManager ) { fontManager.setLeading( factor_ ); }

    void LeadingBox::print ( std::ostream &out, const Indent &indent ) const
    {
      out << indent << "LeadingBox( " << factor_ << " )";
      printSize( out );
      out << std::endl;
    }

    void LeadingBox::render ( const Position &position ) const {}

  } // namespace Ash

} // namespace Dune
