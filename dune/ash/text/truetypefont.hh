#ifndef DUNE_ASH_TEXT_FONT_HH
#define DUNE_ASH_TEXT_FONT_HH

#include <SDL_ttf.h>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/ash/opengl/texture.hh>

namespace Dune
{

  namespace Ash
  {

    // TrueTypeFont
    // ------------

    class TrueTypeFont
    {
      typedef TrueTypeFont ThisType;

    public:
      TrueTypeFont ( const std::string &file, int size );
      ~TrueTypeFont ();

      std::string family () const { return TTF_FontFaceFamilyName( font_ ); }

      bool fixedWidth () const { return TTF_FontFaceIsFixedWidth( font_ ); }

      int lineSkip () const { return TTF_FontLineSkip( font_ ); }

      int maxAscent () const { return TTF_FontAscent( font_ ); }
      int maxDescent () const { return TTF_FontDescent( font_ ); }
      int maxHeight () const { return TTF_FontHeight( font_ ); }

      bool kerning () const { return TTF_GetFontKerning( font_ ); }
      void kerning ( bool enable ) { TTF_SetFontKerning( font_, enable ); }

      int style () const { return TTF_GetFontStyle( font_ ); }
      void style ( int style ) { TTF_SetFontStyle( font_, style ); }

      FieldVector< int, 2 > size ( const std::string &text ) const;

      GLfloat render ( const std::string &text, Texture &texture ) const;

    private:
      // prohibit copying and assignment
      TrueTypeFont ( const ThisType & );
      const ThisType &operator= ( const ThisType & );

      TTF_Font *font_;
    };



    // Implementation of TrueTypeFont
    // ------------------------------

    inline FieldVector< int, 2 > TrueTypeFont::size ( const std::string &text ) const
    {
      FieldVector< int, 2 > size;
      TTF_SizeUTF8( font_, text.c_str(), &size[ 0 ], &size[ 1 ] );
      return size;
    }

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_TEXT_FONT_HH
