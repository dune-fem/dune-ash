#ifndef DUNE_ASH_STRINGVECTOR_HH
#define DUNE_ASH_STRINGVECTOR_HH

#include <initializer_list>
#include <string>
#include <vector>
#include <utility>

#include <dune/fem/io/parameter.hh>

namespace Dune
{

  namespace Ash
  {

    // StringVector
    // ------------

    class StringVector
      : public std::vector< std::string >
    {
      typedef StringVector This;
      typedef std::vector< std::string > Base;

    public:
      StringVector () = default;

      StringVector ( std::initializer_list< std::string >&& list ) : Base( std::move( list ) ) {}

    };

  } // namespace Ash


  namespace Fem
  {

    template< >
    struct ParameterParser< Ash::StringVector >
    {
      static bool parse ( const std::string &s, Ash::StringVector &value )
      {       
        value.clear();

        const std::size_t sLen = s.length();
        for( std::size_t begin = 0; begin < sLen; )
        {   
          std::size_t end = s.find_first_of( ':', begin );
          if( end == s.npos )
            end = sLen;
          if( end - begin > 0 ) 
            value.push_back( s.substr( begin, end - begin ) );
          begin = end+1;
        }
        return true;
      }

      static std::string toString ( const Ash::StringVector &value )
      {
        std::string out;

        for( const auto &s : value )
          out += s + ":";
        out.pop_back();

        return out;
      }
    };

  } //namespace Fem


  namespace Ash
  {

    static inline std::ostream &operator<< ( std::ostream &out, const StringVector &value )
    {
      return (out << Fem::ParameterParser< StringVector >::toString( value ));
    }

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_STRINGVECTOR_HH
