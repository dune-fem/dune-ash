#ifndef DUNE_ASH_TEXTURES_ARROW_HH
#define DUNE_ASH_TEXTURES_ARROW_HH

#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // Arrow
    // -----

    struct Arrow
    {
      // displays the arrow used for the vector field visualization
      //
      // The arrow lives in the reference element [0,1]^2 and points from left to
      // right.

      Arrow( const GLdouble bodyRightWidth = 0.15 )
      : bodyRightWidth_( bodyRightWidth )
      {}

      void operator() ()
      {
        const GLdouble bodyLeftMargin = .1;
        const GLdouble bodyLeftWidth = .2;
        const GLdouble bodyInHeadOverlay = .01;
        const GLdouble headWidth = .4;
        const GLdouble headRightMargin = .1;

        glTranslate( -.5, -.5, 0. );

        glBegin( GL_QUADS );
        {
          glVertex( bodyLeftMargin, .5 - bodyLeftWidth/2. );
          glVertex( .5 + bodyInHeadOverlay, .5 - bodyRightWidth_/2. );
          glVertex( .5 + bodyInHeadOverlay, .5 + bodyRightWidth_/2. );
          glVertex( bodyLeftMargin, .5 + bodyLeftWidth/2. );
        }
        glEnd();

        glBegin( GL_TRIANGLES );
        {
          glVertex( .5, .5 - headWidth/2. );
          glVertex( 1. - headRightMargin, .5 );
          glVertex( .5, .5 + headWidth/2. );
        }
        glEnd();
      }

    private:
      const GLdouble bodyRightWidth_;

    };

  } // namespace Ash

} // namespace Dune


#endif //#ifndef DUNE_ASH_TEXTURES_ARROW_HH
