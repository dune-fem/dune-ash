#ifndef DUNE_ASH_TEXTURES_BACK_HH
#define DUNE_ASH_TEXTURES_BACK_HH

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/opengl.hh>


namespace Dune
{

  namespace Ash
  {

    // NumberSign
    // ----------

    struct Back
    {
      typedef FieldVector< GLfloat, 4 > Color;

      explicit Back ( const FieldVector< GLfloat, 3 > &color )
      : color_( { color[0], color[1], color[2], 1.0f } ) 
      {}

      explicit Back ( const Color &color = { 0, 0, 0, 1 } )
      : color_( color )
      {}


      void operator() ()
      {
        glColor( color_ );

        glBegin( GL_QUADS );
        {
          glVertex( 0.3f, 0.4f );
          glVertex( 0.9f, 0.4f );
          glVertex( 0.9f, 0.6f );
          glVertex( 0.3f, 0.6f );
        }
        glEnd();

        glBegin( GL_TRIANGLES );
        {
          glVertex( 0.1, 0.5 );
          glVertex( 0.3, 0.7 );
          glVertex( 0.3, 0.3 );
        }
        glEnd();

      }

    private:
      Color color_;

    };

  } // namespace Ash

} // namespace Dune


#endif //#ifndef DUNE_ASH_TEXTURES_BACK_HH
