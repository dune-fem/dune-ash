#ifndef DUNE_ASH_TEXTURES_COLORMAPPER_HH
#define DUNE_ASH_TEXTURES_COLORMAPPER_HH

#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    template< class T >
    struct LinearAlphaMapper
    {
      GLenum format () const { return GL_ALPHA; }

      typedef GLfloat Color;

      LinearAlphaMapper ( const T &min, const T &max )
      : min_( min ), max_( max )
      {}

      Color map ( const T &value ) const
      {
        const Color relValue = Color( (value - min_) / (max_ - min_) );
        return std::min( 1.0f, std::max( 0.0f, relValue ) );
      }

    private:
      T min_, max_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_TEXTURES_COLORMAPPER_HH
