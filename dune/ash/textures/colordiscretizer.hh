#ifndef DUNE_ASH_TEXTURES_COLORDISCRETIZER_HH
#define DUNE_ASH_TEXTURES_COLORDISCRETIZER_HH

#include <array>

#include <SDL.h>

#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // ColorDiscretizer
    // ----------------

    template< class Color >
    struct ColorDiscretizer;

    template<>
    struct ColorDiscretizer< std::array< GLfloat, 4 > >
    {
      typedef std::array< GLfloat, 4 > Color;
      typedef Uint32 DiscreteColor;

      static int bitsPerPixel () { return 32; }

      static Uint32 redMask () { return Uint32( 0xff ) << 16; }
      static Uint32 greenMask () { return Uint32( 0xff ) << 8; }
      static Uint32 blueMask () { return Uint32( 0xff ) << 0; }
      static Uint32 alphaMask () { return Uint32( 0xff ) << 24; }

      DiscreteColor operator() ( const Color &color ) const
      {
        return (scale( color[ 0 ] ) << 16) | (scale( color[ 1 ] ) << 8)
               | (scale( color[ 2 ] ) << 0) | (scale( color[ 3 ] ) << 24);
      }

    private:
      static DiscreteColor scale ( const GLfloat &a )
      {
        GLfloat clamped = std::max( std::min( a, 1.0f ), 0.0f );
        return DiscreteColor( 255.0f * clamped + 0.3f );
      }
    };

    template<>
    struct ColorDiscretizer< GLfloat >
    {
      typedef GLfloat Color;
      typedef Uint8 DiscreteColor;

      static int bitsPerPixel () { return 8; }

      static Uint32 redMask () { return Uint32( 0 ); }
      static Uint32 greenMask () { return Uint32( 0 ); }
      static Uint32 blueMask () { return Uint32( 0 ); }
      static Uint32 alphaMask () { return Uint32( 0xff ); }

      DiscreteColor operator() ( const Color &color ) const
      {
        GLfloat clamped = std::max( std::min( color, 1.0f ), 0.0f );
        return Color( 255.0f * clamped + 0.3f );
      }
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_TEXTURES_COLORDISCRETIZER_HH
