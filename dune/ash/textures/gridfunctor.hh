#ifndef DUNE_ASH_TEXTURES_GRIDFUNCTOR_HH
#define DUNE_ASH_TEXTURES_GRIDFUNCTOR_HH

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/functor.hh>
#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // GridFunctor
    // -----------

    template< class Functor >
    struct GridFunctor
    {

      typedef FieldVector< GLfloat, 3 > Position;
      typedef FieldVector< GLfloat, 4 > Color;
      
      explicit GridFunctor( Functor functor, size_t level = 1, const Color &color = Color( 1.0f ), const Position &offset = Position( 0 ) )
      : functor_( functor ),
        border_( RectangleOutline( color ) ),
        offset_( offset ),
        color_( color ),
        level_( level )
      {}

      void operator() ()
      {
        glColor( color_ );

        for( std::size_t i = 0; i < level_*level_; ++i )
        {
          const GLfloat height = 1.0 / level_;
          const GLfloat width = 1.0 / level_;
          const Position position = { ( width * ( i % level_ ) ), height * ( i / level_ ), 0.0f  };

          glMatrixMode( GL_MODELVIEW );
          glPushMatrix();
          glTranslate( position );
          glScale( width, height, 1.0f );
          glTranslate( offset_ );
          functor_();
          glPopMatrix();
        }

        border_();
      }

    private:
      Functor functor_;
      RectangleOutline border_;
      const Position offset_;
      const Color color_;
      const size_t level_;
    };



    // DeformFunctor
    // -------------

    template< class Functor >
    struct DeformFunctor
    {
      typedef FieldVector< GLfloat, 3 > Position;

      explicit DeformFunctor( Functor functor, const Position &topLeft = Position( 0.0f), const Position &bottomRight = { 1.0f, 1.0f, 1.0f } )
      : functor_( functor ),
        topLeft_( topLeft ),
        bottomRight_( bottomRight )
      {}

      void operator() ()
      {
        glMatrixMode( GL_MODELVIEW );
        glPushMatrix();
        glTranslate( topLeft_ );
        glScale( bottomRight_[ 0 ] - topLeft_[ 0 ], bottomRight_[ 1 ] - topLeft_[ 1 ], 1.0f );
        functor_();
        glPopMatrix();
      }

    private:
      Functor functor_;
      const Position topLeft_;
      const Position bottomRight_;
    };
    
  } // namespace Ash

} // namespace Dune


#endif //#ifndef DUNE_ASH_TEXTURES_GRIDFUNCTOR_HH
