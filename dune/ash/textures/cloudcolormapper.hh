#ifndef DUNE_ASH_TEXTURES_CLOUDCOLORMAPPER_HH
#define DUNE_ASH_TEXTURES_CLOUDCOLORMAPPER_HH

#include <array>

#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    template< class T >
    struct CloudColorMapper
    {
      GLenum format () const { return GL_RGBA; }

      typedef std::array< GLfloat, 4 > Color;

      CloudColorMapper ( const T &min, const T &max )
      : min_( min ), max_( max )
      {}

      Color map ( const T &value ) const
      {
        GLfloat relValue = GLfloat( (value - min_) / (max_ - min_) );
        GLfloat clampedValue = std::min( 2.0f, std::max( 0.0f, relValue ) );

        GLfloat gray = (clampedValue - 1.0f <= 0.0f ? 0.5f : 1.0f - 0.5f*clampedValue);
        GLfloat alpha = (clampedValue - 1.0f <= 0.0f ? clampedValue : 1.0f);

        Color color;
        color[ 0 ] = color[ 1 ] = color[ 2 ] = gray;
        color[ 3 ] = alpha;
        return color;
      }

    private:
      T min_, max_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_TEXTURES_COLORMAPPER_HH
