#ifndef IO_hh
#define IO_hh

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <string>
#include <vector>

#include <dune/fem/io/io.hh>

template< class MousePathType >
void writeField( const std::list< MousePathType > &data, const std::string &filename, bool forceWriting = false )
{
  typedef typename std::list< MousePathType >::const_iterator ListIteratorType;
  typedef typename MousePathType::const_iterator PathIteratorType;
  typedef typename MousePathType::value_type DataTuple;
  typedef typename DataTuple::first_type PositionType;

  if( !forceWriting && Dune::Fem::fileExists( filename ) )
  {
    std::cerr << "Error in writeField: '" << filename << "' already exists!\n";
    abort();
  }

  std::ofstream outstream( filename.c_str() );
  for( ListIteratorType it = data.begin(); it != data.end(); ++it )
  {
    for( PathIteratorType pit = it->begin(); pit != it->end(); ++pit )
    {
      const PositionType &position = pit->first;
      const PositionType &direction = pit->second;
      outstream << position[ 0 ] << " " << position[ 1 ] << "  ";
      outstream << direction[ 0 ] << " " << direction[ 1 ] << std::endl;
    }
    outstream << std::endl;
  }
  outstream.close();
}

template< class MousePathType >
void readField( std::list< MousePathType > &data, const std::string &filename, bool forceReading = false )
{
  typedef typename MousePathType :: value_type    DataTuple;
  typedef typename DataTuple :: first_type        PositionType;
  typedef typename DataTuple :: second_type       DirectionType;

  data.clear();

  std::ifstream instream( filename.c_str() );
  if( !instream )
  {
    std::cerr << "Error: Cannot open \"" << filename << "\"." << std::endl;
    if( !forceReading )
      abort();
    return;
  }

  std::string line;
  std::vector< double > row;
  MousePathType path;
  while( std::getline( instream, line ) )
  {
    row.clear();
    std::istringstream ss( line );

    if( line.empty() )
    {
      if( !path.empty() )
        data.push_back( path );
      path.clear();
    }
    else
    {
      double x;
      while( ss >> x )
        row.push_back( x );

      // assert that we read in 4 doubles
      if( row.size() != 4 )
      {
        std::cerr << "File \"" << filename << "\" is faulty." << std::endl;
        abort();
      }

      PositionType p;
      DirectionType d;
      p[0] = row[0];
      p[1] = row[1];
      d[0] = row[2];
      d[1] = row[3];

      // append data to MousePathType structure
      path.push_back( std::make_pair( p, d ) );
    }
  }
  if( !path.empty() )
    data.push_back( path );

  instream.close();
}

#endif // IO_hh

