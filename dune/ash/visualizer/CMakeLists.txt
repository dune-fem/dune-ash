set( HEADERS
  discretefunction.hh
  vectorfield.hh
)

install( FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/ash/visualizer )
