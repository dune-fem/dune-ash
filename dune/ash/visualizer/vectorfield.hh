// vim: set expandtab ts=2 sw=2 sts=2: 
#ifndef VECTORFIELDVISUALIZER_HH
#define VECTORFIELDVISUALIZER_HH

#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // VectorFieldVisualizer
    // ---------------------

    class VectorFieldVisualizerBase
    {
      typedef VectorFieldVisualizerBase ThisType;

    public:
      typedef FieldVector< GLfloat, 2 > VectorType;

      explicit VectorFieldVisualizerBase ( const Dune::Ash::DisplayList &arrowList )
      : arrowList_( &arrowList )
      {}

      // 'v' is the value at the center of the cell
      void drawArrow ( const VectorType &centerCoordinate,
                       const VectorType &direction,
                       GLfloat scalingFactor,
                       GLfloat colorScaling ) const
      {
        const GLfloat abs = direction.two_norm();
        GLfloat v[ 4 ] = { (direction[ 0 ] / abs), (direction[ 1 ] / abs) };

        GLfloat rotationMatrix[ 16 ] = { v[ 0 ], -v[ 1 ], 0.0f, 0.0f,
                                         v[ 1 ],  v[ 0 ], 0.0f, 0.0f,
                                         0.0f,    0.0f,   1.0f, 0.0f,
                                         0.0f,    0.0f,   0.0f, 1.0f };

        const GLfloat redOffset = std::min( std::max( abs / colorScaling, 0.0f ), 1.0f );

        glColor( 0.0f, 0.0f, 1.0f, redOffset );

        glMatrixMode( GL_MODELVIEW );
        glPushMatrix();
        glTranslate( centerCoordinate[ 0 ], 1.0f - centerCoordinate[ 1 ], 0.0f );
        glMultMatrixf( rotationMatrix );
        glScalef( scalingFactor, scalingFactor, 0. );
      
        arrowList()();
        glPopMatrix();
      }

    private:
      const Dune::Ash::DisplayList &arrowList () const { return *arrowList_; }

      const Dune::Ash::DisplayList *arrowList_;
    };



    // VectorFieldVisualizer
    // ---------------------

    template< typename DiscreteFunction >
    class VectorFieldVisualizer
    : public VectorFieldVisualizerBase
    {
      typedef VectorFieldVisualizer< DiscreteFunction > ThisType;
      typedef VectorFieldVisualizerBase BaseType;

    public:
      typedef DiscreteFunction DiscreteFunctionType;
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
      typedef typename IteratorType::Entity EntityType;
      typedef typename EntityType::Geometry Geometry;
      typedef typename Geometry::GlobalCoordinate GlobalCoordinateType;
      typedef typename Geometry::LocalCoordinate LocalCoordinateType;
      typedef typename DiscreteFunctionSpaceType::RangeType RangeType;

    protected:
      using BaseType::drawArrow;

    public:
      VectorFieldVisualizer ( const Dune::Ash::DisplayList &arrowList,
                              const DiscreteFunctionType &discreteFunction )
      : BaseType( arrowList ),
        discreteFunction_( discreteFunction )
      {}

      void visualizeVectorField ( double colorScaling ) const
      {
        const DiscreteFunctionSpaceType &dfSpace = discreteFunction_.space();

        const IteratorType end = dfSpace.end();
        IteratorType it = dfSpace.begin();

        /*
         * Note: we assume here that the grid is conform, i.e. that all cells have the
         * same size
         */
        const Geometry &firstGeometry = it->geometry();
        const double cellWidth = firstGeometry.corner( 1 )[ 0 ] - firstGeometry.corner( 0 )[ 0 ];

        // obtain some sampling points in the reference element
        std::vector< LocalCoordinateType > samplePoints;
        /*for ( int i=0; i < 4; ++i )
        {
          samplePoints.push_back( firstGeometry.local( firstGeometry.center() + firstGeometry.corner( i ) ) );
          samplePoints.back() *= .5;
        }*/
        samplePoints.push_back( LocalCoordinateType( 0.5 ) );

        RangeType value;
        for ( ; it != end; ++it )
        {
          const EntityType &entity = *it;
          const Geometry &geometry = entity.geometry();

          LocalFunctionType localFunction = discreteFunction_.localFunction( entity );
        
          for ( int i=0; i < 1; ++i )
          {
            localFunction.evaluate( samplePoints[ i ], value );
            drawArrow( geometry.global( samplePoints[ i ] ),
                       value,
                       static_cast< GLfloat >( cellWidth ),
                       static_cast< GLfloat >( colorScaling ) );
          }
        }
      }

      // dumps the absolute values of the function to 'filename' for gnuplot
      void dumpAbsoluteValueGnuPlot ( const std::string &filename ) const
      {
        std::ofstream ofstream( filename.c_str() );

        const DiscreteFunctionSpaceType &dfSpace = discreteFunction_.space();
        const IteratorType end = dfSpace.end();
        IteratorType it = dfSpace.begin();

        /*
         * Note: we assume here that the grid is conform, i.e. that all cells have the
         * same size
         */
        const Geometry &firstGeometry = it->geometry();
        const double cellWidth = firstGeometry.corner( 1 )[ 0 ] - firstGeometry.corner( 0 )[ 0 ];

        // obtain some sampling points in the reference element
        std::vector< LocalCoordinateType > samplePoints;
        for ( int i=0; i < 4; ++i )
        {
          samplePoints.push_back( firstGeometry.local( firstGeometry.center() + firstGeometry.corner( i ) ) );
          samplePoints.back() *= .5;
        }

        RangeType value;
        for ( ; it != end; ++it )
        {
          const EntityType &entity = *it;
          const Geometry &geometry = entity.geometry();

          LocalFunctionType localFunction = discreteFunction_.localFunction( entity );
        
          for ( int i=0; i < 4; ++i )
          {
            localFunction.evaluate( samplePoints[ i ], value );
            GlobalCoordinateType global = geometry.global( samplePoints[ i ] );
            ofstream << global[ 0 ] << " " << global[ 1 ] << " " 
                     << std::sqrt( value[ 0 ]*value[ 0 ] + value[ 1 ]*value[ 1 ] )
                     << std::endl;
          }
        }
        ofstream.flush();
      }

    private:
      // prohibit copying and assignment
      VectorFieldVisualizer ( const ThisType & );
      const ThisType &operator= ( const ThisType & );

      const DiscreteFunctionType &discreteFunction_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef VECTORFIELDVISUALIZER_HH
