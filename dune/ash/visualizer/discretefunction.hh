// vim: set expandtab ts=2 sw=2 sts=2: 
#ifndef DISCRETEFUNCTIONVISUALIZER_HH
#define DISCRETEFUNCTIONVISUALIZER_HH

#include <array>
#include <cassert>

#include <SDL.h>

#include <dune/grid/spgrid.hh>

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/gridpart/levelgridpart.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/space/finitevolume.hh>

#include <dune/ash/glue/gridpart.hh>
#include <dune/ash/glue/l2projection.hh>
#include <dune/ash/opengl/texture.hh>
#include <dune/ash/textures/colordiscretizer.hh>

namespace Dune
{

  namespace Ash
  {

    // DiscreteFunctionVisualizer
    // --------------------------

    template< class DiscreteFunction >
    class DiscreteFunctionVisualizer
    {
      typedef DiscreteFunctionVisualizer< DiscreteFunction > ThisType;

    public:
      typedef DiscreteFunction DiscreteFunctionType;

      typedef typename DiscreteFunctionType::GridPartType GridPartType;
      typedef typename DiscreteFunctionType::FunctionSpaceType FunctionSpaceType;

      typedef typename GridPartType::ctype ctype;

      static const int dimension = GridPartType::dimension;

    private:
      typedef SPGrid< ctype, dimension > RasterGridType;
      typedef Fem::LevelGridPart< RasterGridType > RasterGridPartType;
      typedef Fem::FiniteVolumeSpace< FunctionSpaceType, RasterGridPartType, 0 > RasterSpaceType;
      typedef Fem::AdaptiveDiscreteFunction< RasterSpaceType > RasterFunctionType;

      typedef typename RasterGridPartType::template Codim< 0 >::IteratorType RasterIteratorType;
      typedef typename RasterGridPartType::template Codim< 0 >::EntityType RasterEntityType;
      typedef typename RasterGridPartType::IndexSetType RasterIndexSetType;

      typedef typename RasterFunctionType::LocalFunctionType LocalRasterFunctionType;

    public:
      DiscreteFunctionVisualizer ( const DiscreteFunctionType &discreteFunction,
                                   const std::array< int, dimension > &rasterCells )
      : rasterCells_( rasterCells ),
        rasterGrid_( discreteFunction.gridPart().grid().domain(), rasterCells_ ),
        rasterGridPart_( rasterGrid_, 0 ),
        rasterSpace_( rasterGridPart_ ),
        rasterFunction_( "raster function", rasterSpace_ )
      {
        typedef GridPartGlue< GridPartType, RasterGridPartType > Glue;
        typedef GluedL2Projection< Glue > Projection;

        Glue glue( discreteFunction.gridPart(), rasterGridPart_ );
        Projection projection( glue );
        projection( discreteFunction, rasterFunction_ );
      }

      template< class ColorMapper >
      void saveBitmap ( const ColorMapper &colorMapper, const std::string &fileName ) const
      {
        typedef ColorDiscretizer< typename ColorMapper::Color > ColorDiscretizerType;
        typedef typename ColorDiscretizerType::DiscreteColor DiscreteColor;

        ColorDiscretizerType colorDiscretizer;
        const int bpp = colorDiscretizer.bitsPerPixel();
        const Uint32 rmask = colorDiscretizer.redMask();
        const Uint32 gmask = colorDiscretizer.greenMask();
        const Uint32 bmask = colorDiscretizer.blueMask();
        const Uint32 amask = colorDiscretizer.alphaMask();

        SDL_Surface *surface
          = SDL_CreateRGBSurface( SDL_SWSURFACE, rasterCells_[ 0 ], rasterCells_[ 1 ],
                                  bpp, rmask, gmask, bmask, amask );
        if( !surface )
          DUNE_THROW( IOError, "Could not save " << fileName << "." );

        if( 8*int( surface->pitch ) != rasterCells_[ 0 ]*bpp )
        {
          SDL_FreeSurface( surface );
          DUNE_THROW( IOError, "Could not save " << fileName << "." );
        }

        if( SDL_LockSurface( surface ) < 0 )
        {
          SDL_FreeSurface( surface );
          DUNE_THROW( IOError, "Could not save " << fileName << "." );
        }

        const RasterIndexSetType &rasterIndexSet = rasterGridPart_.indexSet();
        DiscreteColor *pixels = static_cast< DiscreteColor * >( surface->pixels );

        const RasterIteratorType rend = rasterSpace_.end();
        for( RasterIteratorType rit = rasterSpace_.begin(); rit != rend; ++rit )
        {
          const RasterEntityType &entity = *rit;
          LocalRasterFunctionType localFunction = rasterFunction_.localFunction( entity );
          typename FunctionSpaceType::RangeType value;
          Fem::CachingQuadrature< RasterGridPartType, 0 > quadrature( entity, 0 );
          assert( quadrature.nop() == 1 );
          localFunction.evaluate( quadrature.point( 0 ), value );
          pixels[ rasterIndexSet.index( entity ) ] = colorDiscretizer( colorMapper.map( value ) );
        }

        SDL_UnlockSurface( surface );

        if( SDL_SaveBMP( surface, fileName.c_str() ) < 0 )
        {
          SDL_FreeSurface( surface );
          DUNE_THROW( IOError, "Could not save " << fileName << "." );
        }

        SDL_FreeSurface( surface );
      }

      template< class ColorMapper >
      void loadTexture ( const ColorMapper &colorMapper, Texture &texture ) const
      {
        const RasterIndexSetType &rasterIndexSet = rasterGridPart_.indexSet();
        typename ColorMapper::Color *texels = new typename ColorMapper::Color[ rasterIndexSet.size( 0 ) ];

        const RasterIteratorType rend = rasterSpace_.end();
        for( RasterIteratorType rit = rasterSpace_.begin(); rit != rend; ++rit )
        {
          const RasterEntityType &entity = *rit;
          LocalRasterFunctionType localFunction = rasterFunction_.localFunction( entity );
          typename FunctionSpaceType::RangeType value;
          Fem::CachingQuadrature< RasterGridPartType, 0 > quadrature( entity, 0 );
          assert( quadrature.nop() == 1 );
          localFunction.evaluate( quadrature.point( 0 ), value );
          texels[ rasterIndexSet.index( entity ) ] = colorMapper.map( value );
        }

        texture.load( colorMapper.format(), rasterCells_, 0, colorMapper.format(), texels );
        delete texels;
      }

    private:
      // prohibit copying and assignment
      DiscreteFunctionVisualizer ( const ThisType & );
      const ThisType &operator= ( const ThisType & );

      std::array< int, 2 > rasterCells_;
      RasterGridType rasterGrid_;
      RasterGridPartType rasterGridPart_;
      RasterSpaceType rasterSpace_;
      RasterFunctionType rasterFunction_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DISCRETEFUNCTIONVISUALIZER_HH
