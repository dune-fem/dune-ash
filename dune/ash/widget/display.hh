// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_ASH_WIDGET_DISPLAY_HH
#define DUNE_ASH_WIDGET_DISPLAY_HH

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    // DisplayWidget
    // -------------

    class DisplayWidget
    : public Dune::Ash::Widget
    {
      typedef DisplayWidget This;

    public:
      DisplayWidget ()
      {}

      template< class Functor >
      explicit DisplayWidget ( Functor functor )
      : displayList_( functor )
      {}

      template< class Functor >
      const This &operator= ( Functor functor )
      {
        displayList_.compile( functor );
        return *this;
      }

      void draw () { displayList_(); }

    private:
      DisplayList displayList_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_DISPLAY_HH
