// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_ASH_EVENT_HH
#define DUNE_ASH_EVENT_HH

namespace Dune
{

  namespace Ash
  {

    struct Event
    {
      virtual ~Event () {}

      virtual void executeEvent () = 0;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_EVENT_HH
