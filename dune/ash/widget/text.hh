// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_ASH_WIDGET_TEXT_HH
#define DUNE_ASH_WIDGET_TEXT_HH

#include <dune/common/fvector.hh>

#include <dune/ash/text/truetypefont.hh>
#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/opengl/texture.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    enum Alignment { AlignLeft, AlignCenter, AlignRight };

    class TextWidget
    : public Dune::Ash::Widget
    {
      typedef TextWidget ThisType;

    public:
      typedef FieldVector< GLfloat, 3 > Color;

      TextWidget ( const TrueTypeFont &font, GLfloat width, const std::string &text, Alignment alignment = AlignLeft, GLfloat alpha = 1.0f );

      void draw ( );

      void setText ( const std::string &text );
      void setText ( const std::string &text, Alignment alignment );

      const Texture &getTexture () const { return texture_; }

    private:
      const TrueTypeFont &font_;
      Texture texture_;
      GLfloat width_;
      GLfloat alpha_;
      Alignment alignment_;
      Color color_;
      Color background_;
      GLfloat x0_, x1_;
      Rectangle rect_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_TEXT_HH
