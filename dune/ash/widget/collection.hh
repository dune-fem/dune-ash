#ifndef DUNE_ASH_WIDGET_COLLECTION_HH
#define DUNE_ASH_WIDGET_COLLECTION_HH

#include <list>

#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    // CollectionWidget
    // ----------------

    class CollectionWidget
    : public Widget
    {
      typedef Widget Base;

      struct Child
      {
        Child ( const Position &o, const Position &s, Widget *w )
        : origin( o ), size( s ),
          widget( w )
        {}

        bool contains ( const Position &x ) const
        {
          bool contains = true;
          for( int i = 0; i < 2; ++i )
            contains &= (x[ i ] - origin[ i ] >= 0) & (x[ i ] - origin[ i ] < size[ i ]);
          return contains;
        }

        Position position ( const Position &x ) const
        {
          Position position;
          for( int i = 0; i < 2; ++i )
            position[ i ] = (x[ i ] - origin[ i ]) / size[ i ];
          return position;
        }

        Position origin, size;
        Widget *widget;
      };

      typedef std::list< Child > ChildList;

    public:
      CollectionWidget ();

      void draw ();

      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

      void mouseButtonDown ( const Position &x, Ticks ticks );
      void mouseButtonUp ( const Position &x, Ticks ticks );

      void tick ( Ticks ticks );
      
      void addChild ( const Position &origin, const Position &size, Widget *widget );
      void removeChild ( Widget *widget );

    private:
      ChildList::const_iterator findChild ( const Position &x ) const;

      ChildList childList_;
      ChildList::const_iterator active_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_SPLIT_HH
