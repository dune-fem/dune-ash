// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fstream>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/text/parser.hh>
#include <dune/ash/widget/floatingtext.hh>

namespace Dune
{

  namespace Ash
  {

    FloatingTextWidget::FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, std::istream &input )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( textPath, input );
    }

    FloatingTextWidget::FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, std::istream &input )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( input );
    }

    FloatingTextWidget::FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, const std::string &fileName )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( textPath, fileName );
    }

    FloatingTextWidget::FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, const std::string &fileName )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( fileName );
    }

    FloatingTextWidget::FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {}

    FloatingTextWidget::FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, std::istream &input )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( textPath, input );
    }

    FloatingTextWidget::FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, std::istream &input )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( input );
    }

    FloatingTextWidget::FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, const std::string &fileName )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( textPath, fileName );
    }

    FloatingTextWidget::FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, const std::string &fileName )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {
      setText( fileName );
    }

    FloatingTextWidget::FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size )
    : fontManager_( dpu ),
      size_( size ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      activeBox_( activeBoxes_.end() )
    {}


    FloatingTextWidget::~FloatingTextWidget () {}

    void FloatingTextWidget::setText ( const std::string &textPath, std::istream &input )
    {
      parBox_.reset( new ParBox( size_[ 0 ], TextParser( textPath )( input ) ) );
      setupText();
    }

    void FloatingTextWidget::setText ( std::istream &input )
    {
      parBox_.reset( new ParBox( size_[ 0 ], TextParser()( input ) ) );
      setupText();
    }

    void FloatingTextWidget::setText ( const std::string &textPath, const std::string &fileName )
    {
      std::ifstream input( (textPath + "/" + fileName).c_str() );
      setText( textPath, input );
    }

    void FloatingTextWidget::setText ( const std::string &fileName )
    {
      std::ifstream input( fileName.c_str() );
      setText( input );
    }


    void FloatingTextWidget::setupText ()
    {
      fontManager_.reset();
      parBox_->align( fontManager_ );

      std::array< GLsizei, 2 > rasterSize;
      rasterSize[ 0 ] = GLsizei( std::round( fontManager_.dotsPerUnit()[ 0 ] * size_[ 0 ] ) );
      rasterSize[ 1 ] = GLsizei( std::round( fontManager_.dotsPerUnit()[ 1 ] * size_[ 1 ] ) );

      glMatrixMode( GL_MODELVIEW );
      glPushMatrix();
      glLoadIdentity();
      glScale(  1.0f / size_[ 0 ], 1.0f / size_[ 1 ], 1.0f );

      texture_.load( GL_RGBA, rasterSize, 0, RenderParBox( *parBox_ ) );
      
      glMatrixMode( GL_MODELVIEW );
      glPopMatrix();

      activeBoxes_.clear();
      parBox_->gatherActive( { 0.0f, parBox_->ascent() }, activeBoxes_ );
      activeBox_ = activeBoxes_.end();

      displayList_.compile( BillBoard( texture_ ) );
    }


    Event *FloatingTextWidget::setEvent ( const std::string &id, Event *event )
    {
      const EventMap::iterator pos = events_.find( id );
      if( pos != events_.end() )
      {
        std::swap( event, pos->second );
        if( !pos->second )
          events_.erase( pos );
        return event;
      }
      else
      {
        if( event )
          events_.insert( std::make_pair( id, event ) );
        return nullptr;
      }
    }


    void FloatingTextWidget::deleteEvents ()
    {
      for( EventMap::iterator it = events_.begin(); it != events_.end(); ++it )
        delete it->second;
      events_.clear();
    }


    void FloatingTextWidget::draw ()
    {
      Rectangle( { background_[ 0 ], background_[ 1 ], background_[ 2 ], 1.0f } )();
      if( parBox_ )
        displayList_();
    }


    void FloatingTextWidget::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {}


    void FloatingTextWidget::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      activeBox_ = activeBoxes_.end();
      const Position position = { x[ 0 ] * size_[ 0 ], x[ 1 ] * size_[ 1 ] };
      for( ActiveBoxList::const_iterator it = activeBoxes_.begin(); it != activeBoxes_.end(); ++it )
      {
        if( it->contains( position ) )
          activeBox_ = it;
      }
    }


    void FloatingTextWidget::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      const Position position = { x[ 0 ] * size_[ 0 ], x[ 1 ] * size_[ 1 ] };
      if( (activeBox_ != activeBoxes_.end()) && activeBox_->contains( position ) )
      {
        const EventMap::const_iterator pos = events_.find( activeBox_->eventId() );
        if( pos != events_.end() )
          pos->second->executeEvent();
        else
          std::cerr << "Error: Event '" << activeBox_->eventId() << "' undefined." << std::endl;
      }
      activeBox_ = activeBoxes_.end();
    }

  } // namespace Ash

} // namespace Dune
