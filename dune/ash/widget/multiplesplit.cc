// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/fvector.hh>

#include <dune/ash/widget/multiplesplit.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of SplitWidget
    // -----------------------------

    template< Orientation orientation >
    MultipleSplitWidget< orientation >::MultipleSplitWidget ( const std::vector< GLdouble > &offsets, const std::vector< bool > drawLine, GLdouble lineOffset )
    : offsets_( offsets.size() + 2 ),
      child_( offsets.size() + 1, nullptr ),
      active_( nullptr ),
      activeIndex_( -1 ),
      drawLine_( drawLine ),
      lineOffset_( lineOffset )
    {
      offsets_[ 0 ] = 0;
      offsets_[ offsets.size() + 1 ] = 1;
      std::copy( offsets.begin(), offsets.end(), offsets_.begin()+1 );
    }

    template< Orientation orientation >
    MultipleSplitWidget< orientation >::MultipleSplitWidget ( const std::vector< int > &ratios, const std::vector< bool > drawLine, GLdouble lineOffset )
    : offsets_( ratios.size() + 1 ),
      child_( ratios.size(), nullptr ),
      active_( nullptr ),
      activeIndex_( -1 ),
      drawLine_( drawLine ),
      lineOffset_( lineOffset )
    {
      offsets_[ 0 ] = 0;
      offsets_[ ratios.size() ] = 1;

      GLint sum = 0;

      for( std::size_t i=0; i < ratios.size(); ++i)
        sum += ratios[ i ];

       // make sure you dont divide with 0
      sum = ( sum < 1 ? 1 : sum );

      for( std::size_t i=0; i < ratios.size() - 1; ++i) 
        offsets_[ i + 1 ] = offsets_[ i ] + static_cast< GLdouble >( ratios[ i ] )  / sum;
      
    }

    template< Orientation orientation >
    void MultipleSplitWidget< orientation >::draw ()
    {
      typedef FieldVector< GLfloat, 3 > Color;

      for( std::size_t i = 0; i< child_.size(); ++i )
      {
        draw( child_[i], i );

        if( drawLine_ [ i ] )
        {
          glLineWidth( 3.0f );
          glBegin( GL_LINES );
          {
            glColor( Color( 0.0f ) );
            Position first( lineOffset_ );
            Position second( 1.0 - lineOffset_ );
            first[ direction ] = offsets_[ i ];
            second[ direction ] = offsets_[ i ];
            glVertex( first );
            glVertex( second );
          }
          glEnd();
        }
      }
    }


    template< Orientation orientation >
    void MultipleSplitWidget< orientation >
      ::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {
      if( active_ )
      {
        active_->mouseMove( childPosition( activeIndex_, x ), dx, ticks );
      }
      else 
      {
        std::size_t i = getChild( x );        
        if( child_[ i ] )
        {
          child_[ i ]->mouseMove( childPosition( i, x ), dx, ticks );
        }
      }
    }


    template< Orientation orientation >
    void MultipleSplitWidget< orientation >
      ::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      if( !active_ )
      {
        activeIndex_ = getChild( x );
        active_ = child_[ activeIndex_ ];
      }
      if( active_ )
        active_->mouseButtonDown( childPosition( activeIndex_, x ), ticks );
    }


    template< Orientation orientation >
    void MultipleSplitWidget< orientation >
      ::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      if( active_ )
      {
        active_->mouseButtonUp( childPosition( activeIndex_, x ), ticks );
        active_ = nullptr;
      }
      else
      {
        std::size_t i = getChild( x );
        if( child_[ i ] )
          child_[ i ]->mouseButtonUp( childPosition( i, x ), ticks );
      }
    }


    template< Orientation orientation >
    void MultipleSplitWidget< orientation >::draw ( Widget *child, std::size_t i )
    {
      if( !child )
        return;

      glMatrixMode( GL_MODELVIEW );
      glPushMatrix();

      FieldVector< GLdouble, 3 > t( 0.0 );
      t[ direction ] = offsets_[ i ];
      glTranslate( t );

      FieldVector< GLdouble, 3 > s( 1.0 );
      s[ direction ] = offsets_[ i + 1 ] - offsets_[ i ];
      glScale( s );

      child->draw();

      glPopMatrix();
    }


    template< Orientation orientation >
    typename MultipleSplitWidget< orientation >::Position
    MultipleSplitWidget< orientation >::childPosition ( std::size_t i, const Position &x )
    {
      assert( i < child_.size() );
      Position xChild( x );
      xChild[ direction ] = (x[ direction ] - offsets_[ i ]) / ( offsets_[ i + 1 ] - offsets_[ i ] );
      return xChild;
    }



    template< Orientation orientation >
    std::size_t MultipleSplitWidget< orientation >::getChild ( const Position &x ) const
    {
      for( std::size_t i = 0; i < child_.size(); ++i )
      {
        if( x[ direction ] < offsets_[ i+1 ] )
          return i;
      }
      return child_.size()-1;
    }



    // Template Instantiation
    // ----------------------

    template class MultipleSplitWidget< Horizontal >;
    template class MultipleSplitWidget< Vertical >;

  } // namespace Ash

} // namespace Dune
