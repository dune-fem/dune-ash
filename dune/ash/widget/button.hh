// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_ASH_WIDGET_BUTTON_HH
#define DUNE_ASH_WIDGET_BUTTON_HH

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/event.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    class ButtonWidget
    : public Dune::Ash::Widget
    {
      typedef ButtonWidget ThisType;

    public:
      typedef FieldVector< GLfloat, 3 > Color;

      template< class Functor >
      explicit ButtonWidget ( Event *event, Functor functor )
      : event_( event ),
        displayList_( functor )
      {
        initialize();
      }

      template< class Functor >
      explicit ButtonWidget ( Functor functor )
      : event_( nullptr ),
        displayList_( functor )
      {
        initialize();
      }

      void draw ();

      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

      void mouseButtonDown ( const Position &x, Ticks ticks );

      void mouseButtonUp ( const Position &x, Ticks ticks );

      template< class Functor >
      void setIcon( Functor functor ) { displayList_.compile( functor ); }

      Event *setEvent ( Event *event );

    private:
      void initialize ();

      Event *event_;
      DisplayList displayList_;
      bool pressed_;
      Color background_;
      Color lineColor_;
      GLfloat lineWidth_;
    };



    // Implementation of ButtonWidget
    // ------------------------------

    inline Event *ButtonWidget::setEvent ( Event *event )
    {
      std::swap( event_, event );
      return event;
    }

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_BUTTON_HH
