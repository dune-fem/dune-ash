#ifndef DUNE_ASH_WIDGET_GRID_HH
#define DUNE_ASH_WIDGET_GRID_HH

#include <array>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/functor.hh>
#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    // GridWidget
    // ----------

    class GridWidget
    : public Widget
    {
      typedef GridWidget This;
      typedef Widget Base;

    public:
      typedef FieldVector< GLfloat, 3 > Color;
      typedef std::array< std::size_t, 2 > Index;

      GridWidget ( std::size_t horizontal, std::size_t vertical,
                   const Color &background = Color( 1 ), const Position &border = Position( 0 ) );

      void draw ();

      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

      void mouseButtonDown ( const Position &x, Ticks ticks );
      void mouseButtonUp ( const Position &x, Ticks ticks );

      Widget *getChild ( const Index &index ) const { return getChild( index[ 0 ], index[ 1 ] ); }
      Widget *getChild ( std::size_t h, std::size_t v ) const;
      Widget *setChild ( const Index &index, Widget *child ) { return setChild( index[ 0 ], index[ 1 ], child ); }
      Widget *setChild ( std::size_t h, std::size_t v, Widget *child );
     
      Index size () const { return size_; }

      void tick ( Ticks ticks );

    private:
      void draw ( Widget *child, const Position &origin, const Position &size );

      Widget *getChild ( const Position &x, Index &index );

      Position childPosition ( const Index &index, const Position &x ) const;
      Position origin ( const Index &index ) const;
      Position size ( const Index &index ) const;

      Index size_;
      Position border_, diff_;
      std::vector< Widget * >  child_;
      Widget *active_;
      Index activeIndex_;
      Rectangle rect_;
    };



    // Implementation of GridWidget
    // ----------------------------

    inline Widget *
    GridWidget::getChild ( std::size_t h, std::size_t v ) const
    {
      assert( h < size_[ 0 ] );
      assert( v < size_[ 1 ] );
      return child_[ h * size_[ 1 ] + v ];
    }


    inline Widget *
    GridWidget::setChild ( std::size_t h, std::size_t v, Widget *child )
    {
      assert( h < size_[ 0 ] );
      assert( v < size_[ 1 ] );
      std::swap( child_[ h * size_[ 1 ] + v ], child );
      return child;
    }

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_GRID_HH
