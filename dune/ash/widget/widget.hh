#ifndef DUNE_ASH_WIDGET_WIDGET_HH
#define DUNE_ASH_WIDGET_WIDGET_HH

#include <dune/common/fvector.hh>

namespace Dune
{

  namespace Ash
  {

    // Orientation
    // -----------

    enum Orientation { Horizontal, Vertical };


    // Reference in CCW order
    // ----------------------
    enum Reference { Center,
                     Top,
                     TopLeft, 
                     Left, 
                     BottomLeft, 
                     Bottom, 
                     BottomRight, 
                     Right,
                     TopRight };


    // Widget
    // ------

    class Widget
    {
      typedef Widget ThisType;

    public:
      typedef unsigned long long Ticks;
      typedef FieldVector< double, 2 > Position;

      virtual ~Widget () {}

      virtual void draw () = 0;

      virtual void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) {}

      virtual void mouseButtonDown ( const Position &x, Ticks ticks ) {}
      virtual void mouseButtonUp ( const Position &x, Ticks ticks ) {}

      virtual void tick ( Ticks ticks ) {}
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_WIDGET_HH
