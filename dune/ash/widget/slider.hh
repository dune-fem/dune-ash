// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_ASH_WIDGET_SLIDER_HH
#define DUNE_ASH_WIDGET_SLIDER_HH

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    // SliderWidget 
    // ------------

    template< Orientation orientation >
    class SliderWidget
    : public Dune::Ash::Widget
    {
      typedef SliderWidget ThisType;

      static const int direction = (orientation == Horizontal ? 0 : 1);

    public:
      typedef FieldVector< GLfloat, 3 > Color;
      typedef std::pair< std::size_t, std::size_t > Range;

      SliderWidget ( const Range &range, const Position &border, GLfloat barWidth = 0.5f );

      void draw ();

      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

      void mouseButtonDown ( const Position &x, Ticks ticks );

      void mouseButtonUp ( const Position &x, Ticks ticks );

      std::size_t position () const { return position_; }

      void incPosition () { if( !grabbed_ && (position_ < active_.second) ) ++position_; }
      void decPosition () { if( !grabbed_ && (position_ > active_.first) ) --position_; }

      void setPosition ( std::size_t position ) { if( !grabbed_ ) position_ = clamp( position ); }
      void setActiveRange ( const Range &range );

    private:
      void followMouse ( const Position &x );
      std::size_t clamp ( std::size_t position ) const;

      Range range_, active_;
      std::size_t position_;
      bool grabbed_;
      FieldVector< GLfloat, 2 > border_;
      GLfloat barWidth_;
      Color colorBackground_, colorForeground_, colorInactive_, colorActive_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_SLIDER_HH
