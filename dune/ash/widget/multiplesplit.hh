#ifndef DUNE_ASH_WIDGET_MULTISPLIT_HH
#define DUNE_ASH_WIDGET_MULTISPLIT_HH

#include <vector>

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/widget.hh>
#include <dune/ash/widget/split.hh>

namespace Dune
{

  namespace Ash
  {

    // MultipleSplitWidget
    // -------------------

    template< Orientation orientation >
    class MultipleSplitWidget
    : public Widget
    {
      typedef Widget Base;

      static const int direction = (orientation == Horizontal ? 0 : 1);

    public:
      explicit MultipleSplitWidget ( const std::vector< int > &ratios, const std::vector< bool > drawLine, GLdouble lineOffset = 0 );
      explicit MultipleSplitWidget ( const std::vector< GLdouble > &offsets, const std::vector< bool > drawLine, GLdouble lineOffset = 0 );

      void draw ();

      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

      void mouseButtonDown ( const Position &x, Ticks ticks );
      void mouseButtonUp ( const Position &x, Ticks ticks );

      Widget* setChild( std::size_t i, Widget *child );
      Widget* getChild( std::size_t i );

      std::size_t size() { return child_.size(); }
      
    private:
      void draw ( Widget *child, std::size_t i );

      Position childPosition ( std::size_t i, const Position &x );

      std::size_t getChild ( const Position &x ) const ;

      std::vector< GLdouble > offsets_;
      std::vector< Widget*> child_;
      Widget *active_;
      std::size_t activeIndex_;
      std::vector< bool > drawLine_;
      GLdouble lineOffset_;
    };


    // Implementation of MultipleSplitWidget
    // -------------------------------------

    template< Orientation orientation >
    inline Widget* 
    MultipleSplitWidget< orientation >::setChild ( std::size_t i, Widget *child )
    {
      assert( i < child_.size() );
      std::swap( child_[ i ], child );
      return child;
    }


    template< Orientation orientation >
    inline Widget* 
    MultipleSplitWidget< orientation >::getChild ( std::size_t i )
    {
      assert( i < child_.size() );
      return child_[i];
    }


  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_MULTISPLIT_HH
