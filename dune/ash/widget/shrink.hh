#ifndef DUNE_ASH_WIDGET_SHRINK_HH
#define DUNE_ASH_WIDGET_SHRINK_HH

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {


    // ShrinkWidget
    // -----------

    template< Reference reference = Center>
    class ShrinkWidget
      : public Widget
    {
      typedef Widget Base;

    public:
      typedef FieldVector< GLfloat, 4 > Color;

      ShrinkWidget ( GLfloat width, GLfloat height, GLfloat alpha = 0.5f );
      ShrinkWidget ( GLfloat width, GLfloat height, Color bgColor );

      void draw ();

      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

      void mouseButtonDown ( const Position &x, Ticks ticks );
      void mouseButtonUp ( const Position &x, Ticks ticks );

      void tick ( Ticks ticks );

      Widget *getChild () const { return child_; }
      Widget *setChild ( Widget *child )
      {
        std::swap( child, child_ );
        return child;
      }

    private:
      Position childPosition ( const Position &x );

      GLfloat width_;
      GLfloat height_; 
      Widget *child_;
      Color bgColor_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_SPLIT_HH
