// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/fvector.hh>

#include <dune/ash/widget/split.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of SplitWidget
    // -----------------------------

    template< Orientation orientation >
    SplitWidget< orientation >::SplitWidget ( GLdouble leftWeight, GLdouble rightWeight )
    : ratio_( leftWeight / (leftWeight + rightWeight) ),
      left_( nullptr ), right_( nullptr ), 
      active_( nullptr )     
    {}


    template< Orientation orientation >
    void SplitWidget< orientation >::draw ()
    {
      draw( left_, 0.0, ratio_ );
      draw( right_, ratio_, 1.0 );
    }


    template< Orientation orientation >
    void SplitWidget< orientation >
      ::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {
      if( active_ )
      {
        active_->mouseMove( activeChildPosition_( ratio_, x ), dx, ticks );
      }
      else if( x[ direction ] < ratio_ )
      {
        if( left_ )
          left_->mouseMove( leftChildPosition( ratio_, x ), dx, ticks );
      }
      else
      {
        if( right_ )
          right_->mouseMove( rightChildPosition( ratio_, x ), dx, ticks );
      }
    }


    template< Orientation orientation >
    void SplitWidget< orientation >
      ::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      if( active_ )
      {
        active_->mouseButtonDown( activeChildPosition_( ratio_, x ), ticks );
      }
      else if( x[ direction ] < ratio_ )
      {
        if( left_ )
          left_->mouseButtonDown( leftChildPosition( ratio_, x ), ticks );
        active_ = left_;
        activeChildPosition_ = leftChildPosition;
      }
      else
      {
        if( right_ )
          right_->mouseButtonDown( rightChildPosition( ratio_, x ), ticks );
        active_ = right_;
        activeChildPosition_ = rightChildPosition;
      }
    }


    template< Orientation orientation >
    void SplitWidget< orientation >
      ::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      if( active_ )
      {
        active_->mouseButtonUp( activeChildPosition_( ratio_, x ), ticks );
        active_ = nullptr;
      }
      else if( x[ direction ] < ratio_ )
      {
        if( left_ )
          left_->mouseButtonUp( leftChildPosition( ratio_, x ), ticks );
      }
      else
      {
        if( right_ )
          right_->mouseButtonUp( rightChildPosition( ratio_, x ), ticks );
      }
    }


    template< Orientation orientation >
    void SplitWidget< orientation >::draw ( Widget *child, GLdouble left, GLdouble right )
    {
      if( !child )
        return;

      glMatrixMode( GL_MODELVIEW );
      glPushMatrix();

      FieldVector< GLdouble, 3 > t( 0.0 );
      t[ direction ] = left;
      glTranslate( t );

      FieldVector< GLdouble, 3 > s( 1.0 );
      s[ direction ] = (right - left);
      glScale( s );

      child->draw();

      glPopMatrix();
    }


    template< Orientation orientation >
    void SplitWidget< orientation >::tick ( Ticks ticks )
    {
      if ( left_ )
        left_->tick( ticks );
      if ( right_ )
        right_->tick( ticks );
    }


    template< Orientation orientation >
    typename SplitWidget< orientation >::Position
    SplitWidget< orientation >::childPosition ( GLdouble left, GLdouble right, const Position &x )
    {
      Position xChild( x );
      // compute position in child
      xChild[ direction ] = (x[ direction ] - left) / (right - left);
      // clamp values to child window (in case mouse is outside the window)
      xChild[ direction ] = std::max( std::min( xChild[ direction ], 1.0 ), 0.0 );
      return xChild;
    }



    // Template Instantiation
    // ----------------------

    template class SplitWidget< Horizontal >;
    template class SplitWidget< Vertical >;

  } // namespace Ash

} // namespace Dune
