// vim: set expandtab ts=8 sw=2 sts=2:
#include <config.h>

#include <dune/ash/widget/empty.hh>

namespace Dune
{

  namespace Ash
  {

    EmptyWidget emptyWidget;

  } // namespace Ash

} // namespace Dune
