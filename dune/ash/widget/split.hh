#ifndef DUNE_ASH_WIDGET_SPLIT_HH
#define DUNE_ASH_WIDGET_SPLIT_HH

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    // SplitWidget
    // -----------

    template< Orientation orientation >
    class SplitWidget
    : public Widget
    {
      typedef Widget Base;

      static const int direction = (orientation == Horizontal ? 0 : 1);

    public:
      SplitWidget ( GLdouble leftWeight, GLdouble rightWeight );

      void draw ();

      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

      void mouseButtonDown ( const Position &x, Ticks ticks );
      void mouseButtonUp ( const Position &x, Ticks ticks );

      void setLeftChild ( Widget *child ) { left_ = child; }
      void setRightChild ( Widget *child ) { right_ = child; }

      void tick ( Ticks ticks );
      
    private:
      void draw ( Widget *child, GLdouble left, GLdouble right );

      static Position childPosition ( GLdouble left, GLdouble right, const Position &x );

      static Position leftChildPosition ( GLdouble ratio, const Position &x ) { return childPosition( 0.0, ratio, x ); }
      static Position rightChildPosition ( GLdouble ratio, const Position &x ) { return childPosition( ratio, 1.0, x ); }

      GLdouble ratio_;
      Widget *left_, *right_;
      Widget *active_;
      Position (*activeChildPosition_) ( GLdouble ratio, const Position &x );
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_SPLIT_HH
