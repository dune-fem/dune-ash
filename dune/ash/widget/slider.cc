// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/fem/io/parameter.hh>

#include <dune/ash/widget/slider.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of SliderWidget
    // ------------------------------

    template< Orientation orientation >
    SliderWidget< orientation >::SliderWidget ( const Range &range, const Position &border, GLfloat barWidth )
    : range_( range ),
      active_( range ),
      position_( range.first ),
      grabbed_( false ),
      border_( border ),
      barWidth_( barWidth ),
      colorBackground_( Fem::Parameter::getValue< Color >( "ash.slider.background", { 1, 1, 1 } ) ),
      colorForeground_( Fem::Parameter::getValue< Color >( "ash.slider.foreground", { 0, 0, 0 } ) ),
      colorInactive_( Fem::Parameter::getValue< Color >( "ash.slider.inactive", { 0.8f, 0.8f, 0.8f } ) ),
      colorActive_( Fem::Parameter::getValue< Color >( "ash.slider.active", { 0.7f, 0.7f, 1.0f } ) )
    {}


    template< Orientation orientation >
    void SliderWidget< orientation >::draw ()
    {
      typedef FieldVector< GLfloat, 2 > Coordinate;

      const Coordinate width = { 1.0f - 2.0f*border_[ 0 ], 1.0f - 2.0f*border_[ 1 ] };
      const GLfloat tic = width[ direction ] / GLfloat( range_.second - range_.first );

      const GLfloat areathickness = 0.15f;
      const GLfloat arrowdist = 1.5f*barWidth_*tic;
      const GLfloat arrowwidth = 2.0f*barWidth_*tic;
      const GLfloat arrowheight = 0.125f;

      glBegin( GL_QUADS );
      {
        glColor( colorBackground_ );
        glQuad( Coordinate( 0.0f ), Coordinate( 1.0f ) );

        Coordinate left = { 0.5f - areathickness*width[ 0 ], 0.5f - areathickness*width[ 1 ] };
        Coordinate right = { 0.5f + areathickness*width[ 0 ], 0.5f + areathickness*width[ 1 ] };

        left[ direction ] = border_[ direction ];
        right[ direction ] = 1.0f - border_[ direction ];

        glColor( colorInactive_ );
        glQuad( left, right );

        left[ direction ] += (active_.first - range_.first)*tic;
        right[ direction ] -= (range_.second - active_.second)*tic;

        glColor( colorActive_ );
        glQuad( left, right );

        left = border_;
        left[ direction ] = border_[ direction ] + (position_ - barWidth_)*tic;

        right = { 1.0f - border_[ 0 ], 1.0f - border_[ 1 ] };
        right[ direction ] = border_[ direction ] + (position_ + barWidth_)*tic;

        glColor( colorForeground_ );
        glQuad( left, right );
      }
      glEnd();

      glBegin( GL_TRIANGLES );
      {
        glColor( colorForeground_ );

        Coordinate left = { 0.5f - arrowheight*width[ 0 ], 0.5f - arrowheight*width[ 1 ] };
        left[ direction ] = border_[ direction ] + position_*tic - arrowdist;

        Coordinate right = { 0.5f + arrowheight*width[ 0 ], 0.5f + arrowheight*width[ 1 ] };
        right[ direction ] = border_[ direction ] + position_*tic + arrowdist;

        if( position_ > active_.first )
        {
          Coordinate x( 0.5f );

          glVertex( left );

          x[ direction ] = left[ direction ] - arrowwidth;
          glVertex( x );

          x = right;
          x[ direction ] = left[ direction ];
          glVertex( x );
        }

        if( position_ < active_.second )
        {
          Coordinate x;

          x = left;
          x[ direction ] = right[ direction ];
          glVertex( x );

          x = { 0.5f, 0.5f };
          x[ direction ] = right[ direction ] + arrowwidth;
          glVertex( x );

          glVertex( right );
        }
      }
      glEnd();
    }


    template< Orientation orientation >
    void SliderWidget< orientation >::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {
      if( grabbed_ )
        followMouse( x );
    }


    template< Orientation orientation >
    void SliderWidget< orientation >::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      grabbed_ = true;
      followMouse( x );
    }


    template< Orientation orientation >
    void SliderWidget< orientation >::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      if( grabbed_ )
        followMouse( x );
      grabbed_ = false;
    }


    template< Orientation orientation >
    void SliderWidget< orientation >::followMouse ( const Position &x )
    {
      const GLfloat width = 1.0f - 2.0f*border_[ direction ];
      const GLfloat tic = width / GLfloat( range_.second - range_.first );
      if( x[ direction ] <= border_[ direction ] )
        position_ = active_.first;
      else if( x[ direction ] >= 1.0f - border_[ direction ] )
        position_ = active_.second;
      else
        position_ = clamp( std::size_t( (x[ direction ] + 0.5f*tic - border_[ direction ]) / tic ) );
    }


    template< Orientation orientation >
    std::size_t SliderWidget< orientation >::clamp ( std::size_t position ) const
    {
      return std::max( active_.first, std::min( active_.second, position ) );
    }


    template< Orientation orientation >
    void SliderWidget< orientation >::setActiveRange ( const Range &range )
    {
      active_.first = std::max( range_.first, range.first );
      active_.second = std::min( range_.second, range.second );
      if( active_.first > active_.second )
        std::swap( active_.first, active_.second );
      position_ = clamp( position_ );
    }


    // Template Instantiation
    // ----------------------

    template class SliderWidget< Horizontal >;
    template class SliderWidget< Vertical >;

  } // namespace Ash

} // namespace Dune
