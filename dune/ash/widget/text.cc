// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/fem/io/parameter.hh>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/widget/text.hh>

namespace Dune
{

  namespace Ash
  {

    TextWidget::TextWidget ( const TrueTypeFont &font, GLfloat width, const std::string &text, Alignment alignment, GLfloat alpha )
    : font_( font ),
      width_( width ),
      alpha_( alpha ),
      alignment_( alignment ),
      color_( Fem::Parameter::getValue< Color >( "ash.text.foreground", { 0, 0, 0 } ) ),
      background_( Fem::Parameter::getValue< Color >( "ash.text.background", { 1, 1, 1 } ) ),
      rect_( { background_[ 0 ], background_[ 1 ], background_[ 2 ], 1.0f } )
    {
      setText( text );
    }


    void TextWidget::setText ( const std::string &text )
    {
      const GLfloat aspect = font_.render( text, texture_ );
      switch( alignment_ )
      {
      case AlignLeft:
        x0_ = 0.0f;
        x1_ = aspect / width_;
        break;

      case AlignCenter:
        x0_ = 0.5f - aspect / (2.0f * width_);
        x1_ = 0.5f + aspect / (2.0f * width_);
        break;

      case AlignRight:
        x0_ = 1.0f - aspect / width_;
        x1_ = 1.0f;
        break;
      }
    }

    void TextWidget::setText ( const std::string &text, Alignment alignment )
    {
      alignment_ = alignment;
      setText( text );
    }


    void TextWidget::draw ( )
    {
      rect_();

      typedef typename BillBoard::Position Position;
      typedef typename BillBoard::Color Color;
      BillBoard( Position{ x0_, 0.0f, 0.0f }, Position{ x1_, 1.0f, 0.0f }, texture_, Color{ color_[0], color_[1], color_[2], alpha_ } )();
    }

  } // namespace Ash

} // namespace Dune
