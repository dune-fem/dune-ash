// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/fem/io/parameter.hh>

#include <dune/ash/widget/button.hh>

namespace Dune
{

  namespace Ash
  {

    void ButtonWidget::draw ()
    {
      glBegin( GL_QUADS );
      {
        glColor( background_ );
        glVertex( 0.0f, 0.0f );
        glVertex( 1.0f, 0.0f );
        glVertex( 1.0f, 1.0f );
        glVertex( 0.0f, 1.0f );
      }
      glEnd();

      glMatrixMode( GL_MODELVIEW );
      glPushMatrix();
      glTranslate( 0.1f, 0.1f, 0.0f );
      glScale( 0.8f, 0.8f, 1.0f );
      displayList_();
      glPopMatrix();

      glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
      glLineWidth( lineWidth_ );
      glBegin( GL_QUADS );
      {
        glColor( lineColor_ );
        glVertex( 0.0f, 0.0f );
        glVertex( 1.0f, 0.0f );
        glVertex( 1.0f, 1.0f );
        glVertex( 0.0f, 1.0f );
      }
      glEnd();
      glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }


    void ButtonWidget::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {}


    void ButtonWidget::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      pressed_ = true;
    }


    void ButtonWidget::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      if( pressed_ && event_ )
        event_->executeEvent();
      pressed_ = false;
    }


    void ButtonWidget::initialize ()
    {
      pressed_ = false;
      background_ = Fem::Parameter::getValue< Color >( "ash.button.background", { 1, 1, 1 } );
      lineColor_ = Fem::Parameter::getValue< Color >( "ash.button.linecolor", { 0, 0, 0 } );
      lineWidth_ = Fem::Parameter::getValue< GLfloat >( "ash.button.linewidth", 3.0f );
    }

  } // namespace Ash

} // namespace Dune
