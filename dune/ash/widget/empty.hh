// vim: set expandtab ts=8 sw=2 sts=2:
#ifndef DUNE_ASH_WIDGET_EMPTY_HH
#define DUNE_ASH_WIDGET_EMPTY_HH

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    // EmptyWidget
    // -----------

    struct EmptyWidget
      : public Dune::Ash::Widget
    {
      void draw () {}
    };

    extern EmptyWidget emptyWidget;

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_EMPTY_HH
