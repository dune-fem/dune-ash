// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/ash/widget/shrink.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of ShrinkWidget
    // -----------------------------

    template< Reference reference >
    ShrinkWidget< reference >::ShrinkWidget ( GLfloat width, GLfloat height, GLfloat alpha )
    : width_( width ),
      height_( height ),
      child_( nullptr ),
      bgColor_( { 0.0f, 0.0f, 0.0f, alpha } )
    {}

    template< Reference reference >
    ShrinkWidget< reference >::ShrinkWidget ( GLfloat width, GLfloat height, Color bgColor )
    : width_( width ),
      height_( height ),
      child_( nullptr ),
      bgColor_( bgColor )
    {}

    template< Reference reference >
    void ShrinkWidget< reference >::draw ()
    {
      if( !child_ )
        return;

      glMatrixMode( GL_MODELVIEW );
      glPushMatrix();

      if( bgColor_[ 3 ] > 0 )
      {
        glColor( bgColor_ );

        glBegin( GL_QUADS );
        {
          glVertex( 0.0f, 0.0f );
          glVertex( 1.0f, 0.0f ); 
          glVertex( 1.0f, 1.0f ); 
          glVertex( 0.0f, 1.0f ); 
        }
        glEnd();
      }

      FieldVector< GLfloat, 3 > t( 0.0 );
      switch( reference )
      {
        case Center:
          t[ 0 ] = ( 1.0 - width_ ) / 2.0;
          t[ 1 ] = ( 1.0 - height_ ) / 2.0;
          break;

        case Top:
          t[ 0 ] = ( 1.0 - width_ ) / 2.0;
          t[ 1 ] = 0.0;
          break;

        case TopLeft:
          t[ 0 ] = 0.0;
          t[ 1 ] = 0.0;
          break;

        case Left:
          t[ 0 ] = 0.0;
          t[ 1 ] = ( 1.0 - height_ ) / 2.0;
          break;

        case BottomLeft:
          t[ 0 ] = 0.0;
          t[ 1 ] = 1.0 - height_;
          break;

        case Bottom:
          t[ 0 ] = ( 1.0 - width_ ) / 2.0;
          t[ 1 ] = 1.0 - height_;
          break;

        case BottomRight:
          t[ 0 ] = 1.0 - width_;
          t[ 1 ] = 1.0 - height_;
          break;

        case Right:
          t[ 0 ] = 1.0 - width_;
          t[ 1 ] = ( 1.0 - height_ ) / 2.0;
          break;

        case TopRight:
          t[ 0 ] = 1.0 - width_;
          t[ 1 ] = 0.0;
          break;

        default:
          // just draw it in the center
          t[ 0 ] = ( 1.0 - width_ ) / 2.0;
          t[ 1 ] = ( 1.0 - height_ ) / 2.0;
      }
      glTranslate( t );

      FieldVector< GLfloat, 3 > s( 1.0 );
      s[ 0 ] = width_;
      s[ 1 ] = height_;
      glScale( s );

      child_->draw();

      glPopMatrix();
    }


    template< Reference reference >
    void ShrinkWidget< reference >
      ::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {
      if( child_ )
        child_->mouseMove( childPosition( x ), dx, ticks );
    }


    template< Reference reference >
    void ShrinkWidget< reference >
      ::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      if( child_ )  
        child_->mouseButtonDown( childPosition( x ), ticks );
    }


    template< Reference reference >
    void ShrinkWidget< reference >
      ::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      if( child_ )
        child_->mouseButtonUp( childPosition( x ), ticks );
    }


    template< Reference reference >
    void ShrinkWidget< reference >::tick ( Ticks ticks )
    {
      if( child_ )
        child_->tick( ticks );
    }


    template< Reference reference >
    typename ShrinkWidget< reference >::Position
    ShrinkWidget< reference >::childPosition ( const Position &x )
    {
      Position xChild( x );
      switch( reference )
      {
        case Center:
          xChild[ 0 ] = (x[ 0 ] - ( 1. - width_ ) / 2.0 ) / width_ ;
          xChild[ 1 ] = (x[ 1 ] - ( 1. - height_ ) / 2.0 ) / height_;
          break;

        case Top:
          xChild[ 0 ] = (x[ 0 ] - ( 1. - width_ ) / 2.0 ) / width_ ;
          xChild[ 1 ] = x[ 1 ]  / height_;
          break;

        case TopLeft:
          xChild[ 0 ] = x[ 0 ]  / width_ ;
          xChild[ 1 ] = x[ 1 ]  / height_;
          break;

        case Left:
          xChild[ 0 ] = x[ 0 ]  / width_ ;
          xChild[ 1 ] = (x[ 1 ] - ( 1. - height_ ) / 2.0 ) / height_;
          break;

        case BottomLeft:
          xChild[ 0 ] = x[ 0 ]  / width_ ;
          xChild[ 1 ] = (x[ 1 ] - ( 1. - height_ ) ) / height_;
          break;

        case Bottom:
          xChild[ 0 ] = (x[ 0 ] - ( 1. - width_ ) / 2.0 ) / width_ ;
          xChild[ 1 ] = (x[ 1 ] - ( 1. - height_ ) ) / height_;
          break;

        case BottomRight:
          xChild[ 0 ] = (x[ 0 ] - ( 1. - width_ ) ) / width_ ;
          xChild[ 1 ] = (x[ 1 ] - ( 1. - height_ ) ) / height_;
          break;

        case Right:
          xChild[ 0 ] = (x[ 0 ] - ( 1. - width_ ) ) / width_ ;
          xChild[ 1 ] = (x[ 1 ] - ( 1. - height_ ) / 2.0 ) / height_;

        case TopRight:
          xChild[ 0 ] = (x[ 0 ] - ( 1. - width_ ) ) / width_ ;
          xChild[ 1 ] = x[ 1 ]  / height_;
          break;

        default:
          // its in the center
          xChild[ 0 ] = (x[ 0 ] - ( 1. - width_ ) / 2.0 ) / width_ ;
          xChild[ 1 ] = (x[ 1 ] - ( 1. - height_ ) / 2.0 ) / height_;
      }
      xChild[ 0 ] = std::max( std::min( xChild[ 0 ], 1.0 ), 0.0 );
      xChild[ 1 ] = std::max( std::min( xChild[ 1 ], 1.0 ), 0.0 );
      return xChild;
    }

    // Template Instantiation
    // ----------------------

    template class ShrinkWidget< Center >;
    template class ShrinkWidget< Top >;
    template class ShrinkWidget< TopLeft >;
    template class ShrinkWidget< Left >;
    template class ShrinkWidget< BottomLeft >;
    template class ShrinkWidget< Bottom >;
    template class ShrinkWidget< BottomRight >;
    template class ShrinkWidget< Right >;
    template class ShrinkWidget< TopRight >;
    


  } // namespace Ash

} // namespace Dune
