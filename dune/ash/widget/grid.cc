// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/grid.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of GridWidget
    // ----------------------------

    GridWidget::GridWidget ( std::size_t horizontal, std::size_t vertical,
                             const Color &background, const Position &border )
    : border_( border ),
      child_( horizontal * vertical, nullptr ),
      active_( nullptr ),
      rect_( { background[ 0 ], background[ 1 ], background[ 2 ], 1.0f } )
    {
      size_[ 0 ] = horizontal;
      size_[ 1 ] = vertical;
      for( int i = 0; i < 2; ++i )
        diff_[ i ] =  (1.0 - border_[ i ]) / size_[ i ];
    }


    void GridWidget::draw ()
    {
      rect_();

      Index index;
      for( index[ 0 ] = 0; index[ 0 ] < size_[ 0 ] ;++index[ 0 ] ) 
      {
        for( index[ 1 ] = 0; index[ 1 ] < size_[ 1 ]; ++index[ 1 ] )
          draw( getChild( index ), origin( index ), size( index ) );
      }
    }


    void GridWidget::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {
      if( active_ )
        active_->mouseMove( childPosition( activeIndex_, x ), dx, ticks );
      else  
      {
        Index index;
        Widget *child = getChild( x, index );
        if( child )
          child->mouseMove( childPosition( index, x ), dx, ticks );
      }
    }


    void GridWidget::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      if( !active_ )
        active_ = getChild( x, activeIndex_ );
      if( active_ )
        active_->mouseButtonDown( childPosition( activeIndex_, x ), ticks );
    }


    void GridWidget::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      if( active_ )
      {
        active_->mouseButtonUp( childPosition( activeIndex_, x ), ticks );
        active_ = nullptr;
      }
      else
      {
        Index index;
        Widget *child = getChild( x, index );
        if( child )
          child->mouseButtonUp( childPosition( index, x ), ticks );
      }
    }


    void GridWidget::draw ( Widget *child, const Position &origin, const Position &size )
    {
      if( !child )
        return;

      glMatrixMode( GL_MODELVIEW );
      glPushMatrix();

      FieldVector< GLdouble, 3 > t( 0.0 );
      t[ 0 ] = origin[ 0 ];
      t[ 1 ] = origin[ 1 ];
      glTranslate( t );

      FieldVector< GLdouble, 3 > s( 1.0 );
      s[ 0 ] = size[ 0 ];
      s[ 1 ] = size[ 1 ];
      glScale( s );

      child->draw();

      glPopMatrix();
    }


    void GridWidget::tick ( Ticks ticks )
    {
      Index index;
      for( index[ 0 ] = 0; index[ 0 ] < size_[ 0 ] ;++index[ 0 ] ) 
      {
        for( index[ 1 ] = 0; index[ 1 ] < size_[ 1 ]; ++index[ 1 ] )
        {
          Widget *child = getChild( index );
          if ( child )
            child->tick( ticks );
        }
      }
    }


    Widget *GridWidget::getChild ( const Position &x, Index &index )
    {
      for( int i = 0; i < 2; ++i )
      {
        index[ i ] = std::floor( x[ i ] / diff_[ i ] );
        if( (index[ i ] >= size_[ i ]) || (x[ i ] - index[ i ]*diff_[ i ] < border_[ i ]) )
          return nullptr;
      }
      return getChild( index );
    }


    typename GridWidget::Position
    GridWidget::childPosition ( const Index &index, const Position &x ) const
    {
      Position xChild( x );
      const Position o = origin( index );
      const Position s = size( index );
      for( int i = 0; i < 2; ++i )
        xChild[ i ] = (x[ i ] - o[ i ]) / s[ i ];
      return xChild;
    }


    typename GridWidget::Position
    GridWidget::origin ( const Index &index ) const
    {
      return { index[ 0 ] * diff_[ 0 ] + border_[ 0 ], index[ 1 ] * diff_[ 1 ] + border_[ 1 ] };
    }


    typename GridWidget::Position
    GridWidget::size ( const Index &index ) const
    {
      return diff_ - border_;
    }

  } // namespace Ash

} // namespace Dune
