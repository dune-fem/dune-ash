// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/widget/collection.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of CollectionWidget
    // ----------------------------------

    CollectionWidget::CollectionWidget ()
    : childList_(),
      active_( childList_.end() )
    {}


    void CollectionWidget::draw ()
    {
      for( ChildList::const_reverse_iterator it = childList_.rbegin(); it != childList_.rend(); ++it )
      {
        glMatrixMode( GL_MODELVIEW );
        glPushMatrix();

        glTranslate( it->origin );
        glScale( it->size );
        it->widget->draw();

        glPopMatrix();
      }
    }


    void CollectionWidget
      ::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
    {
      if( active_ == childList_.end() )
      {
        ChildList::const_iterator pos = findChild( x );
        if( pos != childList_.end() )
          pos->widget->mouseMove( pos->position( x ), dx, ticks );
      }
      else
        active_->widget->mouseMove( active_->position( x ), dx, ticks );
    }


    void CollectionWidget
      ::mouseButtonDown ( const Position &x, Ticks ticks )
    {
      if( active_ == childList_.end() )
        active_ = findChild( x );
      if( active_ != childList_.end() )
        active_->widget->mouseButtonDown( active_->position( x ), ticks );
    }


    void CollectionWidget
      ::mouseButtonUp ( const Position &x, Ticks ticks )
    {
      if( active_ == childList_.end() )
      {
        ChildList::const_iterator pos = findChild( x );
        if( pos != childList_.end() )
          pos->widget->mouseButtonUp( pos->position( x ), ticks );
      }
      else
        active_->widget->mouseButtonUp( active_->position( x ), ticks );
      active_ = childList_.end();
    }


    void CollectionWidget::tick ( Ticks ticks )
    {
      for( ChildList::const_iterator it = childList_.begin(); it != childList_.end(); ++it )
        it->widget->tick( ticks );
    }


    void CollectionWidget::addChild ( const Position &origin, const Position &size, Widget *widget )
    {
      childList_.push_front( Child( origin, size, widget ) );
    }


    void CollectionWidget::removeChild ( Widget *widget )
    {
      for( ChildList::iterator it = childList_.begin(); it != childList_.end(); ++it )
      {
        if( it->widget != widget )
          continue;

        childList_.erase( it );
        return;
      }
    }


    CollectionWidget::ChildList::const_iterator
    CollectionWidget::findChild ( const Position &x ) const
    {
      for( ChildList::const_iterator it = childList_.begin(); it != childList_.end(); ++it )
      {
        if( it->contains( x ) )
          return it;
      }
      return childList_.end();
    }

  } // namespace Ash

} // namespace Dune
