// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_ASH_WIDGET_FLOATINGTEXT_HH
#define DUNE_ASH_WIDGET_FLOATINGTEXT_HH

#include <iostream>
#include <map>
#include <memory>

#include <dune/common/fvector.hh>

#include <dune/ash/text/box.hh>
#include <dune/ash/text/fontmanager.hh>
#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/opengl/texture.hh>
#include <dune/ash/widget/event.hh>
#include <dune/ash/widget/widget.hh>

namespace Dune
{

  namespace Ash
  {

    // FloatingTextWidget
    // ------------------

    class FloatingTextWidget
    : public Dune::Ash::Widget
    {
      typedef FloatingTextWidget ThisType;
      typedef std::map< std::string, Event * > EventMap;
      typedef std::list< ActiveBox > ActiveBoxList;

    public:
      typedef FieldVector< GLfloat, 3 > Color;

      // construction and destruction

      FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, std::istream &input );
      FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, std::istream &input );
      FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, const std::string &fileName );
      FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, const std::string &fileName );
      FloatingTextWidget ( GLfloat dpu, const FieldVector< GLfloat, 2 > &size );
      FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, std::istream &input );
      FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, std::istream &input );
      FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, const std::string &textPath, const std::string &fileName );
      FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size, const std::string &fileName );
      FloatingTextWidget ( const FieldVector< GLfloat, 2 > &dpu, const FieldVector< GLfloat, 2 > &size );

      ~FloatingTextWidget ();

      // widget interface

      void draw ();
      void mouseMove ( const Position &x, const Position &dx, Ticks ticks );
      void mouseButtonDown ( const Position &x, Ticks ticks );
      void mouseButtonUp ( const Position &x, Ticks ticks );

      // special methods

      void setText ( const std::string &textPath, std::istream &input );
      void setText ( std::istream &input );
      void setText ( const std::string &textPath, const std::string &fileName );
      void setText ( const std::string &fileName );

      Event *setEvent ( const std::string &id, Event *event );
      void deleteEvents ();

    private:
      void setupText();

      FontManager fontManager_;
      FieldVector< GLfloat, 2 > size_;
      Color background_;
      std::unique_ptr< ParBox > parBox_;
      Texture texture_;
      DisplayList displayList_;
      EventMap events_;
      ActiveBoxList activeBoxes_;
      ActiveBoxList::const_iterator activeBox_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_WIDGET_FLOATINGTEXT_HH
