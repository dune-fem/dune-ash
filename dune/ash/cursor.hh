#ifndef DUNE_ASH_CURSOR_HH
#define DUNE_ASH_CURSOR_HH

namespace Dune
{

  namespace Ash
  {

    // MouseCursor
    // -----------

    class MouseCursor
    {
      typedef MouseCursor This;

    public:
      explicit MouseCursor ( const std::string &fileName );
      ~MouseCursor ();

    private:
      MouseCursor ( const This & );
      const This &operator= ( const This & );

      friend void setCursor ( const MouseCursor &cursor );

      SDL_Cursor *cursor_;
    };



    // Auxiliary Functions
    // -------------------

    void setCursor ( const MouseCursor &cursor );

    void hideCursor ();
    void showCursor ();

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_CURSOR_HH
