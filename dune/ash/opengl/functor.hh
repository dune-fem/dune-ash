#ifndef DUNE_ASH_OPENGL_FUNCTOR_HH
#define DUNE_ASH_OPENGL_FUNCTOR_HH

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // Rectangle
    // ---------

    struct Rectangle
    {
      typedef Dune::FieldVector< GLfloat, 3 > Position;
      typedef Dune::FieldVector< GLfloat, 4 > Color;

      Rectangle ( const Position &upperLeft, const Position &lowerRight,
                  const Color &color = { 1.0f, 1.0f, 1.0f, 1.0f } )
      : upperLeft_( upperLeft ), lowerRight_( lowerRight ),
        color_( color )
      {}

      explicit Rectangle ( const Color &color = { 1.0f, 1.0f, 1.0f, 1.0f } )
      : upperLeft_( { 0, 0, 0 } ), lowerRight_( { 1, 1, 0 } ),
        color_( color )
      {}

      void operator() ()
      {
        glBegin( GL_QUADS );
        {
          glColor( color_ );
          glQuad( upperLeft_, lowerRight_ );
        }
        glEnd();
      }

    private:
      Position upperLeft_, lowerRight_;
      Color color_;
    };

 

    // RectangleOutline
    // ----------------

    struct RectangleOutline
    : public Rectangle
    {
      RectangleOutline ( const Position &upperLeft, const Position &lowerRight,
                         const Color &color = { 1.0f, 1.0f, 1.0f, 1.0f },
                         GLfloat lineWidth = 1.0f )
      : Rectangle( upperLeft, lowerRight, color ),
        lineWidth_( lineWidth )
      {}

      explicit RectangleOutline ( const Color &color, GLfloat lineWidth = 1.0f )
      : Rectangle( color ),
        lineWidth_( lineWidth )
      {}

      explicit RectangleOutline ( GLfloat lineWidth = 1.0f )
      : Rectangle(),
        lineWidth_( lineWidth )
      {}

      void operator() ()
      {
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        glLineWidth( lineWidth_ );
        static_cast< Rectangle & >( *this )();
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
      }

    private:
      GLfloat lineWidth_;
    };



    // AbstractFunctor
    // ---------------

    struct AbstractFunctor
    {
      virtual ~AbstractFunctor () {}
      virtual void operator() () = 0;
    };



    // FunctorWrapper
    // --------------

    template< class Functor >
    struct FunctorWrapper
    : public AbstractFunctor
    {
      FunctorWrapper ( Functor functor )
      : functor_( functor )
      {}

      void operator() () { functor_(); }

    private:
      Functor functor_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_OPENGL_DISPLAYLIST_HH
