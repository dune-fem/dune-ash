#ifndef DUNE_ASH_OPENGL_ERROR_HH
#define DUNE_ASH_OPENGL_ERROR_HH

#include <iostream>

#include <GL/gl.h>
#include <GL/glu.h>

#include <dune/common/exceptions.hh>

namespace Dune
{

  namespace Ash
  {

    struct GLError
    : public Exception
    {};

  } // namespace Ash

} // namespace Dune

#define DUNE_ASH_CHECK_GLERROR \
{ \
  const GLenum e = glGetError(); \
  switch( e ) \
  { \
  case GL_NO_ERROR: \
    break; \
  case GL_INVALID_ENUM: \
  case GL_INVALID_VALUE: \
  case GL_INVALID_OPERATION: \
    { \
      static bool warn = true; \
      if( warn ) \
        std::cerr << "OpenGL Error [" << __func__ << ":" << __FILE__ << ":" << __LINE__ << "]: " << gluErrorString( e ) << " (ignored)" << std::endl; \
      warn = false; \
    } \
    break; \
  default: \
    DUNE_THROW( Dune::Ash::GLError, gluErrorString( e ) ); \
  } \
}

#endif // #ifndef DUNE_ASH_OPENGL_ERROR_HH
