#ifndef DUNE_ASH_OPENGL_DISPLAYLIST_HH
#define DUNE_ASH_OPENGL_DISPLAYLIST_HH

#include <cassert>

#include <dune/ash/opengl/error.hh>
#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // DisplayList
    // -----------

    struct DisplayList
    {
      DisplayList ()
      {
        allocate();
      }

      template< class Functor >
      explicit DisplayList ( Functor functor )
      {
        allocate();
        compile( functor );
      }

      ~DisplayList ()
      {
        glDeleteLists( handle_, 1 );
        assert( glGetError() == GL_NO_ERROR );
      }

      void operator() () const { glCallList( handle_ ); }

      template< class Functor >
      void compile ( Functor functor )
      {
        assert( glGetError() == GL_NO_ERROR );
        glNewList( handle_, GL_COMPILE );
        functor();
        DUNE_ASH_CHECK_GLERROR;
        glEndList();
        DUNE_ASH_CHECK_GLERROR;
      }

    private:
      void allocate ()
      {
        handle_ = glGenLists( 1 );
        if( handle_ == GLuint( 0 ) )
        {
          DUNE_ASH_CHECK_GLERROR;
          DUNE_THROW( GLError, "Unable to allocate display list." );
        }
      }

      // forbid copying and assignment
      DisplayList ( const DisplayList & );
      const DisplayList &operator= ( const DisplayList & );

      GLuint handle_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_OPENGL_DISPLAYLIST_HH
