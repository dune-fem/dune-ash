#include <config.h>

#include <dune/ash/opengl/texture.hh>

namespace Dune
{

  namespace Ash
  {

    void Texture::load ( GLint internalFormat, const std::array< GLsizei, 2 > &size, GLint border, AbstractFunctor &functor )
    {
#if DUNE_ASH_GLCLEAR
      // save clear color
      FieldVector< GLfloat, 4 > saveClearColor;
      glGetClearColor( saveClearColor );
#endif // #if DUNE_ASH_GLCLEAR

      // save view port
      std::array< GLint, 2 > saveViewportPos;
      std::array< GLsizei, 2 > saveViewportSize;
      glGetViewport( saveViewportPos, saveViewportSize );

      // save projection matrix
      glMatrixMode( GL_PROJECTION );
      glPushMatrix();

      setup( internalFormat, size, border );

      std::array< GLint, 2 > vpPos;
      std::array< GLsizei, 2 > vpSize;
      for( vpPos[ 1 ] = 0; vpPos[ 1 ] < size[ 1 ]; )
      {
        vpSize[ 1 ] = std::min( saveViewportSize[ 1 ], size[ 1 ] - vpPos[ 1 ] );

        const GLfloat top = GLfloat( vpPos[ 1 ] ) / GLfloat( size[ 1 ] );
        const GLfloat bottom = GLfloat( vpPos[ 1 ] + vpSize[ 1 ] ) / GLfloat( size[ 1 ] );

        for( vpPos[ 0 ] = 0; vpPos[ 0 ] < size[ 0 ] ; )
        {
          vpSize[ 0 ] = std::min( saveViewportSize[ 0 ], size[ 0 ] - vpPos[ 0 ] );

          const GLfloat left = GLfloat( vpPos[ 0 ] ) / GLfloat( size[ 0 ] );
          const GLfloat right = GLfloat( vpPos[ 0 ] + vpSize[ 0 ] ) / GLfloat( size[ 0 ] );

          // set view port and projection matrix
          glViewport( saveViewportPos, vpSize );
          glMatrixMode( GL_PROJECTION );
          glLoadIdentity();
          glOrtho( left, right, top, bottom, 0.0, 1.0 );

          // clear frame buffer and render
#if DUNE_ASH_GLCLEAR
          glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
          glClear( GL_COLOR_BUFFER_BIT );
#else // #if DUNE_ASH_GLCLEAR
          glMatrixMode( GL_MODELVIEW );
          glPushMatrix();
          glLoadIdentity();
          glDisable( GL_BLEND );
          Rectangle( { 0.0f, 0.0f, 0.0f, 0.0f } )();
          glEnable( GL_BLEND );
          glPopMatrix();
#endif // #else // #if DUNE_ASH_GLCLEAR
          functor();

          // copy the frame buffer content into the texture
          select();
          glCopyTexSubImage2D( GL_TEXTURE_2D, 0, vpPos[ 0 ], vpPos[ 1 ], saveViewportPos[ 0 ], saveViewportPos[ 1 ], vpSize[ 0 ], vpSize[ 1 ] );
          DUNE_ASH_CHECK_GLERROR;

          vpPos[ 0 ] += vpSize[ 0 ];
        }
        vpPos[ 1 ] += vpSize[ 1 ];
      }

      // restore projection matrix
      glMatrixMode( GL_PROJECTION );
      glPopMatrix();

      // restore view port and clear color
      glViewport( saveViewportPos, saveViewportSize );
#if DUNE_ASH_GLCLEAR
      glClearColor( saveClearColor );
#endif // #if DUNE_ASH_GLCLEAR
      DUNE_ASH_CHECK_GLERROR;
    }

  } // namespace Ash

} // namespace Dune
