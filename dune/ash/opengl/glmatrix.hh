#ifndef DUNE_ASH_OPENGL_GLMATRIX_HH
#define DUNE_ASH_OPENGL_GLMATRIX_HH

#include <cassert>

#include <dune/common/densematrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/typetraits.hh>

#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // Internal Forward Declarations
    // -----------------------------

    template< class K >
    class GLMatrixRow;

    template< class K >
    class GLMatrix;

  } // namespace Ash



  // DenseMatVecTraits for GLMatrixRow
  // ---------------------------------

  template< class K >
  struct DenseMatVecTraits< Ash::GLMatrixRow< K > >
  {
    typedef Ash::GLMatrixRow< K > derived_type;
    typedef K *container_type;
    typedef K value_type;
    typedef std::size_t size_type;
  };



  // DenseMatVecTraits for GLMatrix< K >
  // -----------------------------------

  template< class K >
  struct DenseMatVecTraits< Ash::GLMatrix< K > >
  {
    typedef Ash::GLMatrix< K > derived_type;
    typedef K container_type[ 16 ];
    typedef K value_type;
    typedef std::size_t size_type;
    typedef FieldVector< K, 4 > row_type;

    typedef Ash::GLMatrixRow< K > row_reference;
    typedef Ash::GLMatrixRow< const K > const_row_reference;
  };



  namespace Ash
  {

    // GLMatrixRow
    // -----------

    template< class K >
    class GLMatrixRow
    : public DenseVector< GLMatrixRow< K > >
    {
      typedef DenseVector< GLMatrixRow< K > > Base;

    public:
      typedef typename Base::size_type size_type;

      explicit GLMatrixRow ( K *values )
      : values_( values )
      {}

      GLMatrixRow ( const GLMatrixRow< typename remove_const< K >::type > &other )
      : values_( other.values_ )
      {}

      using Base::operator=;

      const GLMatrixRow &operator= ( const GLMatrixRow &other )
      {
        for( size_type i = 0; i < vec_size(); ++i )
          vec_access( i ) = other[ i ];
        return *this;
      }

      template< class Impl >
      const GLMatrixRow &operator= ( const DenseVector< Impl > &other )
      {
        assert( other.size() == vec_size() );
        for( size_type i = 0; i < vec_size(); ++i )
          vec_access( i ) = other[ i ];
        return *this;
      }


      // interface for DenseVector

      size_type vec_size () const { return 4; }

      const K &vec_access ( size_type i ) const
      {
        assert( i < vec_size() );
        return values_[ 4*i ];
      }

      K &vec_access ( size_type i )
      {
        assert( i < vec_size() );
        return values_[ 4*i ];
      }

    private:
      K *values_;
    };



    // GLMatrix
    // --------

    template< class K >
    class GLMatrix
    : public DenseMatrix< GLMatrix< K > >
    {
      typedef DenseMatrix< GLMatrix< K > > Base;

    public:
      typedef typename Base::size_type size_type;
      typedef typename Base::row_reference row_reference;
      typedef typename Base::const_row_reference const_row_reference;

      GLMatrix ()
      {}

      template< class Impl >
      GLMatrix ( const DenseMatrix< Impl > &other )
      {
        assert( (other.rows() == 4) && (other.cols() == 4) );
        for( int j = 0; j < 4; ++j )
        {
          for( int i = 0; i < 4; ++i )
            values_[ j*4 + i ] = other[ i ][ j ];
        }
      }

      operator FieldMatrix< K, 4, 4 > () const
      {
        FieldMatrix< K, 4, 4 > m;
        for( int j = 0; j < 4; ++j )
        {
          for( int i = 0; i < 4; ++i )
            m[ i ][ j ] = values_[ j*4 + i ];
        }
      }


      // auxiliary OpenGL functions

      friend void glLoadMatrix ( const GLMatrix< K > &m )
      {
        glLoadMatrix( const_cast< K * >( m.values_ ) );
      }

      friend void glLoadTransposeMatrix ( const GLMatrix< K > &m )
      {
        glLoadTransposeMatrix( const_cast< K * >( m.values_ ) );
      }

      friend void glMultMatrix ( const GLMatrix< K > &m )
      {
        glMultMatrix( const_cast< K * >( m.values_ ) );
      }

      friend void glGetMatrix ( GLMatrix< K > &m )
      {
        glGet ( glMatrixMode(), m.values_ );
      }


      // interface for DenseMatrix

      size_type mat_rows () const { return 4; }
      size_type mat_cols () const { return 4; }

      const_row_reference mat_access ( size_type i ) const
      {
        assert( i < 4 );
        return const_row_reference( values_ + i );
      }

      row_reference mat_access ( size_type i )
      {
        assert( i < 4 );
        return row_reference( values_ + i );
      }

    private:
      K values_[ 16 ];
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_OPENGL_GLMATRIX_HH
