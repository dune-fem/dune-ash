#ifndef DUNE_ASH_OPENGL_OPENGL_HH
#define DUNE_ASH_OPENGL_OPENGL_HH

#include <array>

#include <dune/common/fvector.hh>

#include <GL/gl.h>

namespace Dune
{

  namespace Ash
  {

    // glGet
    // -----

    inline void glGet ( GLenum pname, GLboolean *params )
    {
      glGetBooleanv( pname, params );
    }

    inline void glGet ( GLenum pname, GLint *params )
    {
      glGetIntegerv( pname, params );
    }

    inline void glGet ( GLenum pname, GLfloat *params )
    {
      glGetFloatv( pname, params );
    }

    inline void glGet ( GLenum pname, GLdouble *params )
    {
      glGetDoublev( pname, params );
    }



    // glColor
    // -------

    inline void glColor ( GLfloat red, GLfloat green, GLfloat blue )
    {
      glColor3f( red, green, blue );
    }

    inline void glColor ( GLdouble red, GLdouble green, GLdouble blue )
    {
      glColor3d( red, green, blue );
    }

    inline void glColor ( GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha )
    {
      glColor4f( red, green, blue, alpha );
    }

    inline void glColor ( GLdouble red, GLdouble green, GLdouble blue, GLdouble alpha )
    {
      glColor4d( red, green, blue, alpha );
    }

    template< class K >
    inline void glColor ( const FieldVector< K, 3 > &color )
    {
      glColor( color[ 0 ], color[ 1 ], color[ 2 ] );
    }

    template< class K >
    inline void glColor ( const FieldVector< K, 4 > &color )
    {
      glColor( color[ 0 ], color[ 1 ], color[ 2 ], color[ 3 ] );
    }



    // glClearColor
    // ------------

    inline void glGetClearColor ( GLfloat &red, GLfloat &green, GLfloat &blue, GLfloat &alpha )
    {
      GLfloat params[ 4 ];
      glGet( GL_COLOR_CLEAR_VALUE, params );
      red = params[ 0 ];
      green = params[ 1 ];
      blue = params[ 2 ];
      alpha = params[ 3 ];
    }

    inline void glGetClearColor ( FieldVector< GLfloat, 4 > &color )
    {
      glGetClearColor( color[ 0 ], color[ 1 ], color[ 2 ], color[ 3 ] );
    }

    inline void glClearColor ( GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha )
    {
      ::glClearColor( red, green, blue, alpha );
    }

    inline void glClearColor ( const FieldVector< GLfloat, 4 > &color )
    {
      glClearColor( color[ 0 ], color[ 1 ], color[ 2 ], color[ 3 ] );
    }



    // glLoadMatrix
    // ------------

    inline void glLoadMatrix ( const GLfloat *m )
    {
      glLoadMatrixf( m );
    }

    inline void glLoadMatrix ( const GLdouble *m )
    {
      glLoadMatrixd( m );
    }



    // glLoadTransposeMatrix
    // ---------------------

    inline void glLoadTransposeMatrix ( const GLfloat *m )
    {
      glLoadTransposeMatrixf( m );
    }

    inline void glLoadTransposeMatrix ( const GLdouble *m )
    {
      glLoadTransposeMatrixd( m );
    }



    // glMatrixMode
    // ------------

    using ::glMatrixMode;

    inline GLenum glMatrixMode ()
    {
      GLint v;
      glGet( GL_MATRIX_MODE, &v );
      return GLenum( v );
    }



    // glMultMatrix
    // ------------

    inline void glMultMatrix ( GLfloat *m )
    {
      glMultMatrixf( m );
    }

    inline void glMultMatrix ( GLdouble *m )
    {
      glMultMatrixd( m );
    }



    // glScale
    // -------

    inline void glScale ( GLfloat x, GLfloat y, GLfloat z )
    {
      glScalef( x, y, z );
    }

    inline void glScale ( GLdouble x, GLdouble y, GLdouble z )
    {
      glScaled( x, y, z );
    }

    template< class K >
    inline void glScale ( const FieldVector< K, 3 > &x )
    {
      glScale( x[ 0 ], x[ 1 ], x[ 2 ] );
    }

    template< class K >
    inline void glScale ( const FieldVector< K, 2 > &x )
    {
      glScale( x[ 0 ], x[ 1 ], K( 1 ) );
    }



    // glScissorBox
    // ------------

    inline void glGetScissorBox ( GLint &x, GLint &y, GLsizei &width, GLsizei &height )
    {
      GLint params[ 4 ];
      glGet( GL_SCISSOR_BOX, params );
      x = params[ 0 ];
      y = params[ 1 ];
      width = params[ 2 ];
      height = params[ 3 ];
    }

    inline void glGetScissorBox ( std::array< GLint, 2 > &x, std::array< GLsizei, 2 > &size )
    {
      glGetScissorBox( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glGetScissorBox ( FieldVector< GLint, 2 > &x, FieldVector< GLsizei, 2 > &size )
    {
      glGetScissorBox( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glScissorBox ( GLint x, GLint y, GLsizei width, GLsizei height )
    {
      ::glScissor( x, y, width, height );
    }

    inline void glScissorBox ( const std::array< GLint, 2 > &x, const std::array< GLsizei, 2 > &size )
    {
      glScissorBox( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glScissorBox ( const FieldVector< GLint, 2 > &x, const FieldVector< GLsizei, 2 > &size )
    {
      glScissorBox( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glScissorBox ( const FieldVector< GLint, 2 > &x, const std::array< GLsizei, 2 > &size )
    {
      glScissorBox( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glScissorBox ( const std::array< GLint, 2 > &x, const FieldVector< GLsizei, 2 > &size )
    {
      glScissorBox( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }



    // glTexCoord
    // ----------

    inline void glTexCoord ( GLfloat x, GLfloat y )
    {
      glTexCoord2f( x, y );
    }

    inline void glTexCoord ( GLdouble x, GLdouble y )
    {
      glTexCoord2d( x, y );
    }

    template< class K >
    inline void glTexCoord ( const FieldVector< K, 2 > &x )
    {
      glTexCoord( x[ 0 ], x[ 1 ] );
    }



    // glTranslate
    // -----------

    inline void glTranslate ( GLfloat x, GLfloat y, GLfloat z )
    {
      glTranslatef( x, y, z );
    }

    inline void glTranslate ( GLdouble x, GLdouble y, GLdouble z )
    {
      glTranslated( x, y, z );
    }

    template< class K >
    inline void glTranslate ( const FieldVector< K, 3 > &x )
    {
      glTranslate( x[ 0 ], x[ 1 ], x[ 2 ] );
    }

    template< class K >
    inline void glTranslate ( const FieldVector< K, 2 > &x )
    {
      glTranslate( x[ 0 ], x[ 1 ], K( 0 ) );
    }



    // glVertex
    // --------

    inline void glVertex ( GLfloat x, GLfloat y )
    {
      glVertex2f( x, y );
    }

    inline void glVertex ( GLfloat x, GLfloat y, GLfloat z )
    {
      glVertex3f( x, y, z );
    }

    inline void glVertex ( GLdouble x, GLdouble y )
    {
      glVertex2d( x, y );
    }

    inline void glVertex ( GLdouble x, GLdouble y, GLdouble z )
    {
      glVertex3d( x, y, z );
    }

    template< class K >
    inline void glVertex ( const FieldVector< K, 2 > &x )
    {
      glVertex( x[ 0 ], x[ 1 ] );
    }

    template< class K >
    inline void glVertex ( const FieldVector< K, 3 > &x )
    {
      glVertex( x[ 0 ], x[ 1 ], x[ 2 ] );
    }



    // glViewport
    // ----------

    inline std::array< GLsizei, 2 > glMaxViewportDims ()
    {
      std::array< GLsizei, 2 > maxDims;
      glGet( GL_MAX_VIEWPORT_DIMS, &(maxDims[ 0 ]) );
      return maxDims;
    }

    inline std::array< GLsizei, 2 > glViewportDims ()
    {
      GLsizei params[ 4 ];
      glGet( GL_VIEWPORT, params );

      std::array< GLsizei, 2 > dims;
      for( int i = 0; i < 2; ++i )
        dims[ i ] = params[ i+2 ];
      return dims;
    }

    inline void glGetViewport ( GLint &x, GLint &y, GLsizei &width, GLsizei &height )
    {
      GLint params[ 4 ];
      glGet( GL_VIEWPORT, params );
      x = params[ 0 ];
      y = params[ 1 ];
      width = params[ 2 ];
      height = params[ 3 ];
    }

    inline void glGetViewport ( std::array< GLint, 2 > &x, std::array< GLsizei, 2 > &size )
    {
      glGetViewport( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glGetViewport ( FieldVector< GLint, 2 > &x, FieldVector< GLsizei, 2 > &size )
    {
      glGetViewport( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glViewport ( GLint x, GLint y, GLsizei width, GLsizei height )
    {
      ::glViewport( x, y, width, height );
    }

    inline void glViewport ( const std::array< GLint, 2 > &x, const std::array< GLsizei, 2 > &size )
    {
      glViewport( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glViewport ( const FieldVector< GLint, 2 > &x, const FieldVector< GLsizei, 2 > &size )
    {
      glViewport( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glViewport ( const FieldVector< GLint, 2 > &x, const std::array< GLsizei, 2 > &size )
    {
      glViewport( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }

    inline void glViewport ( const std::array< GLint, 2 > &x, const FieldVector< GLsizei, 2 > &size )
    {
      glViewport( x[ 0 ], x[ 1 ], size[ 0 ], size[ 1 ] );
    }



    // glQuad
    // ------

    template< class T >
    inline void glQuad ( const FieldVector< T, 2 > &a, const FieldVector< T, 2 > &b )
    {
      glVertex( a[ 0 ], a[ 1 ] );
      glVertex( b[ 0 ], a[ 1 ] );
      glVertex( b[ 0 ], b[ 1 ] );
      glVertex( a[ 0 ], b[ 1 ] );
    }

    template< class T >
    inline void glQuad ( const FieldVector< T, 3 > &a, const FieldVector< T, 3 > &b )
    {
      assert( a[ 2 ] == b[ 2 ] );
      glVertex( a[ 0 ], a[ 1 ], a[ 2 ] );
      glVertex( b[ 0 ], a[ 1 ], a[ 2 ] );
      glVertex( b[ 0 ], b[ 1 ], a[ 2 ] );
      glVertex( a[ 0 ], b[ 1 ], a[ 2 ] );
    }



    // GLTypeIdentifier
    // ----------------

    template< class T >
    struct GLTypeIdentifier;

    template<>
    struct GLTypeIdentifier< GLbyte >
    {
      static const GLenum value = GL_BYTE;
    };

    template<>
    struct GLTypeIdentifier< GLshort >
    {
      static const GLenum value = GL_SHORT;
    };

    template<>
    struct GLTypeIdentifier< GLint >
    {
      static const GLenum value = GL_INT;
    };

    template<>
    struct GLTypeIdentifier< GLfloat >
    {
      static const GLenum value = GL_FLOAT;
    };

    template<>
    struct GLTypeIdentifier< GLdouble >
    {
      static const GLenum value = GL_DOUBLE;
    };

    template<>
    struct GLTypeIdentifier< GLubyte >
    {
      static const GLenum value = GL_UNSIGNED_BYTE;
    };

    template<>
    struct GLTypeIdentifier< GLushort >
    {
      static const GLenum value = GL_UNSIGNED_SHORT;
    };

    template<>
    struct GLTypeIdentifier< GLuint >
    {
      static const GLenum value = GL_UNSIGNED_INT;
    };

    template< class T, std::size_t N >
    struct GLTypeIdentifier< std::array< T, N > >
    {
      static const GLenum value = GLTypeIdentifier< T >::value;
    };



    // GLType
    // ------

    template< GLenum id >
    struct GLType;

    template<>
    struct GLType< GL_BYTE >
    {
      typedef GLbyte type;
    };

    template<>
    struct GLType< GL_SHORT >
    {
      typedef GLshort type;
    };

    template<>
    struct GLType< GL_INT >
    {
      typedef GLint type;
    };

    template<>
    struct GLType< GL_FLOAT >
    {
      typedef GLfloat type;
    };

    template<>
    struct GLType< GL_DOUBLE >
    {
      typedef GLdouble type;
    };

    template<>
    struct GLType< GL_UNSIGNED_BYTE >
    {
      typedef GLubyte type;
    };

    template<>
    struct GLType< GL_UNSIGNED_SHORT >
    {
      typedef GLushort type;
    };

    template<>
    struct GLType< GL_UNSIGNED_INT >
    {
      typedef GLuint type;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_OPENGL_OPENGL_HH
