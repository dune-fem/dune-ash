#ifndef DUNE_ASH_OPENGL_TEXTURE_HH
#define DUNE_ASH_OPENGL_TEXTURE_HH

#include <array>

#include <GL/gl.h>

#include <dune/common/fvector.hh>
#include <dune/ash/opengl/error.hh>
#include <dune/ash/opengl/functor.hh>
#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // Texture
    // -------

    struct Texture
    {
      Texture () { glGenTextures( 1, &handle_ ); }
      ~Texture () { glDeleteTextures( 1, &handle_ ); }

      void select () const { glBindTexture( GL_TEXTURE_2D, handle_ ); }

      template< class T >
      void load ( GLint internalFormat, const std::array< GLsizei, 1 > &size, GLint border, GLenum format, const T *data )
      {
        glBindTexture( GL_TEXTURE_1D, handle_ );
        glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage1D( GL_TEXTURE_1D, 0, internalFormat, size[ 0 ], border, format, GLTypeIdentifier< T >::value, data );
        DUNE_ASH_CHECK_GLERROR;
      }

      template< class T >
      void load ( GLint internalFormat, const std::array< GLsizei, 2 > &size, GLint border, GLenum format, const T *data )
      {
        glBindTexture( GL_TEXTURE_2D, handle_ );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage2D( GL_TEXTURE_2D, 0, internalFormat, size[ 0 ], size[ 1 ], border, format, GLTypeIdentifier< T >::value, data );
        DUNE_ASH_CHECK_GLERROR;
      }

      template< class T >
      void load ( GLint internalFormat, const std::array< GLsizei, 3 > &size, GLint border, GLenum format, const T *data )
      {
        glBindTexture( GL_TEXTURE_3D, handle_ );
        glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage3D( GL_TEXTURE_3D, 0, internalFormat, size[ 0 ], size[ 1 ], size[ 2 ], border, format, GLTypeIdentifier< T >::value, data );
        DUNE_ASH_CHECK_GLERROR;
      }

      void load ( GLint internalFormat, const std::array< GLsizei, 2 > &size, GLint border, AbstractFunctor &functor );

      template< class Functor >
      void load ( GLint internalFormat, const std::array< GLsizei, 2 > &size, GLint border, Functor functor )
      {
        FunctorWrapper< Functor > functorWrapper( functor );
        load( internalFormat, size, border, static_cast< AbstractFunctor & >( functorWrapper ) );
      }

      void setup ( GLint internalFormat, const std::array< GLsizei, 1 > &size, GLint border )
      {
        glBindTexture( GL_TEXTURE_1D, handle_ );
        glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage1D( GL_TEXTURE_1D, 0, internalFormat, size[ 0 ], border, GL_ALPHA, GL_UNSIGNED_BYTE, nullptr );
        DUNE_ASH_CHECK_GLERROR;
      }

      void setup ( GLint internalFormat, const std::array< GLsizei, 2 > &size, GLint border )
      {
        glBindTexture( GL_TEXTURE_2D, handle_ );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage2D( GL_TEXTURE_2D, 0, internalFormat, size[ 0 ], size[ 1 ], border, GL_ALPHA, GL_UNSIGNED_BYTE, nullptr );
        DUNE_ASH_CHECK_GLERROR;
      }

      void setup ( GLint internalFormat, const std::array< GLsizei, 3 > &size, GLint border )
      {
        glBindTexture( GL_TEXTURE_3D, handle_ );
        glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage3D( GL_TEXTURE_3D, 0, internalFormat, size[ 0 ], size[ 1 ], size[ 2 ], border, GL_ALPHA, GL_UNSIGNED_BYTE, nullptr );
        DUNE_ASH_CHECK_GLERROR;
      }

    private:
      // forbid copying and assignment
      Texture ( const Texture & );
      const Texture &operator= ( const Texture & );

      GLuint handle_;
    };



    // BillBoard
    // ---------

    struct BillBoard
    {
      typedef Dune::FieldVector< GLfloat, 3 > Position;
      typedef Dune::FieldVector< GLfloat, 2 > TexPosition;
      typedef Dune::FieldVector< GLfloat, 4 > Color;

      BillBoard ( const Position &upperLeft, const Position &lowerRight,
                  const Texture &texture,
                  const TexPosition &texUpperLeft = { 0, 0 },
                  const TexPosition &texLowerRight = { 1, 1 },
                  const Color &color = { 1, 1, 1, 1 } )
      : upperLeft_( upperLeft ), lowerRight_( lowerRight ),
        texture_( texture ),
        texUpperLeft_( texUpperLeft ), texLowerRight_( texLowerRight ),
        color_( color )
      {}

      BillBoard ( const Position &upperLeft, const Position &lowerRight,
                  const Texture &texture, const Color &color )
      : upperLeft_( upperLeft ), lowerRight_( lowerRight ),
        texture_( texture ),
        texUpperLeft_( { 0, 0 } ), texLowerRight_( { 1, 1 } ),
        color_( color )
      {}

      BillBoard ( const Texture &texture,
                  const TexPosition &texUpperLeft = { 0, 0 },
                  const TexPosition &texLowerRight = { 1, 1 },
                  const Color &color = { 1, 1, 1, 1 } )
      : upperLeft_( { 0, 0, 1 } ), lowerRight_( { 1, 1, 1 } ),
        texture_( texture ),
        texUpperLeft_( texUpperLeft ), texLowerRight_( texLowerRight ),
        color_( color )
      {}

      BillBoard ( const Texture &texture, const Color &color )
      : upperLeft_( { 0, 0, 1 } ), lowerRight_( { 1, 1, 1 } ),
        texture_( texture ),
        texUpperLeft_( { 0, 0 } ), texLowerRight_( { 1, 1 } ),
        color_( color )
      {}

      void operator() ()
      {
        glEnable( GL_TEXTURE_2D );

        glColor( color_ );
        texture_.select();
        glBegin( GL_QUADS );
        {
          glTexCoord( texUpperLeft_[ 0 ], texUpperLeft_[ 1 ] );
          glVertex( upperLeft_[ 0 ], upperLeft_[ 1 ]);
          glTexCoord( texLowerRight_[ 0 ], texUpperLeft_[ 1 ] );
          glVertex( lowerRight_[ 0 ], upperLeft_[ 1 ] );
          glTexCoord( texLowerRight_[ 0 ], texLowerRight_[ 1 ] );
          glVertex( lowerRight_[ 0 ], lowerRight_[ 1 ] );
          glTexCoord( texUpperLeft_[ 0 ], texLowerRight_[ 1 ] );
          glVertex( upperLeft_[ 0 ], lowerRight_[ 1 ] );
        }
        glEnd();

        glDisable( GL_TEXTURE_2D );
      }

    private:
      Position upperLeft_, lowerRight_;
      const Texture &texture_;
      TexPosition texUpperLeft_, texLowerRight_;
      Color color_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_OPENGL_TEXTURE_HH
