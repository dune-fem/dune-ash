// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <SDL_image.h>

#include <dune/ash/image.hh>

namespace Dune
{

  namespace Ash
  {

    // SDLImage
    // --------

    struct SDLImage
    {
      SDLImage ();
      ~SDLImage () { IMG_Quit(); }

      SDL_Surface *load ( const std::string &file ) { return IMG_Load( file.c_str() ); }
    };



    // Single Instance of SDLImage
    // ---------------------------

    SDLImage sdlImage;



    // Implementation of SDLImage
    // --------------------------

    SDLImage::SDLImage ()
    {
      const int flags = IMG_INIT_JPG | IMG_INIT_PNG;
      if( IMG_Init( flags ) != flags  )
      {
        std::cerr << "SDL_image: " << IMG_GetError() << ".";
        abort();
      }

      SDL_version vCompiled;
      SDL_IMAGE_VERSION( &vCompiled );
      const SDL_version *vLinked = IMG_Linked_Version();
      if( vLinked->major != vCompiled.major )
        std::cerr << "Warning: A completely different version of SDL_image was used at compile time." << std::endl;
      if( vLinked->minor < vCompiled.minor )
        std::cerr << "Warning: An new version of SDL_image was used at compile time." << std::endl;
    }



    // loadImage
    // ---------

    std::array< GLsizei, 2 > loadImage ( const std::string &file, Texture &texture )
    {
      SDL_Surface *surface = sdlImage.load( file.c_str() );
      if( !surface )
        DUNE_THROW( Exception, "Unable to load image from '" << file << "'." );

      std::array< GLsizei, 2 > size;
      size[ 0 ] = surface->w;
      size[ 1 ] = surface->h;

      GLenum format = (surface->format->Amask ? GL_RGBA : GL_RGB);
      texture.load( GL_RGBA, size, 0, format, (const GLubyte *)surface->pixels );

      SDL_FreeSurface( surface );
      return size;
    }

  } // namespace Ash

} // namespace Dune
