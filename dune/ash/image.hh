#ifndef DUNE_ASH_IMAGE_HH
#define DUNE_ASH_IMAGE_HH

#include <array>

#include <dune/ash/opengl/texture.hh>

namespace Dune
{

  namespace Ash
  {

    std::array< GLsizei, 2 > loadImage ( const std::string &file, Texture &texture );

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_IMAGE_HH
