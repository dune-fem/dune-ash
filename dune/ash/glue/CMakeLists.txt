set( HEADERS
  gridpart.hh
  l2projection.hh
  levelgridpart.hh
)

install( FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/ash/glue )
