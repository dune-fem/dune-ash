#ifndef DUNE_ASH_GLUE_GRIDPART_HH
#define DUNE_ASH_GLUE_GRIDPART_HH

#include <dune/ash/glue/levelgridpart.hh>

namespace Dune
{

  namespace Ash
  {

    // GridPartGlue
    // ------------

    template< class InsideGridPart, class OutsideGridPart >
    class GridPartGlue;



    // GridPartGlue for LevelGridPart
    // ------------------------------

    template< class InsideGrid, class OutsideGrid >
    class GridPartGlue< Fem::LevelGridPart< InsideGrid >, Fem::LevelGridPart< OutsideGrid > >
    : public LevelGridPartGlue< InsideGrid, OutsideGrid >
    {
      typedef GridPartGlue< Fem::LevelGridPart< InsideGrid >, Fem::LevelGridPart< OutsideGrid > > ThisType;
      typedef LevelGridPartGlue< InsideGrid, OutsideGrid > BaseType;

    public:
      typedef typename BaseType::InsideGridPartType InsideGridPartType;
      typedef typename BaseType::OutsideGridPartType OutsideGridPartType;

      GridPartGlue ( const InsideGridPartType &insideGridPart,
                     const OutsideGridPartType &outsideGridPart )
      : BaseType( insideGridPart, outsideGridPart )
      {}
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_GLUE_GRIDPART_HH
