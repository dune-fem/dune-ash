#ifndef DUNE_ASH_GLUE_LEVELGRIDPART_HH
#define DUNE_ASH_GLUE_LEVELGRIDPART_HH

#include <algorithm>
#include <limits>

#include <dune/grid/spgrid/grid.hh>

#include <dune/fem/gridpart/levelgridpart.hh>

namespace Dune
{

  namespace Ash
  {

    // LevelGridPartGlue
    // -----------------

    template< class InsideGrid, class OutsideGrid >
    class LevelGridPartGlue;



    // LevelGridPartGlue for SPGrid of equal dimension
    // -----------------------------------------------

    template< class ct, int dim, template< int > class RefInside, template< int > class RefOutside, class Comm >
    class LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >
    {
      typedef SPGrid< ct, dim, RefInside, Comm > InsideGridType;
      typedef SPGrid< ct, dim, RefOutside, Comm > OutsideGridType;

      typedef typename InsideGridType::LevelGridView InsideGridViewType;
      typedef typename OutsideGridType::LevelGridView OutsideGridViewType;

    public:
      class Geometry;
      class Intersection;
      class IntersectionIterator;

      typedef ct ctype;

      static const int dimension = dim;

      typedef Fem::LevelGridPart< InsideGridType > InsideGridPartType;
      typedef Fem::LevelGridPart< OutsideGridType > OutsideGridPartType;

      typedef Intersection IntersectionType;
      typedef IntersectionIterator IntersectionIteratorType;

      LevelGridPartGlue ( const InsideGridPartType &insideGridPart,
                          const OutsideGridPartType &outsideGridPart )
      : insideGridView_( insideGridPart.grid().levelGridView( insideGridPart.level() ) ),
        outsideGridView_( outsideGridPart.grid().levelGridView( outsideGridPart.level() ) )
      {}

      IntersectionIteratorType begin () const;
      IntersectionIteratorType end () const;

    private:
      InsideGridViewType insideGridView_;
      OutsideGridViewType outsideGridView_;
    };



    // LevelGridPartGlue::Geometry for SPGrid of equal dimension
    // ---------------------------------------------------------

    template< class ct, int dim, template< int > class RefInside, template< int > class RefOutside, class Comm >
    class LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >::Geometry
      : public SPBasicGeometry< dim, dim, InsideGridType, LevelGridPartGlue< InsideGridType, OutsideGridType >::Geometry >
    {
      typedef SPBasicGeometry< dim, dim, InsideGridType, LevelGridPartGlue< InsideGridType, OutsideGridType >::Geometry > Base;

    public:
      typedef typename Base::GeometryCache GeometryCache;
      typedef typename Base::GlobalVector GlobalVector;

      Geometry ( const GlobalVector &h, const GlobalVector &origin )
      : geometryCache_( h, SPDirection<dim>( (1u << dim)-1u ) ),
        origin_( origin )
      {}

      GlobalVector origin () const { return origin_; }
      const GeometryCache &geometryCache () const { return geometryCache_; }

    private:
      GeometryCache geometryCache_;
      GlobalVector origin_;
    };



    // LevelGridPartGlue::Intersection for SPGrid of equal dimension
    // -------------------------------------------------------------

    template< class ct, int dim, template< int > class RefInside, template< int > class RefOutside, class Comm >
    class LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >::Intersection
    {
      friend class LevelGridPartGlue< InsideGridType, OutsideGridType >::IntersectionIterator;

    public:
      static const int mydimension = dimension;

      typedef typename InsideGridViewType::template Codim< 0 >::Entity InsideEntity;
      typedef typename OutsideGridViewType::template Codim< 0 >::Entity OutsideEntity;

      typedef LevelGridPartGlue< InsideGridType, OutsideGridType >::Geometry Geometry;
      typedef LevelGridPartGlue< InsideGridType, OutsideGridType >::Geometry LocalGeometry;

    private:
      typedef typename InsideGridViewType::template Codim< 0 >::Iterator InsideIterator;

      typedef typename InsideGridViewType::Implementation::GridLevel InsideGridLevel;
      typedef typename OutsideGridViewType::Implementation::GridLevel OutsideGridLevel;

      typedef typename InsideEntity::Implementation::EntityInfo InsideEntityInfo;
      typedef typename OutsideEntity::Implementation::EntityInfo OutsideEntityInfo;

      explicit Intersection ( const InsideIterator &inside,
                              const OutsideGridLevel &outsideGridLevel )
      : inside_( inside ),
        outside_( outsideGridLevel )
      {}

      const InsideGridLevel &insideGridLevel () const { return inside_.impl().gridLevel(); }
      const OutsideGridLevel &outsideGridLevel () const { return outside_.gridLevel(); }

      const InsideEntityInfo &insideEntityInfo () const { return inside_.impl().entityInfo(); }
      const OutsideEntityInfo &outsideEntityInfo () const { return outside_; }
      OutsideEntityInfo &outsideEntityInfo () { return outside_; }

      bool equals ( const Intersection &other ) const
      {
        return (inside_ == other.inside_) && outside_.equals( other.outside_ );
      }

    public:
      bool conforming () const { return false; }

      int indexInInside () const { return 0; }
      int indexInOutside () const { return 0; }

      bool neighbor () const { return true; }
      bool self () const { return true; }

      InsideEntity inside () const { return *inside_; }
      OutsideEntity outside () const { return OutsideEntity( typename OutsideEntity::Implementation( outside_) ); }

      GeometryType type () const
      {
        typedef typename GenericGeometry::CubeTopology< mydimension >::type Topology;
        return GeometryType( Topology() );
      }

      Geometry geometry () const
      {
        typename Geometry::GlobalCoordinate origin, h;
        for( int i = 0; i < dimension; ++i )
        {
          const long int wIn = insideGridLevel().globalMesh().width( i );
          const long int wOut = outsideGridLevel().globalMesh().width( i );
          const long int pIn = insideEntityInfo().id()[ i ] / 2;
          const long int pOut = outsideEntityInfo().id()[ i ] / 2;

          const long int begin = std::max( pIn*wOut, pOut*wIn );
          const long int end = std::min( (pIn+1)*wOut, (pOut+1)*wIn );

          origin[ i ] = ctype( begin ) / ctype( wIn*wOut );
          origin[ i ] += insideGridLevel().domain().cube().origin()[ i ];

          h[ i ] = ctype( end - begin ) / ctype( wIn*wOut );
          h[ i ] *= insideGridLevel().domain().cube().width()[ i ];
        }
        return LocalGeometry( h, origin );
      }

      LocalGeometry geometryInInside () const
      {
        typename LocalGeometry::GlobalCoordinate origin, h;
        for( int i = 0; i < mydimension; ++i )
        {
          const long int wIn = insideGridLevel().globalMesh().width( i );
          const long int wOut = outsideGridLevel().globalMesh().width( i );
          const long int pIn = insideEntityInfo().id()[ i ] / 2;
          const long int pOut = outsideEntityInfo().id()[ i ] / 2;

          const long int begin = std::max( pOut*wIn - pIn*wOut, 0l );
          const long int end = std::min( (pOut*wIn - pIn*wOut) + wIn, wOut );

          origin[ i ] = ctype( begin ) / ctype( wOut );
          h[ i ] = ctype( end - begin ) / ctype( wOut );
        }
        return LocalGeometry( h, origin );
      }

      LocalGeometry geometryInOutside () const
      {
        typename LocalGeometry::GlobalCoordinate origin, h;
        for( int i = 0; i < mydimension; ++i )
        {
          const long int wIn = insideGridLevel().globalMesh().width( i );
          const long int wOut = outsideGridLevel().globalMesh().width( i );
          const long int pIn = insideEntityInfo().id()[ i ] / 2;
          const long int pOut = outsideEntityInfo().id()[ i ] / 2;

          const long int begin = std::max( pIn*wOut - pOut*wIn, 0l );
          const long int end = std::min( (pIn*wOut - pOut*wIn) + wOut, wIn );

          origin[ i ] = ctype( begin ) / ctype( wIn );
          h[ i ] = ctype( end - begin ) / ctype( wIn );
        }
        return LocalGeometry( h, origin );
      }

    private:
      InsideIterator inside_;
      OutsideEntityInfo outside_;
    };



    // LevelGridPartGlue::IntersectionIterator for SPGrid of equal dimension
    // ---------------------------------------------------------------------

    template< class ct, int dim, template< int > class RefInside, template< int > class RefOutside, class Comm >
    class LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >::IntersectionIterator
    {
      friend class LevelGridPartGlue< InsideGridType, OutsideGridType >;

      typedef typename Intersection::InsideIterator InsideIterator;

      typedef typename Intersection::InsideGridLevel InsideGridLevel;
      typedef typename Intersection::OutsideGridLevel OutsideGridLevel;

      typedef typename Intersection::InsideEntityInfo InsideEntityInfo;
      typedef typename Intersection::OutsideEntityInfo OutsideEntityInfo;

      typedef SPMultiIndex< dimension > MultiIndex;

      IntersectionIterator ( const InsideIterator inside,
                             const OutsideGridLevel &outsideGridLevel )
      : intersection_( inside, outsideGridLevel )
      {
        initOutside( insideEntityInfo().id() );
      }

    public:
      const Intersection &operator* () const { return intersection_; }
      const Intersection &operator-> () const { return &intersection_; }

      bool operator== ( const IntersectionIterator &other ) const
      {
        return intersection_.equals( other.intersection_ );
      }

      bool operator!= ( const IntersectionIterator &other ) const
      {
        return !intersection_.equals( other.intersection_ );
      }

      const IntersectionIterator &operator++ ()
      {
        increment();
        return *this;
      }

    private:
      const InsideGridLevel &insideGridLevel () const { return insideEntityInfo().gridLevel(); }
      const OutsideGridLevel &outsideGridLevel () const { return outsideEntityInfo().gridLevel(); }

      const InsideEntityInfo &insideEntityInfo () const { return intersection_.insideEntityInfo(); }
      const OutsideEntityInfo &outsideEntityInfo () const { return intersection_.outsideEntityInfo(); }
      OutsideEntityInfo &outsideEntityInfo () { return intersection_.outsideEntityInfo(); }

      int outsideBegin ( const MultiIndex &insideId, int i ) const
      {
        const int insideWidth = insideGridLevel().globalMesh().width( i );
        const int outsideWidth = outsideGridLevel().globalMesh().width( i );
        const int j = insideId[ i ] / 2;
        // the outside value needs to be rounded down, here
        return 2*((j * outsideWidth) / insideWidth) + 1;
      }

      int outsideEnd ( const MultiIndex &insideId, int i ) const
      {
        const int insideWidth = insideGridLevel().globalMesh().width( i );
        const int outsideWidth = outsideGridLevel().globalMesh().width( i );
        const int j = insideId[ i ] / 2 + 1;
        // the outside value needs to be rounded up, here
        return 2*((j * outsideWidth + (insideWidth-1)) / insideWidth) + 1;
      }

      void initOutside ( const MultiIndex &insideId )
      {
        if( intersection_.inside_.impl() )
        {
          for( int i = 0; i < dimension; ++i )
            outsideEntityInfo().id()[ i ] = outsideBegin( insideId, i );
          outsideEntityInfo().update( 0 );
        }
        else
          std::fill( outsideEntityInfo().id().begin(), outsideEntityInfo().id().end(), std::numeric_limits< int >::max() );
      }

      void increment ()
      {
        MultiIndex &outsideId = outsideEntityInfo().id();
        for( int i = 0; i < dimension; ++i )
        {
          outsideId[ i ] += 2;
          if( outsideId[ i ] != outsideEnd( insideEntityInfo().id(), i ) )
            return outsideEntityInfo().update();
          outsideId[ i ] = outsideBegin( insideEntityInfo().id(), i );
        }

        ++intersection_.inside_;
        initOutside( insideEntityInfo().id() );
      }

      Intersection intersection_;
    };



    // Implementation of LevelGridPartGlue for SPGrid of equal dimension
    // -----------------------------------------------------------------

    template< class ct, int dim, template< int > class RefInside, template< int > class RefOutside, class Comm >
    inline typename LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >::IntersectionIteratorType
    LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >::begin () const
    {
      return IntersectionIterator( insideGridView_.template begin< 0 >(), outsideGridView_.impl().gridLevel() );
    }

    template< class ct, int dim, template< int > class RefInside, template< int > class RefOutside, class Comm >
    inline typename LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >::IntersectionIteratorType
    LevelGridPartGlue< SPGrid< ct, dim, RefInside, Comm >, SPGrid< ct, dim, RefOutside, Comm > >::end () const
    {
      return IntersectionIterator( insideGridView_.template end< 0 >(), outsideGridView_.impl().gridLevel() );
    }

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_GLUE_LEVELGRIDPART_HH
