#ifndef DUNE_ASH_GLUE_L2PROJECTION_HH
#define DUNE_ASH_GLUE_L2PROJECTION_HH

#include <dune/geometry/quadraturerules.hh>

#include <dune/fem/quadrature/elementquadrature.hh>
#include <dune/fem/operator/1order/localmassmatrix.hh>

namespace Dune
{

  namespace Ash
  {

    template< class GridPartGlue >
    class GluedL2Projection
    {
      typedef GluedL2Projection< GridPartGlue > ThisType;

    public:
      typedef GridPartGlue GridPartGlueType;

    private:
      typedef typename GridPartGlueType::ctype ctype;

      static const int dimension = GridPartGlueType::dimension;

      typedef typename GridPartGlueType::IntersectionIterator IntersectionIterator;
      typedef typename GridPartGlueType::Intersection Intersection;

      typedef typename GridPartGlueType::InsideGridPartType DomainGridPart;
      typedef typename GridPartGlueType::OutsideGridPartType RangeGridPart;

      typedef typename Intersection::InsideEntity DomainEntity;
      typedef typename Intersection::OutsideEntity RangeEntity;

      typedef Dune::QuadratureRule< ctype, dimension > QuadratureRule;
      typedef Dune::QuadratureRules< ctype, dimension > QuadratureRules;
      typedef typename QuadratureRule::const_iterator QuadratureIterator;

    public:
      explicit GluedL2Projection ( const GridPartGlueType &glue )
      : glue_( glue )
      {}

      template< class DomainFunction, class RangeFunction >
      void operator() ( const DomainFunction &u, RangeFunction &w ) const
      {
        typedef typename DomainFunction::LocalFunctionType DomainLocalFunction;
        typedef typename RangeFunction::LocalFunctionType RangeLocalFunction;

        typedef typename RangeFunction::DiscreteFunctionSpaceType RangeSpace;

        typedef Fem::LocalMassMatrix< RangeSpace, Fem::ElementQuadrature< RangeGridPart, 0 > > LocalMassMatrix;

        // assemle right hand side into w

        w.clear();

        for( const Intersection &intersection : glue_ )
        {
          const typename Intersection::Geometry geometry = intersection.geometry();

          const DomainEntity inside = intersection.inside();
          const DomainLocalFunction ulocal = u.localFunction( inside );

          const RangeEntity outside = intersection.outside();
          RangeLocalFunction wlocal = w.localFunction( outside );

          const int order = ulocal.order() + wlocal.order();
          const QuadratureRule &quadrature = QuadratureRules::rule( intersection.type(), order );
          const QuadratureIterator qend = quadrature.end();
          for( QuadratureIterator qit = quadrature.begin(); qit != qend; ++qit )
          {
            typename DomainLocalFunction::RangeType value;
            ulocal.evaluate( intersection.geometryInInside().global( qit->position() ), value );
            value *= qit->weight() * geometry.integrationElement( qit->position() );
            wlocal.axpy( intersection.geometryInOutside().global( qit->position() ), value );
          }
        }

        // apply inverse mass matrix to w (in place)

        LocalMassMatrix localMassMatrix( w.space(), 2*w.space().order() );

        for( const RangeEntity &entity : w.space() )
        {
          RangeLocalFunction wlocal = w.localFunction( entity );
          localMassMatrix.applyInverse( entity, wlocal );
        }
      }

    private:
      const GridPartGlueType &glue_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_GLUE_L2PROJECTION_HH
