#ifndef DUNE_ASH_PARALLEL_MUTEX_HH
#define DUNE_ASH_PARALLEL_MUTEX_HH

namespace Dune
{

  namespace Ash
  {

    // Mutex
    // -----

    class Mutex
    {
      typedef Mutex This;

      struct Storage;

    public:
      Mutex ();
      ~Mutex ();

      // try to acquire the mutex without waiting
      bool acquire ();

      // wait for the mutex and acquire it
      void wait ();

      // release the mutex
      void release ();

    private:
      // prohibit copying and assignment
      Mutex ( const This & );
      const This &operator= ( const This & );

      Storage *storage_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_PARALLEL_MUTEX_HH
