// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>

#include <pthread.h>

#include <dune/ash/parallel/mutex.hh>

namespace Dune
{

  namespace Ash
  {

    // Mutex::Storage
    // --------------

    struct Mutex::Storage
    {
      pthread_mutex_t mutex;
    };



    // Implementation of Mutex
    // -----------------------

    Mutex::Mutex ()
    : storage_( new Storage() )
    {
      if( pthread_mutex_init( &(storage_->mutex), nullptr ) != 0 )
      {
        std::cerr << "Error creating mutex." << std::endl;
        abort();
      }
    }


    Mutex::~Mutex ()
    {
      pthread_mutex_destroy( &(storage_->mutex) );
      delete storage_;
    }


    bool Mutex::acquire ()
    {
      return (pthread_mutex_trylock( &(storage_->mutex) ) == 0);
    }


    void Mutex::wait ()
    {
      pthread_mutex_lock( &(storage_->mutex) );
    }


    void Mutex::release ()
    {
      pthread_mutex_unlock( &(storage_->mutex) );
    }

  } // namespace Ash

} // namespace Dune
