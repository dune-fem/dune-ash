set( HEADERS
  mutex.hh
  thread.hh
)

install( FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/ash/parallel )
