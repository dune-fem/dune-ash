#ifndef DUNE_ASH_PARALLEL_THREAD_HH
#define DUNE_ASH_PARALLEL_THREAD_HH

namespace Dune
{

  namespace Ash
  {

    // Thread
    // ------

    class Thread
    {
      typedef Thread This;

      struct Storage;

    public:
      Thread ();
      virtual ~Thread ();

      bool done () const;
      bool running () const;

      void run ();

      void cancel ();
      void wait ();

    protected:
      virtual void main () = 0;

    private:
      // prohibit copying and assignment
      Thread ( const This & );
      const This &operator= ( const This & );

      Storage *storage_;
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef DUNE_ASH_PARALLEL_THREAD_HH
