// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>

#include <pthread.h>

#include <dune/ash/parallel/thread.hh>

namespace Dune
{

  namespace Ash
  {

    // Thread::Storage
    // ---------------

    struct Thread::Storage
    {
      static void *main ( void *p );

      pthread_t thread;
      bool done;
    };



    // Thread::Storage::main
    // ---------------------

    void *Thread::Storage::main ( void *p )
    {
      int dummy;
      pthread_setcancelstate( PTHREAD_CANCEL_ENABLE, &dummy );
      pthread_setcanceltype( PTHREAD_CANCEL_ASYNCHRONOUS, &dummy );

      Thread *thread = (Thread *)p;
      thread->main();
      thread->storage_->done = true;

      return nullptr;
    }



    // Implementation of Thread
    // ------------------------

    Thread::Thread ()
    : storage_( nullptr )
    {}


    Thread::~Thread () { wait(); }


    bool Thread::done () const
    {
      return (storage_ && storage_->done);
    }


    bool Thread::running () const
    {
      return storage_;
    }


    void Thread::run ()
    {
      if( !running() )
      {
        storage_ = new Storage();
        storage_->done = false;
        pthread_create( &(storage_->thread), nullptr, Storage::main, (void *)this );
      }
    }


    void Thread::cancel ()
    {
      if( running() && !done() )
        pthread_cancel( storage_->thread );
    }


    void Thread::wait ()
    {
      if( running() )
      {
        pthread_join( storage_->thread, nullptr );
        delete storage_;
        storage_ = nullptr;
      }
    }

  } // namespace Ash

} // namespace Dune
