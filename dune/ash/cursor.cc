// vim: set expandtab ts=2 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

#include <SDL.h>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/ash/cursor.hh>

namespace Dune
{

  namespace Ash
  {

    // Implementation of MouseCursor
    // -----------------------------

    MouseCursor::MouseCursor ( const std::string &fileName )
    {
      FieldVector< int, 2 > size( 0 );

      std::ifstream fin( fileName.c_str() );
      if( !fin )
        DUNE_THROW( IOError, "Unable to open '" << fileName << "'." ); 

      std::vector< std::string > lines;
      while( true )
      {
        std::string line;
        std::getline( fin, line );
        if( !fin.good() )
          break;

        const std::size_t cpos = line.find( '#' );
        if( cpos == 0 )
          continue;
        if( cpos != line.npos )
          line = line.substr( 0, line.rfind( ' ', cpos ) );

        lines.push_back( line );
        size[ 0 ] = std::max( size[ 0 ], int( line.length() ) );
        ++size[ 1 ];
      }
      fin.close();

      size[ 0 ] = (size[ 0 ] + 7) & ~7;
      const int bytePerLine = size[ 0 ] / 8;
      Uint8 *data = new Uint8[ size[ 1 ] * bytePerLine ];
      Uint8 *mask = new Uint8[ size[ 1 ] * bytePerLine ];

      bool hasHotSpot = false;
      FieldVector< int, 2 > hotSpot( 0 );
      for( int j = 0; j < size[ 1 ]; ++j )
      {
        const std::string &line = lines[ j ];
        for( int i = 0; i < bytePerLine; ++i )
        {
          Uint8 &data_ij = data[ j*bytePerLine + i ];
          Uint8 &mask_ij = mask[ j*bytePerLine + i ];

          data_ij = mask_ij = Uint8( 0 );
          for( int k = 0; k < 8; ++k )
          {
            if( i*8 + k >= int( line.length() ) )
              break;

            const Uint8 bit = Uint8( 0x80 ) >> k;
            char c = line[ i*8 + k ];
            switch( c )
            {
            case 'X':
              if( hasHotSpot )
                DUNE_THROW( IOError, fileName << ": Multiple hot spots." );
              else
                hotSpot = { i*8 + k, j };
              hasHotSpot = true;
            case 'x':
              data_ij |= bit;
              mask_ij |= bit;
              break;

            case ':':
              if( hasHotSpot )
                DUNE_THROW( IOError, fileName << ": Multiple hot spots." );
              else
                hotSpot = { i*8 + k, j };
              hasHotSpot = true;
            case '.':
              mask_ij |= bit;
              break;

            case '^':
              if( hasHotSpot )
                DUNE_THROW( IOError, fileName << ": Multiple hot spots." );
              else
                hotSpot = { i*8 + k, j };
              hasHotSpot = true;
            case ' ':
              break;

            default:
              if( (int( c ) & 0xff) < ' ' )
                DUNE_THROW( IOError, fileName << ": Invalid control character (" << int( c ) << ")." );
              else
                DUNE_THROW( IOError, fileName << ": Invalid character (" << c << ")." );
            }
          }
        }
      }

      if( !hasHotSpot )
      {
        std::cerr << fileName << ": No hot spot defined, using (0,0)." << std::endl;
        for( int i = 0; i < 2; ++i )
          hotSpot[ i ] = (hotSpot[ i ] >= size[ i ] ? 0 : hotSpot[ i ]);
      }

      cursor_ = SDL_CreateCursor( data, mask, size[ 0 ], size[ 1 ], hotSpot[ 0 ], hotSpot[ 1 ] );

      delete[] mask;
      delete[] data;
    }


    MouseCursor::~MouseCursor ()
    {
      SDL_FreeCursor( cursor_ );
    }



    // Auxiliary Functions
    // -------------------

    void setCursor ( const MouseCursor &cursor )
    {
      SDL_SetCursor ( cursor.cursor_ );
    }


    void hideCursor ()
    {
      SDL_ShowCursor( SDL_DISABLE );
    }


    void showCursor ()
    {
      SDL_ShowCursor( SDL_ENABLE );
    }

  } // namespace Ash

} // namespace Dune
