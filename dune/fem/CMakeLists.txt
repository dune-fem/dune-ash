set( HEADERS
  serialoutputmap.hh
  writerutilities.hh
)

install( FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fem )
