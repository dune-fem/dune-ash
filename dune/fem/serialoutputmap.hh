// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_FEM_OUTPUTMAP_HH
#define DUNE_FEM_OUTPUTMAP_HH

#include <fstream>
#include <map>

#include <dune/fem/io/parameter.hh>
#include <dune/fem/io/streams/binarystreams.hh>

namespace Dune
{

  namespace Fem
  {

    /////////////////////////////////////////////////////////////////////////////
    //
    //  get global id set map to a standard vector for a DOF output which 
    //  later on can be communicated to other processes 
    //
    /////////////////////////////////////////////////////////////////////////////

    template< class DiscreteFunction >
    struct SerialOutputMap
    {
      /** \brief type of discrete function */
      typedef DiscreteFunction DiscreteFunctionType;

      /** \brief type of discrete function space */
      typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
      /** \brief type of grid */
      typedef typename DiscreteFunctionType::GridType GridType;

    private:
      /** \brief type of the global id set */
      typedef typename GridType::GlobalIdSet IdSetType;
      /** \brief type of the global id */
      typedef typename IdSetType::IdType IdType;
      
      typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType; 
      typedef typename IteratorType::Entity EntityType; 

      typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;

      static std::string sanitizePrefix ( const std::string &prefix )
      {
        std::string myPrefix = prefix;
        if( myPrefix.empty() )
        {
          myPrefix = "./";
        }
        if ( myPrefix[ myPrefix.size() - 1 ] != '/' )
        {
          myPrefix += '/';
        }
        char first = myPrefix[ 0 ];
        // if not indicated otherwise, we assume paths to be relative to the current $PWD
        if ( first != '/' && first != '.' )
        {
          myPrefix = "./" + myPrefix;
        }
        return myPrefix;
      }

      /*
       * Debug info (just for convenience). Can be made into a no-op (which is optimized out)
       * by embracing the whole method body with an #if 0 ... #endif - so the code 
       * inside .read(...) and .write(...) does not need to be changed
       */
      static void debugInfo ( const std::string &msg, int rank = 0 )
      {
        if( Parameter::verbose() )
          std::cerr << msg;
      }
      
    public:
      //! constructor 
      explicit SerialOutputMap ( DiscreteFunctionType &solution )
      : solution_( solution ),
        idSet_( solution_.space().grid().globalIdSet() )
      {}

      //! destructor
      ~SerialOutputMap () {}

      std::string makeFilename ( const std::string filename, const int rank ) const
      {
        std::ostringstream newFileName;
        newFileName << filename << "_" << rank;
        return newFileName.str();
      }

      const DiscreteFunctionSpaceType &space() const
      {
        return solution().space();
      }

      bool willWrite ( const int checkPointStep, const int timestep )
      {
        return ( (checkPointStep > 0) && (((timestep % checkPointStep) == 0) && timestep > 0) );
      }

      void write ( const std::string &prefix, const std::string &filename ) const
      {
        const GridType &grid = space().grid();
        int size = grid.comm().size();
        int rank = grid.comm().rank();

        debugInfo( "====================in SerialOutputMap.write('" + prefix + "', '" + filename + "');\n", rank );

        std::string myPrefix = sanitizePrefix( prefix );
        debugInfo( "myPrefix: '" + myPrefix + "', filename: '" + filename + "'\n", rank );

        // create masterfile and partfiles (process data files)
        if( rank == 0 )
        {
          std::ofstream masterfile( (myPrefix + filename).c_str() );
          debugInfo( + "writing to masterfile '" + myPrefix + filename + "'\n", rank );

          for( int i = 0; i < size; ++i )
          {
            // write filename of process file into master file
            masterfile << makeFilename( filename, i ) << std::endl;
            debugInfo( "----piecefilename: '" + makeFilename( filename, i ) + "'\n", rank );
          }
        }

        // open piece file for writing
        BinaryFileOutStream outPiece( myPrefix + makeFilename( filename, rank ) );
        debugInfo( "opening piecefile (for writing) '" + myPrefix + makeFilename( filename, rank ) + "'\n" );

        // get number of elements in the corresponding file and write in outPiece
        int numElements = 0;
        const IteratorType end = space().end();
        for( IteratorType it = space().begin(); it != end; ++it )
          ++numElements;
        outPiece << numElements;

        // iterate over all entities
        std::vector< double > data;
        data.reserve( space().blockMapper().maxNumDofs() * space().localBlockSize );
        for( IteratorType it = space().begin(); it != end; ++it )
        {
          const EntityType &entity = *it;
          const IdType id = idSet_.id( entity );
          outPiece << id; 

          // get local function and write into the corresponding vector coordinate
          const LocalFunctionType &solutionEntity = solution().localFunction( entity );
          // write thos local functions in the map:
          const unsigned int numDoFs = solutionEntity.numDofs();
          data.resize( numDoFs );
          for( unsigned int i = 0; i < numDoFs; ++i )
            data[ i ] = solutionEntity[ i ];
          outPiece << data;
        }
      }

      void read ( const std::string &prefix, const std::string &filename ) 
      {
        debugInfo( "====================in SerialOutputMap.read('" + prefix + "', '" + filename + "');\n" );

        std::string myPrefix = sanitizePrefix( prefix );
        debugInfo( "myPrefix: '" + myPrefix + "', filename: '" + filename + "'\n" );

        solution().clear();

        // object data of DataMap will contain complete vector without data,
        // remains to be filled
        typedef std::map< IdType, std::vector< double > > DataMap;
        DataMap data;
        const IteratorType end = space().end();
        for( IteratorType it = space().begin(); it != end; ++it )
        {
          const EntityType &entity = *it;
          data.insert( std::make_pair( idSet_.id( entity ), std::vector< double >() ) );
        }

        // read all data once, but remember only what's relevant for this process
        std::ifstream masterfile( (myPrefix + filename).c_str() );
        debugInfo( "opening '" + myPrefix + filename + "' for reading...\n" );
        if ( !masterfile )
        {
          std::cerr << "Error opening master file '" << myPrefix + filename << "'\n";
          abort();
        }
        while( masterfile )
        {
          std::string pieceFilename;
          std::getline( masterfile, pieceFilename );
          if( pieceFilename.empty() )
            break;

          pieceFilename = myPrefix + pieceFilename;
          {
            std::ifstream exists( pieceFilename.c_str() );
            if ( !exists )
            {
              std::cerr << "piece file '" << pieceFilename << "' does not exist!\n";
              abort();
            }
          }

          BinaryFileInStream inPiece( pieceFilename );
          debugInfo( "opening binary file '" + pieceFilename + "' for reading...\n" );
          int numElements;
          inPiece >> numElements;
          for( int i = 0; i < numElements; ++i )
          {
            IdType id;
            inPiece >> id; 
            std::vector< double > dofs;
            inPiece >> dofs;
            typename DataMap::iterator pos = data.find( id );
            if( pos != data.end() )
              pos->second = dofs;
          }
        }

        // copy the data into the discrete function
        for( IteratorType it = space().begin(); it != end; ++it )
        {
          const EntityType &entity = *it;
          LocalFunctionType solutionEntity = solution().localFunction( entity );
          
          const std::vector< double > &dofs = data[ idSet_.id( entity ) ];
          unsigned int numDOFs = solutionEntity.numDofs();
          if( dofs.size() != numDOFs )
            DUNE_THROW( Dune::IOError, "Number of DoFs does read back does not coincide with number of DoFs required." );
          for( unsigned int i = 0; i < numDOFs; ++i )
            solutionEntity[ i ] = dofs[ i ];
        }
      }

      const DiscreteFunctionType &solution () const { return solution_; }
      DiscreteFunctionType &solution () { return solution_; }

    protected:      
      DiscreteFunctionType &solution_;
      const IdSetType &idSet_;
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_OUTPUTMAP_HH
