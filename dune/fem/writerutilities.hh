// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DUNE_WRITEUTIL_HH
#define DUNE_WRITEUTIL_HH

#include <dune/fem/io/io.hh>
#include <dune/fem/io/parameter.hh>

#include "../../dune/fem/serialoutputmap.hh"
#include <dune/fem/io/file/vtkio.hh>


inline std::string makeTpFilename ( const std::string filename, const int counter ) 
{
  std::ostringstream newFileName;
  newFileName << filename << "_" << counter;
  return newFileName.str();
}

/**
 * bool willWrite: can be called in baseevolution by eocDataOutput.willWrite( tp )
 *
 */
template < class DiscreteFunctionType, class GridPartType >
void xdrVtkWriteAndRead ( const GridPartType& gridPart, 
                          const std::string& basefilename, 
                          DiscreteFunctionType& solution,
                          bool willWrite, 
                          const int timeStep )
{
  // type of outPutMap 
  typedef Dune::Fem::SerialOutputMap< DiscreteFunctionType > OutputMap;
  OutputMap map( solution );

  if ( true/*willWrite*/ )
  {
    std::string XDRFilename = makeTpFilename ( basefilename, timeStep );
    std::cout << "------->writing XDR output to " << XDRFilename << std::endl;
    map.write( XDRFilename );
    //test if write and read functions work fine:
    Dune::Fem::VTKIO< GridPartType, true > vtkio( gridPart, 2 );
    map.read( XDRFilename );
    DiscreteFunctionType sol2= map.solution();
    vtkio.addVertexData ( sol2 );
    std::cout << "-------> test vtk file to testfilevtk" << std::endl;
    std::string VTKFilename = makeTpFilename ( "testvtk", timeStep );
    vtkio.write ( VTKFilename );
	}
}

/**
 * write concentration data into xdr file
 * 
 */
template < class DiscreteFunctionType >
void writeSerialXdr ( const std::string &prefix, const std::string &basefilename, 
                      DiscreteFunctionType& solution,
                      const int counter, const bool verbose = false )
{
  if( verbose )
    std::cout << "in writeXerialXdr( '" << prefix << "', '" << basefilename << "',...);\n";

  if( !Dune::Fem::directoryExists( prefix ) ) 
  { 
    Dune::Fem::createDirectory( prefix );
  }

  typedef Dune::Fem::SerialOutputMap< DiscreteFunctionType > OutputMap;
  OutputMap map( solution );
  std::string XDRFilename = makeTpFilename( basefilename, counter );
  if( verbose )
    std::cout << "------->writing XDR output to " << ( !prefix.empty()  ? prefix : "./" ) << "/" << XDRFilename << std::endl;
  map.write( prefix, XDRFilename );
}


#endif // #ifndef WRITE_UTIL
