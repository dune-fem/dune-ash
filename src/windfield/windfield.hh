#ifndef HEAT_HH
#define HEAT_HH

#include <cassert>
#include <cmath>

#include <dune/fem/function/common/function.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/ash/io.hh>


// EvolutionFunction
// -----------------

template< class FunctionSpace, class Impl >
struct EvolutionFunction
: public Dune::Fem::Function< FunctionSpace, Impl >
{
  explicit EvolutionFunction ( const double time = 0.0 )
  : time_( time )
  {}

  const double &time () const
  {
    return time_;
  }

  void setTime ( const double time )
  {
    time_ = time;
  }

private:
  double time_;
};



namespace Problem
{

  template<class Domain, class Range>
  class FileDataReader
  {
    typedef Domain      DomainType;
    typedef Range       RangeType;

    typedef FileDataReader< DomainType, RangeType > FileDataReaderType;
    typedef std::vector< std::pair < DomainType, RangeType > > MousePathType;
    MousePathType          dataVector;
    double                 radius_;

  public:
    FileDataReader ( double radius = 0.1 )
    : radius_( radius )
    {
      std::string filename = Dune::Fem::Parameter::getValue< std::string >( "ash.session" ) + "/mousepaths";
      radius_ = Dune::Fem::Parameter::getValue< double >( "windfield.radius" );
      std::list< MousePathType > paths;
      readField( paths, filename );
      dataVector = collectMousePaths( paths );
    }

    ~FileDataReader () {}

    static void evaluate( const DomainType &x, RangeType &phi)
    {
      instance().eval(x,phi);
    }

    static int nrOfPointsAround( const DomainType &x)
    {
      return instance().nrPoints(x);
    }

  private:
    static FileDataReaderType &instance ()
    {
      static FileDataReaderType instance_;
      return instance_;
    }

    static MousePathType collectMousePaths ( const std::list< MousePathType > &paths )
    {
      typedef typename std::list< MousePathType >::const_iterator Iterator;
      MousePathType vField;
      for( Iterator it = paths.begin(); it != paths.end(); ++it )
        vField.insert( vField.end(), it->begin(), it->end() );
      return vField;
    }

    void eval( const DomainType &x, RangeType & phi )
    {
      phi=0;
      int nrOfPoints =0;
      const int dataSize = dataVector.size();
      for(int i=0; i< dataSize;++i)
      {
        DomainType dist(x);
        dist[0] -= dataVector[i].first[0];
        dist[1] -= 1.0 - dataVector[i].first[1];

        if( dist.two_norm() < radius_ )
        {
          nrOfPoints++;
          phi[0] += dataVector[i].second[0];
          phi[1] -= dataVector[i].second[1];
        }
      }
      phi /= (nrOfPoints > 0)? nrOfPoints:1.;
    }

    int nrPoints( const DomainType &x)
    {
      int nrOfPoints =0;
      const int dataSize = dataVector.size();
      for(int i=0; i< dataSize;++i)
      {
        DomainType dist(x);
        dist[0] -= dataVector[i].first[0];
        dist[1] -= 1.0 - dataVector[i].first[1];
        if( dist.two_norm() < radius_ )
        {
          nrOfPoints++;
        }
      }
      return nrOfPoints; 
    }
  };



  // WindField
  // ---------

  template< class FunctionSpace >
  struct WindField
  : public EvolutionFunction< FunctionSpace, WindField< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    void evaluate ( const DomainType &x, RangeType &phi ) const
    {
      phi =0;
      FileDataReader<DomainType,RangeType> :: evaluate (x,phi);
    }

    using EvolutionFunction< FunctionSpace, WindField< FunctionSpace > >::time;
  };



  // VortexFunction
  // --------------

  template< class FunctionSpace >
  struct VortexFunction
  : public Dune::Fem::Function< FunctionSpace, VortexFunction< FunctionSpace > >
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;

    static const int dimDomain = DomainType::dimension;
    static const int dimRange = RangeType::dimension;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;
    const double omega_;
    const int density_;

    VortexFunction ()
    : omega_( Dune::Fem::Parameter::getValue< double >( "windfield.vortex.magnitude" ) ),
      density_( Dune::Fem::Parameter::getValue< int >( "windfield.vortex.density" ) )
    {}

    void evaluate ( const DomainType &x, RangeType &phi ) const
    {
      phi[0]= -omega_ * cos(2*M_PI*density_*x[1]) * sin(2*M_PI*density_*x[0]);
      phi[1]= omega_ * cos(2*M_PI*density_*x[0]) * sin(2*M_PI*density_*x[1]);
    }

  };


  // HeatModel
  // ---------

  template< class FunctionSpace >
  struct HeatModel
  {
    typedef FunctionSpace FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType DomainType;
    typedef typename FunctionSpaceType::RangeType RangeType;
    typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

    typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

    typedef Dune::Fem::TimeProviderBase TimeProviderType;

    HeatModel ( const TimeProviderType &timeProvider, const double timeStepFactor )
    : timeProvider_( timeProvider ),
      timeStepFactor_( timeStepFactor ),
      eps_(Dune::Fem::Parameter::getValue< double >( "windfield.eps" ))
    {}

    template< class Entity, class Point >
    void massFlux ( const Entity &entity, const Point &x,
                    const RangeType &value, RangeType &flux ) const
    {
      flux = value;
    }

    template< class Entity, class Point >
    void diffusiveFlux ( const Entity &entity, const Point &x,
                         const JacobianRangeType &gradient,
                         JacobianRangeType &flux ) const
    {
      const int nrOfPoints = FileDataReader< DomainType, RangeType >::nrOfPointsAround( entity.geometry().global( x ) );
      flux = gradient;
      flux *= eps_ / (1.0 + nrOfPoints);
      flux *= timeStepFactor_*timeProvider_.deltaT();
    }

  private:
    const TimeProviderType &timeProvider_;
    double timeStepFactor_;
    double eps_;
  };

} // namespace Problem

#endif // #ifndef HEAT_HH
