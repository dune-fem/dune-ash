#ifndef EQUI_HH
#define EQUI_HH

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/io/parameter.hh>

// equilbrateSolution
// ------------------

template< class DiscreteFunction >
void equilbrateSolution ( DiscreteFunction &rhs )
{
  DiscreteFunction equi( rhs );
  equi.clear();
  typedef typename DiscreteFunction::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunction::LocalFunctionType LocalFunctionType;
  typedef typename DiscreteFunctionSpaceType :: RangeType RangeType;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;
  typedef typename EntityType::Geometry GeometryType;

  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;


  const DiscreteFunctionSpaceType &dfSpace = rhs.space();

  int nrOfVals =0;
  RangeType middel(0);
  
  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;

    LocalFunctionType rhsLocal = rhs.localFunction( entity );
    
    QuadratureType quadrature( entity, 2*dfSpace.order()+1 );
    const size_t numQuadraturePoints = quadrature.nop();
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      RangeType val(0);
      rhsLocal.evaluate(quadrature[pt], val);

      if(val.two_norm()>1e-4)
      {
        middel += val;
        nrOfVals++;
      }
    }
  }

  middel /= (nrOfVals >0)? nrOfVals : 1;

  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;
    const GeometryType &geometry = entity.geometry();

    LocalFunctionType equiLocal = equi.localFunction( entity );
    
    QuadratureType quadrature( entity, 2*dfSpace.order()+1 );
    const size_t numQuadraturePoints = quadrature.nop();
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {

      const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
      double weight = quadrature.weight( pt ) * geometry.integrationElement( x );
      RangeType val(middel);
      val *= weight;

      equiLocal.axpy(quadrature[pt], val );
    }
  }
  const double factor = Dune::Fem::Parameter::getValue<double> ("windfield.equilbration.factor");
  equi *= factor;
  rhs += equi;
  rhs.communicate();
}

#endif // #ifndef RHS_HH
