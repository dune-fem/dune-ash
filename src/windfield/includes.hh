#ifndef BETWEENEND_INCLUDES
#define BETWEENEND_INCLUDES

#ifndef USE_TWISTFREE_MAPPER
#define USE_TWISTFREE_MAPPER
#endif

// iostream includes
#include <iostream>

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

// include discrete function space
#include <dune/fem/space/lagrange.hh>

// include discrete function
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/blockvectorfunction.hh>

// include linear operators
#include <dune/fem/operator/linear/spoperator.hh>

#include <dune/fem/solver/umfpacksolver.hh>
#include <dune/fem/solver/cginverseoperator.hh>

// include norms
#include <dune/fem/misc/l2norm.hh>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include <dune/fem/io/streams/xdrstreams.hh>

#include "../common/discretizationtraits.hh"
#include "windfield.hh"
#include "elliptic.hh"

namespace windfield
{

  typedef typename DiscretizationTraits::GridType GridType;
  typedef typename DiscretizationTraits::GridPartType GridPartType;

  typedef typename DiscretizationTraits::WindFieldSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

  typedef typename DiscretizationTraits::WindFieldType DiscreteFunctionType;
  typedef Dune::Fem::SparseRowLinearOperator< DiscreteFunctionType, DiscreteFunctionType > LinearOperatorType;
  typedef Dune::Fem::CGInverseOperator< DiscreteFunctionType > LinearInverseOperatorType;

  typedef Problem::WindField< FunctionSpaceType > WindFieldType;
  typedef Problem::VortexFunction< FunctionSpaceType > VortexFunctionType;
  typedef Dune::Fem::GridFunctionAdapter< VortexFunctionType, GridPartType > VortexAdapterType;
  typedef Dune::Fem::GridFunctionAdapter< WindFieldType, GridPartType > WindFieldAdaptarType;

  // define Laplace operator
  typedef Problem::HeatModel< FunctionSpaceType > HeatModelType;
  typedef EllipticOperator< DiscreteFunctionType, HeatModelType > EllipticOperatorType;

  // select norm for error computation
  typedef Dune::Fem::L2Norm< GridPartType > NormType;

} // namespace windfield

#endif // #ifndef BETWEENEND_INCLUDES
