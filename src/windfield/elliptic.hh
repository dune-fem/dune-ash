#ifndef ELLIPTIC_HH
#define ELLIPTIC_HH

#include <dune/common/fmatrix.hh>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/stencil.hh>


// EllipticOperator
// ----------------

template< class DiscreteFunction, class Model >
struct EllipticOperator
: public Dune::Fem::Operator< DiscreteFunction >
{
  typedef DiscreteFunction DiscreteFunctionType;
  typedef Model ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity EntityType;
  typedef typename EntityType::Geometry GeometryType;

  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;

  typedef Dune::Fem::DiagonalStencil< DiscreteFunctionSpaceType, DiscreteFunctionSpaceType > StencilType;

public:
  explicit EllipticOperator ( const ModelType &model = Model() )
  : model_( model )
  {}
      
  virtual void
  operator() ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const;

  template< class JacobianOperator >
  void jacobian ( const DiscreteFunction &u, JacobianOperator &jOp ) const;

private:
  ModelType model_;
};

template< class DiscreteFunction, class Model >
inline void EllipticOperator< DiscreteFunction, Model >
  ::operator() ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const 
{
  w.clear();

  const DiscreteFunctionSpaceType &dfSpace = w.space();

  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;
    const GeometryType &geometry = entity.geometry();

    const LocalFunctionType uLocal = u.localFunction( entity );
    LocalFunctionType wLocal = w.localFunction( entity );

    QuadratureType quadrature( entity, uLocal.order() + wLocal.order() );
    const size_t numQuadraturePoints = quadrature.nop();
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
      const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

      typename LocalFunctionType::RangeType vu, avu;
      uLocal.evaluate( quadrature[ pt ], vu );
      model_.massFlux( entity, quadrature[ pt ], vu, avu );

      typename LocalFunctionType::JacobianRangeType du, adu;
      uLocal.jacobian( quadrature[ pt ], du );
      model_.diffusiveFlux( entity, x, du, adu );

      avu *= weight;
      adu *= weight;
      wLocal.axpy( quadrature[ pt ], avu, adu );
    }
  }
  w.communicate();
}

template< class DiscreteFunction, class Model >
template< class JacobianOperator >
inline void EllipticOperator< DiscreteFunction, Model >
  ::jacobian ( const DiscreteFunction &u, JacobianOperator &jOp ) const
{
  typedef typename JacobianOperator::LocalMatrixType LocalMatrixType;
  typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;

  const DiscreteFunctionSpaceType &dfSpace = u.space();

  StencilType stencil( jOp.domainSpace(), jOp.rangeSpace() );
  jOp.reserve( stencil );
  jOp.clear();

  const std::size_t maxNumLocalDofs = dfSpace.blockMapper().maxNumDofs() * DiscreteFunctionSpaceType::localBlockSize;
  std::vector< typename LocalFunctionType::RangeType > phi( maxNumLocalDofs );
  std::vector< typename LocalFunctionType::JacobianRangeType > dphi( maxNumLocalDofs );

  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;
    const GeometryType &geometry = entity.geometry();

    LocalMatrixType jLocal = jOp.localMatrix( entity, entity );

    const BasisFunctionSetType &basis = jLocal.domainBasisFunctionSet();
    const unsigned int numBasisFunctions = basis.size();
          
    QuadratureType quadrature( entity, 2*dfSpace.order() );
    const size_t numQuadraturePoints = quadrature.nop();
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
      const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

      basis.evaluateAll( quadrature[ pt ], phi );
      basis.jacobianAll( quadrature[ pt ], dphi );

      for( unsigned int i = 0; i < numBasisFunctions; ++i )
      {
        typename LocalFunctionType::RangeType aphi;
        model_.massFlux( entity, quadrature[ pt ], phi[ i ], aphi );
        typename LocalFunctionType::JacobianRangeType adphi;
        model_.diffusiveFlux( entity, x, dphi[ i ], adphi );
        for( unsigned int j = 0; j < numBasisFunctions; ++j )
        {
          double value = aphi * phi[ j ];
          for( int k = 0; k < adphi.rows; ++k )
            value += adphi[ k ] * dphi[ j ][ k ];
          jLocal.add( j, i, weight * value );
        }
      }
    }
  }
}

#endif // #ifndef ELLIPTIC_HH
