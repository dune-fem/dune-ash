#include <config.h>
#define USE_TWISTFREE_MAPPER


// iostream includes
#include <iostream>

// include parameter handling
#include <dune/fem/io/parameter.hh>

#include <dune/fem/space/common/interpolate.hh>

#include <dune/fem/io/streams/xdrstreams.hh>
#include <dune/fem/serialoutputmap.hh>

#include "../common/filemanager.hh"

#include "includes.hh"

// loca includes
#include "rhs.hh"
#include "equilibrate.hh"


using namespace windfield;


// algorithm
// ---------

void algorithm ( GridType &grid, const double timeStep )
{
  // create discrete function space
  GridPartType gridPart( grid, grid.maxLevel() );
  DiscreteFunctionSpaceType dfSpace( gridPart );

  // create exact solution
  WindFieldType windField;
  WindFieldAdaptarType gridWindField( "windField", windField, gridPart, dfSpace.order()+1 );

  // create discrete solution as interpolation of exact solution
  DiscreteFunctionType solution( "solution", dfSpace );
  interpolate( gridWindField, solution );

  equilbrateSolution( solution );

  // setup time provider
  const double endTime = 0.1;
  Dune::Fem::GridTimeProvider< GridType > timeProvider( grid );

  // create discrete functions for intermediate functionals
  DiscreteFunctionType rhs( "rhs", dfSpace );
  rhs.clear();
  DiscreteFunctionType tmp( "tmp", dfSpace );

  // create operators for explicit and implicit part of time discretization
  EllipticOperatorType explicitEllipticOp( HeatModelType( timeProvider, -0.4 ) );
  EllipticOperatorType implicitEllipticOp( HeatModelType( timeProvider, 0.6 ) );

  // initialize time provider (we use a constant time step, here)
  timeProvider.init( timeStep );

  // assemble matrix for implicit part
  LinearOperatorType implicitLinearOp( "linear implicit elliptic operator", dfSpace, dfSpace );
  implicitEllipticOp.jacobian( solution, implicitLinearOp );

  LinearOperatorType explicitLinearOp( "linear explicit elliptic operator", dfSpace, dfSpace );
  explicitEllipticOp.jacobian( solution, explicitLinearOp );

/*********************************************************/

  LinearInverseOperatorType solver( implicitLinearOp, 1e-8, 1e-8 );

  windfield::NormType norm(gridPart);
  const double valueIn = norm.norm(solution);

  if( std::abs( valueIn ) < 1e-10 )
  {
    // create discrete function in order to plot error
    const std::string session = Dune::Fem::Parameter::getValue< std::string >( "ash.session" );
    Dune::Fem::SerialOutputMap< DiscreteFunctionType > output( solution );
    output.write( session, "windfield.xdr" );
    return;
  }

  windField.setTime( timeProvider.time() );
  assembleRHS( windField, tmp );

  // time loop
  for( ; timeProvider.time() < endTime; timeProvider.next( timeStep ) )

  {
    // assemble right hand side functional
    explicitLinearOp( solution, rhs );
    rhs.axpy( timeProvider.deltaT(), tmp );

    // solve for updated solution
    solver( rhs, solution );
  }

  const double valueOut =  norm.norm( solution );
  const double ampFactor = Dune::Fem::Parameter::getValue< double >( "windfield.amplifiction" );
  solution *= ampFactor * valueIn/ valueOut;

  // calc and add some mirco turbolences
  VortexFunctionType vortexAnalytical;
  VortexAdapterType vortexAnalytic( "vortex function", vortexAnalytical, gridPart, dfSpace.order()+1);
  DiscreteFunctionType vortex( "vortexFunction", dfSpace );
  interpolate( vortexAnalytic, vortex );
  solution += vortex;

  // create discrete function in order to plot error
  const std::string session = Dune::Fem::Parameter::getValue< std::string >( "ash.session" );
  Dune::Fem::SerialOutputMap< DiscreteFunctionType > output( solution );
  output.write( session, "windfield.xdr" );
}


// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append all given parameters
  Dune::Fem::Parameter::append( argc, argv );
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  Ash::FileManager::appendParameterFile( "general.param" );
  Ash::FileManager::appendParameterFile( "windfield.param" );

  // create grid from DGF file
  const std::string gridfile = Ash::FileManager::macroGridFile();
  Dune::GridPtr< GridType > gridptr( gridfile );
  GridType &grid = *gridptr;
  grid.loadBalance();

  // initial grid refinement
  const int refinements = Dune::Fem::Parameter::getValue< int >( "ash.refinements" );
  for( int i = 0; i < refinements; ++i )
    Dune::Fem::GlobalRefine::apply( grid, 1 );

  algorithm( grid, 0.002 );

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
