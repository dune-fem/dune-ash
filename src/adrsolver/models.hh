#ifndef MODELS_HH
#define MODELS_HH

#include "problem_ash.hh"
#include "advectdiff.hh"

#include "../windfield/includes.hh"


/**********************************************
 * Analytical model                           *
 *********************************************/

/**
 * @brief Traits class for AdvectionDiffusionModel
 */
template <class GridPart,int dimRange2,
          int dimRange1 = dimRange2 * GridPart::GridType::dimensionworld>
class AdvDiffModelTraits
{
public:
  typedef GridPart                                                   GridPartType;
  typedef typename GridPartType :: GridType                          GridType;
  static const int dimDomain = GridType::dimensionworld;
  static const int dimRange = dimRange2;
  static const int dimGradRange = dimRange1;
  // Definition of domain and range types
  typedef Dune::FieldVector< double, dimDomain >                           DomainType;
  typedef typename GridType :: template Codim <0> :: Geometry :: LocalCoordinate LocalCoordinate;
  typedef Dune::FieldVector< double, LocalCoordinate :: dimension -1 >     FaceLocalCoordinate;
  typedef Dune::FieldVector< double, dimRange >                            RangeType;
  typedef Dune::FieldVector< double, dimGradRange >                        GradientType;
  // ATTENTION: These are matrices (c.f. AdvectionDiffusionModel)
  typedef Dune::FieldMatrix< double, dimRange, dimDomain >                 FluxRangeType;
  typedef Dune::FieldMatrix< double, dimGradRange, dimDomain >             DiffusionRangeType;
  typedef typename GridPartType :: IntersectionType                        Intersection;
  typedef typename GridPartType :: IntersectionIteratorType                IntersectionIterator;
  typedef typename GridType :: template Codim< 0 > :: Entity               EntityType;
};



// AdvectionDiffusionModel
// -----------------------

template < class GridPartType, class ProblemType >
class AdvectionDiffusionModel
{
public:
  typedef typename GridPartType :: GridType                          GridType;
  static const int dimDomain = GridType::dimensionworld;
  static const int dimRange = 1;
  typedef AdvDiffModelTraits< GridPartType, dimRange,
                              dimRange*dimDomain >                   Traits;
  typedef typename Traits :: DomainType                              DomainType;
  typedef typename Traits :: LocalCoordinate                         LocalCoordinate;
  typedef typename Traits :: RangeType                               RangeType;
  typedef typename Traits :: GradientType                            GradientType;
  typedef typename Traits :: FluxRangeType                           FluxRangeType;
  typedef typename Traits :: DiffusionRangeType                      DiffusionRangeType;
  typedef typename Traits :: EntityType                              EntityType;
  typedef typename Traits :: Intersection                            Intersection;
  typedef typename Traits :: FaceLocalCoordinate                     FaceLocalCoordinate;
  typedef typename DiscretizationTraits::WindFieldSpaceType          VeloDFSpace;
  typedef typename DiscretizationTraits::WindFieldType               VeloDFType;
  typedef typename VeloDFSpace :: JacobianRangeType                  VeloJacobianType;
  typedef typename VeloDFSpace :: RangeType                          VeloRangeType;

public:
  explicit AdvectionDiffusionModel ( const ProblemType &problem )
  : problem_( problem ),
    diffusion_( problem.diffusion() )
  {
    tstep_eps = diffusion_;
  }

  void advection ( const  EntityType &entity, double time, const LocalCoordinate &x,
                   const VeloRangeType &v, const RangeType &u, FluxRangeType &f ) const
  {
    for( int r = 0; r < dimRange; ++r )
    {
      for( int d = 0; d < dimDomain; ++d )
        f[ r ][ d ] = v[ d ]*u[ r ];
    }
  }

#if 0
  void velocity ( const EntityType &entity, double time, const LocalCoordinate &x,
                  DomainType &v ) const
  {
    v = 0;
  }
#endif

  void diffusion1 ( const EntityType &entity, double time, const LocalCoordinate &x,
                    const RangeType &u, DiffusionRangeType &a ) const
  {
    a = 0;
    for( int r = 0; r < dimRange; ++r )
    {
      for( int d = 0; d < dimDomain; ++d )
        a[ r*dimDomain + d ][ d ]= u[ r ];
    }
  }

  double diffusion2 ( const EntityType &entity, double time, const LocalCoordinate &x,
                      const RangeType &u, const GradientType &v, FluxRangeType &A ) const
  {
    for( int r = 0; r < dimRange; ++r )
    {
      for( int d = 0; d < dimDomain; ++d )
        A[ r ][ d ] = diffusion_ * v[ r*dimDomain + d ];
    }
    return tstep_eps;
  }

  void source ( const EntityType &entity, double time, const LocalCoordinate &x,
                const VeloJacobianType &gradVelo, const RangeType &u, RangeType &source ) const
  {
    DomainType xGlobal = entity.geometry().global( x );
    problem_.evaluateRhs ( xGlobal, time, source );
  }

  bool hasBoundaryValue ( const Intersection &it, double time, const FaceLocalCoordinate &x ) const
  {
    return false;
  }

  double boundaryFlux1 ( const Intersection &it, double time, const FaceLocalCoordinate &x,
                         const RangeType &uLeft, RangeType &gLeft ) const
  {
    gLeft = 0;
    return 0;
  }

  double boundaryFlux2 ( const Intersection &it, double time, const FaceLocalCoordinate &x,
                         const VeloRangeType &vel, const RangeType &uLeft, RangeType &gLeft ) const
  {
    DomainType normal = it.unitOuterNormal( x );

    gLeft = 0;
    if( normal*vel > 0 )
      gLeft.axpy( normal*vel, uLeft );
    return 0;
  }

  void boundaryValue ( const Intersection &it, double time, const FaceLocalCoordinate &x,
                       const RangeType &uLeft, RangeType &uRight ) const
  {}

  double diffusionTimeStep () const { return 2 * tstep_eps; }

  double diffusionCoefficient () const { return diffusion_; }

protected:
  const ProblemType &problem_;
  double diffusion_;
  double tstep_eps;
};



/**
 * @brief defines the advective flux
 */
template <class ModelType>
class UpwindFlux {
public:
  typedef ModelType Model;
  typedef typename Model::Traits Traits;
  typedef typename Traits :: Intersection                 Intersection;
  typedef typename Traits :: FaceLocalCoordinate          FaceLocalCoordinate;
  typedef typename Model::DomainType                      DomainType;
  typedef typename Model::RangeType                       RangeType;
  typedef typename Model::FluxRangeType                   FluxRangeType;
  typedef typename Model::DiffusionRangeType              DiffusionRangeType;
  typedef typename Model::VeloRangeType                   VeloRangeType;

  /**
   * @brief Constructor
   */
  UpwindFlux(const Model& mod) : model_(mod) {}

  const Model& model() const {return model_;}

  double numericalFlux( const Intersection& it,
                        double time,
                        const FaceLocalCoordinate& x,
                        const VeloRangeType &vLeft,
                        const VeloRangeType &vRight,
                        const RangeType& uLeft,
                        const RangeType& uRight,
                        RangeType& gLeft,
                        RangeType& gRight ) const
  {
    const DomainType normal = it.integrationOuterNormal(x);
    VeloRangeType meanVelo ( vRight );
    meanVelo += vLeft;
    meanVelo *= 0.5;
    const double upwind = normal * meanVelo;

    if (upwind>0)
      gLeft = uLeft;
    else
      gLeft = uRight;
    gLeft *= upwind;
    gRight = gLeft;
    return std::abs(upwind);
  }
private:
  const Model& model_;
};

#endif // #ifndef MODELS_HH
