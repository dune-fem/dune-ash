#ifndef DISCRETEMODELS_HH
#define DISCRETEMODELS_HH

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/gridpart/common/gridpart.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/pass/localdg/discretemodel.hh>
#include <dune/fem/pass/localdg.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/space/discontinuousgalerkin.hh>

#include "../common/discretizationtraits.hh"

namespace Dune
{

  // Internal Forward Declarations
  // -----------------------------

  template< class Model, int polOrd, int passVId, int passUId >
  class AdvDiffDModel1;

  template< class Model, class NumFlux, int polOrd, int passVId, int passUId, int passGradId >
  class AdvDiffDModel2; 


  // PassTraits
  // ----------

  template< class Model, int dimRange, int polOrd >
  struct PassTraits
  {
    typedef typename Model::Traits ModelTraits;

    typedef typename ModelTraits::GridPartType GridPartType;
    //typedef typename GridPartType :: GridType                        GridType;
    static const int dimDomain = ModelTraits::dimDomain;

    typedef Fem::CachingQuadrature< GridPartType, 0 > VolumeQuadratureType;
    typedef Fem::CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

    typedef typename DiscretizationTraits::SolutionSpaceType::ToNewDimRange< dimRange >::Type DiscreteFunctionSpaceType;

    typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;

    typedef Fem::ISTLBlockVectorDiscreteFunction< DiscreteFunctionSpaceType > DestinationType;
//    typedef Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DestinationType;
  };



  // AdvDiffTraits1
  // --------------

  template< class Model, int polOrd, int passVId, int passUId >
  struct AdvDiffTraits1
  {
    typedef typename Model :: Traits                                 ModelTraits;
    typedef typename ModelTraits :: GridType                         GridType;

    enum { dimRange = ModelTraits::dimGradRange };
    enum { dimDomain = ModelTraits::dimDomain };

    typedef PassTraits< Model, dimRange, polOrd >                    Traits;
    typedef typename Traits :: FunctionSpaceType                     FunctionSpaceType;

    typedef typename Traits :: VolumeQuadratureType                  VolumeQuadratureType;
    typedef typename Traits :: FaceQuadratureType                    FaceQuadratureType;
    typedef typename Traits :: GridPartType                          GridPartType;

    typedef typename Traits :: DiscreteFunctionSpaceType             DiscreteFunctionSpaceType;
    typedef typename Traits :: DestinationType                       DestinationType;
    typedef DestinationType                                          DiscreteFunctionType;

    typedef typename DestinationType :: DomainType                   DomainType;
    typedef typename ModelTraits :: FaceLocalCoordinate              FaceLocalCoordinate;
    typedef typename ModelTraits :: LocalCoordinate                  LocalCoordinate;
    typedef typename DestinationType :: RangeType                    RangeType;
    typedef typename DestinationType :: JacobianRangeType            JacobianRangeType;

    typedef AdvDiffDModel1< Model, polOrd, passVId, passUId > DGDiscreteModelType;
  };

  template <class Model, class NumFlux, int polOrd, int passVId, int passUId, int passGradId >
  struct AdvDiffTraits2
  {
    typedef typename Model :: Traits                                 ModelTraits;
    typedef typename ModelTraits :: GridType                         GridType;

    enum { dimRange = ModelTraits::dimRange };
    enum { dimDomain = ModelTraits::dimDomain };

    typedef PassTraits< Model, dimRange, polOrd >                    Traits;
    typedef typename Traits :: FunctionSpaceType                     FunctionSpaceType;

    typedef typename Traits :: VolumeQuadratureType                  VolumeQuadratureType;
    typedef typename Traits :: FaceQuadratureType                    FaceQuadratureType;
    typedef typename Traits :: GridPartType                          GridPartType;
    typedef typename Traits :: DiscreteFunctionSpaceType             DiscreteFunctionSpaceType;
    typedef typename Traits :: DestinationType                       DestinationType;
    typedef DestinationType                                          DiscreteFunctionType;

    typedef typename DestinationType :: DomainType                   DomainType;
    typedef typename ModelTraits :: FaceLocalCoordinate              FaceLocalCoordinate;
    typedef typename ModelTraits :: LocalCoordinate                  LocalCoordinate;
    typedef typename DestinationType :: RangeType                    RangeType;
    typedef typename DestinationType :: JacobianRangeType            JacobianRangeType;

    typedef AdvDiffDModel2< Model, NumFlux, polOrd, passVId, passUId, passGradId > DGDiscreteModelType;
  };



  // AdvDiffDModel1
  // --------------

  template< class Model, int polOrd, int passVId, int passUId >
  class AdvDiffDModel1
  : public Fem::DGDiscreteModelDefaultWithInsideOutside< AdvDiffTraits1< Model, polOrd, passVId, passUId >, passVId, passUId >
  {
    typedef Fem::DGDiscreteModelDefaultWithInsideOutside< AdvDiffTraits1< Model, polOrd, passVId, passUId >, passVId, passUId > BaseType;

    using BaseType::numericalFlux;
    using BaseType::boundaryFlux;

    // This type definition allows a convenient access to arguments of passes.
    std::integral_constant< int, passUId > uVar;

  public:
    typedef AdvDiffTraits1< Model, polOrd, passVId, passUId > Traits;

    typedef typename Traits :: DomainType                            DomainType;
    typedef typename Traits :: LocalCoordinate                       LocalCoordinate;
    typedef typename Traits :: FaceLocalCoordinate                   FaceLocalCoordinate;
    typedef typename Traits :: RangeType                             RangeType;
    typedef typename Traits :: GridType                              GridType;
    typedef typename Traits :: JacobianRangeType                     JacobianRangeType;
    typedef typename Traits :: GridPartType :: IntersectionType      Intersection;
    typedef typename GridType :: template Codim< 0 > :: Entity       EntityType;

  public:
    explicit AdvDiffDModel1 ( const Model &model )
    : model_( model ),
      cflDiffinv_( 2 * (polOrd + 1) )
    {}

    bool hasSource () const { return false; }
    bool hasFlux () const { return true; } 

    template< class ArgumentTuple >
    double numericalFlux ( const Intersection &it, double time, const FaceLocalCoordinate &x,
                           const ArgumentTuple &uLeft, const ArgumentTuple &uRight,
                           RangeType &gLeft, RangeType &gRight )
    {
      JacobianRangeType diffmatrix;
      const DomainType normal = it.integrationOuterNormal( x );

      gLeft = 0;
      model_.diffusion1( this->inside(), time, it.geometryInInside().global( x ), uLeft[ uVar ], diffmatrix );
      diffmatrix.umv( normal, gLeft );
      model_.diffusion1( this->outside(), time, it.geometryInOutside().global( x ), uRight[ uVar ], diffmatrix );
      diffmatrix.umv( normal, gLeft );
      gLeft *= 0.5;
      
      gRight = gLeft;

      // upper bound for the next time step length
      return model_.diffusionTimeStep() * cflDiffinv_;
    }

    template< class ArgumentTuple, class FaceDom >
    double boundaryFlux ( const Intersection &it, double time, const FaceDom &x,
                          const ArgumentTuple &uLeft, RangeType &gLeft )
    {
      JacobianRangeType diffmatrix;
      const DomainType normal = it.integrationOuterNormal( x );

      gLeft = 0;
      model_.diffusion1 ( this->inside(), time, it.geometryInInside().global( x ), uLeft[ uVar ], diffmatrix );
      diffmatrix.umv( normal, gLeft );

      return 0.5 * model_.diffusionTimeStep() * cflDiffinv_;
    }

    template< class ArgumentTuple >
    void analyticalFlux ( const EntityType &en, double time, const LocalCoordinate &x,
                          const ArgumentTuple &u, JacobianRangeType &f )
    {
      model_.diffusion1( en, time, x, u[ uVar ], f );
    } 

  private:
    const Model &model_;
    const double cflDiffinv_;
  };



  template< class Model, class NumFlux, int polOrd, int passVId, int passUId, int passGradId >
  class AdvDiffDModel2
  : public Fem::DGDiscreteModelDefaultWithInsideOutside< AdvDiffTraits2< Model, NumFlux, polOrd, passVId, passUId, passGradId >, passVId, passUId, passGradId >
  {
    typedef Fem::DGDiscreteModelDefaultWithInsideOutside< AdvDiffTraits2< Model, NumFlux, polOrd, passVId, passUId, passGradId >, passVId, passUId, passGradId > BaseType;

  public:
    typedef AdvDiffTraits2< Model, NumFlux, polOrd, passVId, passUId, passGradId > Traits;

    typedef typename Traits :: DomainType                            DomainType;
    typedef typename Traits :: LocalCoordinate                       LocalCoordinate;
    typedef typename Traits :: FaceLocalCoordinate                   FaceLocalCoordinate;

    typedef typename Traits :: RangeType                             RangeType;
    typedef typename Traits :: GridType                              GridType;
    typedef typename Traits :: JacobianRangeType                     JacobianRangeType;

    typedef typename Traits :: GridPartType :: IntersectionType      Intersection;
    typedef typename GridType :: template Codim< 0 > :: Entity       EntityType;

    AdvDiffDModel2(const Model& mod,const NumFlux& numf) 
    : model_( mod ),
      numFlux_( numf ),
      penalty_( 2.5 ), 
      cflDiffinv_(2.*(polOrd+1.) )
    {}

    using BaseType::numericalFlux;
    using BaseType::boundaryFlux;

    bool hasSource () const { return true; }
    bool hasFlux () const { return true; }

    template< class ArgumentTuple, class JacobianTuple >
    double source ( const EntityType &entity, double time, const DomainType &x,
                    const ArgumentTuple &u, const JacobianTuple &j, RangeType &s ) const
    {
      model_.source( entity, time, x, Dune::get< 0 >( j ), Dune::get< 1 >( u ), s );
      return 0.0;
    }

    template< class ArgumentTuple >
    double numericalFlux ( const Intersection &intersection, double time, const FaceLocalCoordinate &x,
                           const ArgumentTuple &uLeft, const ArgumentTuple &uRight,
                           RangeType &gLeft, RangeType& gRight )
    {
      const double waveSpeed
        = numFlux_.numericalFlux( intersection, time, x, Dune::get< 0 >( uLeft ), Dune::get< 0 >( uRight ), Dune::get< 1 >( uLeft ), Dune::get< 1 >( uRight ), gLeft, gRight );

      const DomainType normal = intersection.integrationOuterNormal( x );
      JacobianRangeType diffmatrix;
      RangeType diffflux( 0.0 );
      model_.diffusion2( this->inside(), time, intersection.geometryInInside().global( x ), Dune::get< 1 >( uLeft ), Dune::get< 2 >( uLeft ), diffmatrix );
      diffmatrix.umv( normal, diffflux );
      model_.diffusion2( this->outside(), time, intersection.geometryInOutside().global( x ), Dune::get< 1 >( uRight ), Dune::get< 2 >( uRight ), diffmatrix );
      diffmatrix.umv( normal, diffflux );
      diffflux *= 0.5;

      const double factor = penalty_ * model_.diffusionCoefficient() * normal.two_norm2() / (0.5 * (this->enVolume() + this->nbVolume()));
      diffflux.axpy( factor, (Dune::get< 1 >( uLeft ) - Dune::get< 1 >( uRight )) );

      gLeft += diffflux;
      gRight += diffflux;
      return std::max( waveSpeed, factor*cflDiffinv_ );
    }

    template< class ArgumentTuple >
    double boundaryFlux ( const Intersection &intersection, double time, const FaceLocalCoordinate &x,
                          const ArgumentTuple &uLeft, RangeType &gLeft )
    {
      const double waveSpeed = 0.0;

      const DomainType normal = intersection.integrationOuterNormal( x );
      model_.boundaryFlux2( intersection, time, x, Dune::get< 0 >( uLeft ), Dune::get< 1 >( uLeft ), gLeft );
      gLeft *= normal.two_norm();

      return waveSpeed;
    }

    template< class ArgumentTuple >
    void analyticalFlux ( const EntityType &entity, double time, const LocalCoordinate &x,
                          const ArgumentTuple &u, JacobianRangeType &f )
    {
      model_.advection( entity, time, x, Dune::get< 0 >( u ), Dune::get< 1 >( u ), f );
      JacobianRangeType diffmatrix;
      model_.diffusion2( entity, time, x, Dune::get< 1 >( u ), Dune::get< 2 >( u ), diffmatrix );
      f += diffmatrix;
    }

  private:
    const Model &model_;
    const NumFlux &numFlux_;
    double penalty_;
    double cflDiffinv_;
  };

} // namespace Dune

#endif // #ifndef DISCRETEMODELS_HH
