#include <config.h>

#include <iostream>
#include <string>

#include <dune/fem/operator/projection/l2projection.hh>
#include <dune/fem/gridpart/common/gridpart.hh>
#include <dune/fem/solver/odesolver.hh>

#include "../common/discretizationtraits.hh"
#include "../common/filemanager.hh"
#include "stepper.hh"


// main 
// ----

int main ( int argc, char **argv )
try
{
  // Initialize MPI (always do this even if you are not using MPI)
  Dune::Fem::MPIManager::initialize( argc, argv );

  // Initialize parameter data base
  Dune::Fem::Parameter::append( argc, argv );
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // load the parameter files required by ash
  Ash::FileManager::appendParameterFile( "general.param" );
  Ash::FileManager::appendParameterFile( "adrsolver.param" );

  // load the session parameters
  const std::string session = Dune::Fem::Parameter::getValue< std::string >( "ash.session" );
  if( Dune::Fem::fileExists( session + "/adr.param" ) )
    Dune::Fem::Parameter::append( session + "/adr.param" );

  // get number of desired threads (default is 1)
  Dune::Fem::ThreadManager::setMaxNumberThreads( 1 );

  // ProblemType is a Dune::Function that evaluates to $u_0$ and also has a
  // method that gives you the exact solution.
  typedef DiscretizationTraits::GridType GridType;
  typedef Dune::U0< GridType > ProblemType;
  ProblemType problem;

  // read grid
  const std::string gridfile = Ash::FileManager::macroGridFile();
  Dune::GridPtr< GridType > gridptr( gridfile );
  Dune::Fem::Parameter::appendDGF( gridfile );
  GridType &grid = *gridptr;

  // refine the grid until the startLevel is reached
  const int refinements = Dune::Fem::Parameter::getValue< int >( "ash.refinements", 0 );
  for( int level = 0; level < refinements; ++level )
    Dune::Fem::GlobalRefine::apply( grid, 1 );

  typedef Stepper< GridType, ProblemType > Algorithm;
  Algorithm algorithm( grid, problem );
  Algorithm::DiscreteSpaceType &space = algorithm.space();
  Algorithm::DiscreteFunctionType u( "solution", space );
  algorithm( u ); 
  algorithm.finalize(u);

  // log all parameters
  Dune::Fem::Parameter::write( "parameter.log" );

  return 0;
}
catch( const Dune::Exception &e )
{                           
  std::cerr << e << std::endl;
  return 1;
}
catch( ... )
{
  std::cerr << "Generic exception caught." << std::endl;
  return 2;
}                                                    
