#ifndef DUNE_DGOPERATORS_HH
#define DUNE_DGOPERATORS_HH

#include <string>

#include <dune/fem/gridpart/common/gridpart.hh>
#include <dune/fem/pass/localdg.hh>
#include <dune/fem/pass/insertfunction.hh>
#include <dune/fem/operator/common/spaceoperatorif.hh>
#include <dune/fem/space/discontinuousgalerkin.hh>

#include "../common/discretizationtraits.hh"
#include "discretemodels.hh"


namespace Dune 
{

  template< class Model, template< class > class NumFlux, int polOrd >
  struct DGAdvectionDiffusionOperator
  : public Fem::SpaceOperatorInterface< typename PassTraits< Model, Model::Traits::dimRange, polOrd >::DestinationType >
  {
    enum PassIdType { u, veloPass, gradPass, advectPass };   

    static const int dimRange  = Model::dimRange;
    static const int dimDomain = Model::Traits::dimDomain;

    typedef NumFlux< Model > NumFluxType;

    typedef AdvDiffDModel1< Model, polOrd, veloPass, u > DiscreteModel1Type; 
    typedef AdvDiffDModel2< Model, NumFluxType, polOrd, veloPass, u, gradPass > DiscreteModel2Type;

    typedef typename DiscreteModel1Type :: Traits                      Traits1;
    typedef typename DiscreteModel2Type :: Traits                      Traits2;

    typedef typename Model :: Traits :: GridType                       GridType;

    typedef typename Traits2 :: DomainType                             DomainType;
    typedef typename Traits2 :: DiscreteFunctionType                   DiscreteFunction2Type;

    typedef typename Traits1 :: DiscreteFunctionSpaceType              Space1Type;
    typedef typename Traits2 :: DiscreteFunctionSpaceType              Space2Type;
    typedef typename Traits1 :: DestinationType                        Destination1Type;
    typedef typename Traits2 :: DestinationType                        Destination2Type;
    typedef Destination2Type                                           DestinationType;
    typedef Space2Type                                                 SpaceType;

    typedef typename Traits1 :: GridPartType                           GridPartType;

    typedef typename Model :: VeloDFSpace                              VeloDFSpace;
    typedef typename Model :: VeloDFType                               VeloDFType;

    typedef Fem::StartPass< DiscreteFunction2Type, u > Pass0Type;
    typedef Fem::InsertFunctionPass< VeloDFType, Pass0Type, veloPass > Pass05Type;
    typedef Fem::LocalDGPass< DiscreteModel1Type, Pass05Type, gradPass > Pass1Type;
    typedef Fem::LocalDGPass< DiscreteModel2Type, Pass1Type, advectPass > Pass2Type;

  public:
    DGAdvectionDiffusionOperator ( GridType &grid, const NumFluxType &numFlux )
    : grid_( grid ),
      model_( numFlux.model() ),
      gridPart_( grid_, grid_.maxLevel() ),
      veloSpace_( gridPart_ ),
      velo_( "velocity", veloSpace_ ),
      space1_( gridPart_ ),
      space2_( gridPart_ ),
      discreteModel1_( model_ ),
      discreteModel2_( model_, numFlux ),
      pass05_ ( velo_, pass0_ ), 
      pass1_ ( discreteModel1_, pass05_, space1_), 
      pass2_ ( discreteModel2_, pass1_, space2_)  
    {
      // read velocity funciton in
      const std::string session = Dune::Fem::Parameter::getValue< std::string >( "ash.session" );
      Dune::Fem::SerialOutputMap< VeloDFType > map( velo_ );
      map.read( session, "windfield.xdr" );
    }

    void operator() ( const DestinationType &u, DestinationType &w ) const { pass2_( u, w ); }

    double timeStepEstimate () const { return pass2_.timeStepEstimate(); }

    void setTime ( double time ) { pass2_.setTime( time ); }

    const SpaceType &space () const { return space2_; }

  private:
    GridType &grid_;
    const Model &model_;
    GridPartType gridPart_;
    VeloDFSpace veloSpace_;
    VeloDFType velo_;
    Space1Type space1_;
    Space2Type space2_;
    DiscreteModel1Type discreteModel1_;
    DiscreteModel2Type discreteModel2_;
    Pass0Type pass0_;
    Pass05Type pass05_;
    Pass1Type pass1_;
    Pass2Type pass2_;
  };

} // namespace Dune

#endif // #ifndef DUNE_DGOPERATORS_HH
