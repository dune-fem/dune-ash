#ifndef STEPPER_HH
#define STEPPER_HH

#include <iostream>
#include <string>

#include <dune/fem/operator/projection/l2projection.hh>
#include <dune/fem/solver/odesolver.hh>
#include <dune/fem/solver/rungekutta/implicit.hh>

#include <dune/fem/solver/pardginverseoperators.hh>
#include <dune/fem/solver/istlinverseoperators.hh>
#include <dune/fem/solver/newtoninverseoperator.hh>

#include <dune/fem/operator/dghelmholtz.hh>

#include "../common/discretizationtraits.hh"
#include "baseevolution.hh"
#include "models.hh"

// approximation order
const int order   = POLORDER;
const int rkSteps = POLORDER + 1;

template< class GridImp, class InitialDataType >
struct StepperTraits
{
  // type of Grid
  typedef GridImp GridType;
  typedef DiscretizationTraits::GridPartType GridPartType;

  // An analytical version of our model
  typedef AdvectionDiffusionModel< GridPartType, InitialDataType >   ModelType;

  // The flux for the discretization of advection terms
  typedef UpwindFlux< ModelType >                                    FluxType;

  // The DG Operator (using 2 Passes)
  typedef Dune::DGAdvectionDiffusionOperator< ModelType, UpwindFlux,
                                              order >                DgType; 

  // The discrete function for the unknown solution is defined in the DgOperator
  typedef typename DgType::DestinationType                         DiscreteFunctionType;
  // ... as well as the Space type
  typedef typename DgType::SpaceType                               DiscreteSpaceType;

  typedef DuneODE::OdeSolverInterface< DiscreteFunctionType >      OdeSolverInterfaceType;

  typedef Dune::Fem::DGHelmholtzOperator< DgType >                 HelmHoltzOperatorType;
  typedef typename HelmHoltzOperatorType :: JacobianOperatorType   JacobianOperatorType;

//  typedef Dune::Fem::ParDGGeneralizedMinResInverseOperator< DiscreteFunctionType > LinearOperatorType;
  typedef Dune::Fem::ISTLInverseOperator< DiscreteFunctionType, Dune::Fem::ISTLRestartedGMRes  > LinearOperatorType;
  typedef Dune::Fem::NewtonInverseOperator< JacobianOperatorType, LinearOperatorType > NonLinearSolverType;

  // The solver zoo
  typedef DuneODE::ImplicitRungeKuttaSolver< HelmHoltzOperatorType, NonLinearSolverType > ImplicitOdeSolverType;

  /*
  // The ODE Solvers
  typedef DuneODE::ImplicitOdeSolver< DiscreteFunctionType >       ImplicitOdeSolverType;
  */

  // type of restriction/prolongation projection for adaptive simulations 
  typedef Dune::Fem::RestrictProlongDefault< DiscreteFunctionType > RestrictionProlongationType;
};

template< class GridImp, class InitialDataType >
struct Stepper
: public AlgorithmBase< StepperTraits< GridImp, InitialDataType> >
{
  // my traits class 
  typedef StepperTraits< GridImp, InitialDataType> Traits ;

  // my base class 
  typedef AlgorithmBase < Traits > BaseType;

  // type of Grid
  typedef typename Traits::GridType                  GridType;

  // An analytical version of our model
  typedef typename Traits::ModelType                 ModelType;

  // The flux for the discretization of advection terms
  typedef typename Traits::FluxType                  FluxType;

  // The DG Operator (using 2 Passes)
  typedef typename Traits::DgType                    DgType;

  typedef typename Traits::HelmHoltzOperatorType      HelmHoltzOperatorType;

  // The discrete function for the unknown solution is defined in the DgOperator
  typedef typename Traits::DiscreteFunctionType      DiscreteFunctionType;

  // ... as well as the Space type
  typedef typename Traits::DiscreteSpaceType         DiscreteSpaceType;

  // The ODE Solvers
  typedef typename Traits::OdeSolverInterfaceType    OdeSolverInterfaceType;
  typedef typename Traits::ImplicitOdeSolverType     ImplicitOdeSolverType;

  typedef typename BaseType::TimeProviderType   TimeProviderType;

  using BaseType::grid_;

  // brief constructor of the stepper class
  Stepper(GridType& grid, InitialDataType& problem) :
    BaseType ( grid ),
    problem_(problem),
    model_(problem_),
    convectionFlux_(model_),
    dgOperator_(grid_, convectionFlux_),
    dgHelmHolz_( dgOperator_ ),
    odeSolver_(0)
  {}

  ~Stepper () { delete odeSolver_; }

  InitialDataType &problem () const { return *problem_; }

  const ModelType &model () const { return model_; }

  // before first step, do data initialization 
  void initializeStep(TimeProviderType& tp, DiscreteFunctionType& u)
  {
    // create implicit or explicit ode solver
    odeSolver_ = new ImplicitOdeSolverType(dgHelmHolz_, tp, rkSteps);
    assert( odeSolver_ );

    Dune::Fem::L2Projection< InitialDataType, DiscreteFunctionType > l2pro;
    l2pro(problem_, u);                                  

    // odeSolver_->initialize applies the DG Operator once to get an initial
    // estimate on the time step. This does not change the initial data u.
    odeSolver_->initialize(u);                          
  }

  // solve ODE for one time step 
  void step(TimeProviderType& tp, DiscreteFunctionType& u) 
  {
    assert(odeSolver_);
    odeSolver_->solve(u);
  }

  // after last step, do EOC calculation 
  void finalizeStep(TimeProviderType& tp, DiscreteFunctionType& u)
  {
    // delete ode solver
    delete odeSolver_;
    odeSolver_ = 0;
  }

private:
  // InitialDataType is a Dune::Operator that evaluates to $u_0$ and also has a
  // method that gives you the exact solution.
  InitialDataType  &problem_;
  ModelType               model_;
  // Initial flux for advection discretization (UpwindFlux)
  FluxType                convectionFlux_;
  DgType                  dgOperator_;
  HelmHoltzOperatorType   dgHelmHolz_;
  OdeSolverInterfaceType *odeSolver_;
};

#endif // #ifndef STEPPER_HH
