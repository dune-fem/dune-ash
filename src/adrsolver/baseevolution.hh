#ifndef FEMHOWTO_BASEEVOL_HH
#define FEMHOWTO_BASEEVOL_HH

#include <sstream>

#include <dune/fem/writerutilities.hh>


template< class Traits >
struct AlgorithmBase
{
  typedef typename Traits::GridType GridType;
  typedef typename Traits::GridPartType GridPartType;
  typedef typename Traits::DiscreteSpaceType DiscreteSpaceType;
  typedef typename Traits::DiscreteFunctionType DiscreteFunctionType;
  typedef Dune::Fem::GridTimeProvider< GridType > TimeProviderType;

  AlgorithmBase ( GridType &grid )
  : grid_( grid ),
    gridPart_( grid_, grid_.maxLevel() ),
    space_( gridPart_ ),
    saveStep_( Dune::Fem::Parameter::getValue< double > ( "ash.savestep" ) ),
    saveTime_( 0 )
  {}

  bool willWrite ( TimeProviderType &tp ) const
  {
    const bool truelyWrite = (saveTime_ < tp.time());
    saveTime_ += (truelyWrite ? saveStep_ : 0);
    return truelyWrite;
  }
  
  DiscreteSpaceType &space () { return space_; }

  virtual void initializeStep ( TimeProviderType &tp, DiscreteFunctionType &solution ) = 0;

  virtual void step ( TimeProviderType &tp, DiscreteFunctionType &solution ) = 0;

  virtual void finalizeStep ( TimeProviderType &tp, DiscreteFunctionType &solution ) = 0;

  virtual void operator () ( DiscreteFunctionType &solution )
  {
    const std::string basefilename = "adr.solution";

    const double startTime = Dune::Fem::Parameter::getValue< double >( "ash.startTime", 0.0 );
    saveTime_ = startTime;
    const double endTime = Dune::Fem::Parameter::getValue<double>( "ash.endTime", 1.0 );
    const std::string session = Dune::Fem::Parameter::getValue< std::string >( "ash.session" );

    const std::string path = session + "/adrdata";
    Dune::Fem::createDirectory( path );

    TimeProviderType tp( startTime, grid_ ); 
    initializeStep( tp, solution );

    int writeCounter = 0;
    writeSerialXdr< DiscreteFunctionType >( path, basefilename, solution, writeCounter );
    ++writeCounter;

    tp.provideTimeStepUpperBound( 0.5 * saveStep_ );

    for( tp.init(); tp.time() < endTime; tp.next() )
    {
      tp.provideTimeStepUpperBound( 0.5 * saveStep_ );

      step( tp, solution );

      if( !solution.dofsValid() )
        abort();

      if( willWrite( tp ) )
      {
        writeSerialXdr< DiscreteFunctionType >( path, basefilename, solution, writeCounter );
        ++writeCounter;
      }
    } 

    // write last time step  
    writeSerialXdr< DiscreteFunctionType >( path, basefilename, solution, writeCounter );

    // finalize time step 
    finalizeStep( tp, solution );

    // wait for all procs to reach this point 
    grid_.comm().barrier();
    if( grid_.comm().rank() == 0 ) 
    {
      std::string filename =  session + "/adr.done";
      std::ofstream endtoken( filename.c_str() );
      if( endtoken.is_open() ) 
        endtoken << "The eagle has landed on his beagle!" << std::endl;
      else 
        std::cerr << "Couldn't open file '" << filename << "'" << std::endl;
    }
  }

  void finalize ( DiscreteFunctionType &solution ) {}

protected:      
  GridType &grid_;
  GridPartType gridPart_;
  DiscreteSpaceType space_;
  const double saveStep_;
  mutable double saveTime_;
}; 

#endif // #ifndef FEMHOWTO_BASEEVOL_HH
