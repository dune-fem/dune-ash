#ifndef PROBLEM_ASH_HH
#define PROBLEM_ASH_HH

#include <dune/common/fmatrix.hh>

#include <dune/fem/space/common/functionspace.hh>

namespace Dune
{

  template< class GridType >
  struct U0 
  : public Fem::Function< Fem::FunctionSpace< double, double, GridType::dimensionworld, 1 >, U0< GridType > >
  { 
    typedef U0< GridType > ThisType;
    typedef Fem::Function< Fem::FunctionSpace< double, double, GridType::dimensionworld, 1 >, U0< GridType > > BaseType;

  public:
    typedef typename BaseType::FunctionSpaceType                  FunctionSpaceType;

    typedef typename FunctionSpaceType::DomainType                DomainType;
    typedef typename FunctionSpaceType::RangeType                 RangeType;
    typedef typename FunctionSpaceType::DomainFieldType           DomainFieldType;
    typedef typename FunctionSpaceType::RangeFieldType            RangeFieldType;

    typedef typename GridType::template Codim< 0 >::Entity Entity;

    U0 () 
    : startTime_( Fem::Parameter::getValue< double >( "ash.startTime",0.0 ) ),
      concentration_( Fem::Parameter::getValue< double >( "ash.volcano.concentration" ) ),
      radius_( Fem::Parameter::getValue< double >( "ash.volcano.radius" ) ),
      diffusion_( Fem::Parameter::getValue< double >( "ash.diffusion", 0.01 ) )
    {                                                            
      position_ = Fem::Parameter::getValue< DomainType >( "ash.volcano.position" );
    }

    double startTime () const { return startTime_; }

    double diffusion () const { return diffusion_; }

    //void velocity ( const DomainType &x, DomainType &v ) const { v = 0; }

    void evaluate ( const DomainType &x, RangeType &res ) const
    {
      evaluate( x, startTime_, res );
    }

    void evaluate ( const DomainType &x, double t, RangeType &res ) const
    {
      res = 0;
    }

    void evaluateRhs ( const DomainType &x, double time, RangeType &source ) const
    {
      source = 0;
      DomainType distDisturb ( x );
      distDisturb.axpy( -1, position_ );
      if( distDisturb.two_norm() <= radius_ )
        source += concentration_;

      // scale such that source term is zero at initial time and increases quickly
      source *= (1 - std::exp( -100*time ) );

      // scale additionally with weighted Gaussian so that volcano eruption converges to zero for longer times
      source *= std::exp( -12.5*(time*time) );
    }

  private:
    double         startTime_;
    DomainType     position_;
    const double   concentration_;
    const double   radius_;
    const double   diffusion_;
  };

} // namespace Dune

#endif // #ifndef PROBLEM_ASH_HH
