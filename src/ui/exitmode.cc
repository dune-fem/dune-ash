// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/widget/button.hh>

#include "exitmode.hh"

using Dune::Ash::ButtonWidget;


struct ExitMode::EventRestart
: public Dune::Ash::Event
{
  explicit EventRestart ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent () 
  {
    modeStack_.setExitStatus( FrontendModeStack::StatusRestart );
    modeStack_.clear(); 
  }

private:
  FrontendModeStack &modeStack_;
};


struct ExitMode::EventShutdown
: public Dune::Ash::Event
{
  explicit EventShutdown ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent () 
  {
    modeStack_.setExitStatus( FrontendModeStack::StatusShutdown );
    modeStack_.clear(); 
  }

private:
  FrontendModeStack &modeStack_;
};

struct ExitMode::EventClose
: public Dune::Ash::Event
{
  explicit EventClose ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent () { modeStack_.pop(); }

private:
  FrontendModeStack &modeStack_;
};


// Implementation of ExitMode
// --------------------------

ExitMode::ExitMode( FrontendModeStack &modeStack )
: FrontendMode( modeStack ),
  father_( nullptr ),
  shrinkWidget_( 12.0f / 48.0f, 7.0f / 27.0f ),
  split_( 1, 6 ),
  controlArray_( 12, 1, { 1.0f, 1.0f, 1.0f }, { 0.0154f, 0.1f } ),
  buttonArray_( 2, 1, { 1.0f, 1.0f, 1.0f }, { 0.2f, 0.3f } )
{
  shrinkWidget_.setChild( &split_ );

  split_.setLeftChild( &controlArray_ );
  split_.setRightChild( &buttonArray_ );

  controlArray_.setChild( 11, 0, new ButtonWidget( new EventClose( modeStack ), modeStack.icon( 1 ) ) );

  buttonArray_.setChild( 0, 0, new ButtonWidget( new EventRestart( modeStack ), modeStack.icon( 11 ) ) );
  buttonArray_.setChild( 1, 0, new ButtonWidget( new EventShutdown( modeStack ), modeStack.icon( 12 ) ) ); 
}


ExitMode::~ExitMode ()
{
  for( std::size_t v = 0; v < buttonArray_.size()[ 1 ]; ++v )
  {
     for( std::size_t h = 0; h < buttonArray_.size()[ 0 ]; ++h )
    {
      Widget *widget = buttonArray_.setChild( h, v, nullptr );

      if( !widget )
        continue;

      Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( widget );
      if( button )
        delete button->setEvent( nullptr );

      delete widget;
    }
  }
  for( std::size_t v = 0; v < controlArray_.size()[ 1 ]; ++v )
  {
     for( std::size_t h = 0; h < controlArray_.size()[ 0 ]; ++h )
    {
      Widget *widget = controlArray_.setChild( h, v, nullptr );

      if( !widget )
        continue;

      Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( widget );
      if( button )
        delete button->setEvent( nullptr );

      delete widget;
    }
  }
}


void ExitMode::initializeMode ()
{
  father_ = modeStack().father( this );
}


void ExitMode::draw ()
{
  if( father_ )
    father_->draw();
  shrinkWidget_.draw();
}


void ExitMode::tick ( Ticks ticks )
{
  if( father_ )
    father_->tick( ticks );
  shrinkWidget_.tick( ticks );
}
