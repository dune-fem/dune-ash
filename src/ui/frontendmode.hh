// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef FRONTENDMODE_HH
#define FRONTENDMODE_HH

#include <cassert>
#include <iostream>
#include <map>
#include <string>

#include <SDL.h>

#include <dune/common/fvector.hh>

#include <dune/ash/opengl/texture.hh>
#include <dune/ash/widget/widget.hh>

#include "sharedinfo.hh"

class FrontendMode;



// FrontEndModeStack
// -----------------

class FrontendModeStack
{
  typedef FrontendModeStack ThisType;

  typedef std::vector< FrontendMode * > StackType;

public:
  struct TopWidget
    : public Dune::Ash::Widget
  {
    explicit TopWidget ( FrontendModeStack &modeStack )
      : modeStack_( modeStack )
    {}

    void draw ();

    void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

    void mouseButtonDown ( const Position &x, Ticks ticks );
    void mouseButtonUp ( const Position &x, Ticks ticks );

    void tick ( Ticks ticks );

  private:
    FrontendModeStack &modeStack_;
  };

  typedef Dune::Ash::Widget::Ticks Ticks;
  typedef std::map< std::string, std::string > InfoMap;

  enum ExitStatus { StatusRestart = 0, StatusShutdown = 1, StatusError = 0x10 };

  explicit FrontendModeStack ( SharedInformation *sharedInformation );

  ~FrontendModeStack () 
  { 
    clear(); 
    collectGarbage();
  }

  bool empty () const { return stack_.empty(); }

  const FrontendMode &top () const { return *stack_.back(); }
  FrontendMode &top () { return *stack_.back(); }

  void clear ();
  void push ( FrontendMode *mode );
  void pop ();
  void popAll ();

  TopWidget &topWidget () { return topWidget_; }

  void setLanguage ( std::size_t lang );
  std::size_t languages () const { return languagePaths_.size(); }
  const std::string &getLanguagePath () const { return languagePaths_[ currentLanguage_ ]; }

  FrontendMode *father ( FrontendMode *mode );

  SharedInformation *sharedInformation () { return sharedInformation_; } 

  const Dune::Ash::Texture &mapTexture () const { return mapTexture_; }
  const Dune::Ash::Texture &equationTexture () const { return equationTexture_; }
  const Dune::Ash::Texture &flagsTexture () const { return flagsTexture_; }

  Dune::Ash::BillBoard icon ( int icon );
  Dune::Ash::BillBoard icon3x2 ( int icon );

  Dune::Ash::BillBoard flag ( int flag );

  void setExitStatus ( ExitStatus status ) { exitStatus_ = status; }  
  const ExitStatus exitStatus () const { return exitStatus_; }

  const InfoMap &infoMap () const { return infoMap_; }

private:
  void collectGarbage ();

  FrontendModeStack ( const ThisType & );
  const ThisType &operator= ( const ThisType & );

  StackType stack_;
  std::stack< FrontendMode * > garbage_;
  TopWidget topWidget_;

  SharedInformation *sharedInformation_;

  std::size_t currentLanguage_;
  std::vector< std::string > languagePaths_;

  Dune::Ash::Texture mapTexture_;
  Dune::Ash::Texture equationTexture_;
  Dune::Ash::Texture flagsTexture_;
  Dune::Ash::Texture iconsTexture_;

  InfoMap infoMap_;

  ExitStatus exitStatus_;
};



/*
 * This file defines a basic virtual interface for a mode of the userinterface.
 * Modes will be - in our case - "drawing mode" (the user can draw the vector field
 * with the mouse) and "visualization mode" (the userinterface draws the calculations
 * which are performed by the backend)
 *
 * This interface should describe anything which is needed to communicate from the low level
 * class MainWindow (which does most of the SDL stuff) to the more abstract layers...
 */
class FrontendMode
  : public Dune::Ash::Widget
{
  typedef FrontendMode ThisType;

public:
  explicit FrontendMode ( FrontendModeStack &modeStack )
    : modeStack_( modeStack )
  {}

  virtual ~FrontendMode () {}

  // this is invoked to set up the mode (i.e. only once, not before each frame)
  virtual void initializeMode () {}

  // cleanUp is invoked when the mode is popped
  // note: The mode is deleted later by the garbage collector.
  virtual void cleanUp () {}

  virtual void setLanguage () {}

  /*
   * Some simple accessor methods
   */
  const FrontendModeStack &modeStack () const { return modeStack_; }
  FrontendModeStack &modeStack () { return modeStack_; }

private:
  // prohibit copying and assignment
  FrontendMode ( const ThisType & );
  const ThisType &operator= ( const ThisType & );

  FrontendModeStack &modeStack_;
};



// Implementation of FrontendModeStack
// -----------------------------------

inline void FrontendModeStack::push ( FrontendMode *mode )
{
  assert( mode );
  stack_.push_back ( mode );
  top().initializeMode();
}


inline void FrontendModeStack::pop ()
{
  stack_.back()->cleanUp();
  garbage_.push( stack_.back() );
  stack_.pop_back();
  if( !empty() )
    top().initializeMode();
}

inline void FrontendModeStack::popAll ()
{
  while( stack_.size() > 1 )
    pop();
}


inline Dune::Ash::BillBoard FrontendModeStack::icon ( int icon )
{
  Dune::FieldVector< GLfloat, 2 > texUpperLeft, texLowerRight;
  texUpperLeft[ 0 ] = GLfloat( icon % 4 )*0.25f;
  texUpperLeft[ 1 ] = GLfloat( icon / 4 )*0.25f;
  texLowerRight[ 0 ] = texUpperLeft[ 0 ] + 0.25f;
  texLowerRight[ 1 ] = texUpperLeft[ 1 ] + 0.25f;

  return Dune::Ash::BillBoard( iconsTexture_, texUpperLeft, texLowerRight );
}

inline Dune::Ash::BillBoard FrontendModeStack::icon3x2 ( int icon )
{
  Dune::FieldVector< GLfloat, 2 > texUpperLeft, texLowerRight;
  texUpperLeft[ 0 ] = GLfloat( icon % 4 )*0.25f + 0.005;
  texUpperLeft[ 1 ] = GLfloat( icon / 4 )*0.25f + 0.005;
  texLowerRight[ 0 ] = texUpperLeft[ 0 ] + 0.24f;
  texLowerRight[ 1 ] = texUpperLeft[ 1 ] + 0.24f;

  return Dune::Ash::BillBoard( { 1. / 6., 0., 0. }, { 5. / 6., 1., 0. }, iconsTexture_, texUpperLeft, texLowerRight );
}

inline Dune::Ash::BillBoard FrontendModeStack::flag ( int flag )
{
  Dune::FieldVector< GLfloat, 2 > texUpperLeft, texLowerRight;
  texUpperLeft[ 0 ] = GLfloat( flag % 4 )*0.25f + 0.005;
  texUpperLeft[ 1 ] = GLfloat( flag / 4 )*0.25f + 0.005;
  texLowerRight[ 0 ] = texUpperLeft[ 0 ] + 0.24f;
  texLowerRight[ 1 ] = texUpperLeft[ 1 ] + 0.24f;

  return Dune::Ash::BillBoard( flagsTexture_, texUpperLeft, texLowerRight );
}

#endif // #ifndef FRONTENDMODE_HH
