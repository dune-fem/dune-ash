#ifndef FILEPARSER_HH
#define FILEPARSER_HH

#include <string>
#include <fstream>
#include <map>

#include <dune/fem/io/parameter.hh>

#include "../common/filemanager.hh"
#include "frontendmode.hh"

struct FileParser
{
  typedef std::map< std::string, std::string > TextMap;

  FileParser ( const std::string &fileName )
  : fileName_( Dune::Fem::Parameter::getValue< std::string >( std::string( "ui.text." ) + fileName, fileName ) )
  {}

  void operator() ( const std::string &languagePath, TextMap &container ) const
  {
    std::string absFilename = languagePath + "/" + fileName_;

    std::ifstream file( absFilename );

    if( !file )
      DUNE_THROW( Dune::IOError, "File '" + fileName_ + "' not found for current language." );

    container.clear();
    std::string preamble;
    std::string *content = &preamble;

    while( file )
    {
      std::string line;
      std::getline( file, line);

      if( line.substr(0, 2) == "%[" )
      {
        std::size_t pos = line.find_first_of( ']', 2 );
        if (pos == line.npos )
          return; // throw 

        std::size_t begin = line.find_first_not_of( ' ', 2 );
        std::size_t end = line.find_last_not_of( ' ', pos - 1 );

        const std::string section = line.substr( begin, end - begin + 1 );
        content = &(container[ section ]);
        if( content->empty() )
          (*content) = preamble;
      }
      else
        (*content) += line + "\n";
    }
  }

private:
  const std::string fileName_;
};

#endif // #ifndef FILEPARSER_HH
