// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef INPUTMODE_HH
#define INPUTMODE_HH

#include <list>
#include <utility>

#include <dune/ash/widget/split.hh>

#include "frontendmode.hh"
#include "inputwidget.hh"
#include "sharedinfo.hh"
#include "panel.hh"


// InputMode
// ---------

class InputMode
: public FrontendMode
{
  typedef InputMode ThisType;
  typedef FrontendMode BaseType;

  struct UndoLastMove;
  struct ComputeWindField;
  struct ClearMousePath;
  struct WindFieldComputed;

public:
  InputMode ( FrontendModeStack &modeStack );
  ~InputMode ();

  void cleanUp ();

  void startSimulation ();

  void draw () { splitWidget_.draw(); }

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { splitWidget_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks ) { splitWidget_.tick( ticks ); }

  void setLanguage ();

private:
  Dune::Ash::SplitWidget< Dune::Ash::Horizontal > splitWidget_;
  InputWidget inputWidget_;
  Panel panel_;
  double diffusion_coef_;
  double perturbation_coef_;
  bool field_written_;
  SharedInformation *sharedInformation_;
};

#endif // INPUTMODE_HH
