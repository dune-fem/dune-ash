// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <dune/fem/io/io.hh>

#include <dune/ash/image.hh>
#include <dune/ash/opengl/opengl.hh>

#include "infomode.hh"
#include "mainmode.hh"
#include "volcanomode.hh"


// MainMode::EventNextMode
// -----------------------

struct MainMode::EventNextMode
: public Dune::Ash::Event
{
  explicit EventNextMode ( MainMode &mode )
  : mode_( mode )
  {}

  void executeEvent ()
  {
    mode_.sharedInformation_->reset();
    mode_.modeStack().push( new VolcanoMode( mode_.modeStack() ) );
  }

private:
  MainMode &mode_;
};



// MainMode::EventLink
// -------------------

struct MainMode::EventLink
: public Dune::Ash::Event
{
  EventLink ( FrontendModeStack &modeStack, const std::string &panel )
  : modeStack_( modeStack ),
    panel_( panel )
  {}

  void executeEvent () 
  {
    modeStack_.push( new InfoMode( modeStack_, panel_ ) );
  }

private:
  FrontendModeStack &modeStack_;
  std::string panel_;
};



// Implementation of MainMode
// --------------------------

MainMode::MainMode ( FrontendModeStack &modeStack )
: FrontendMode( modeStack ),
  sharedInformation_( modeStack.sharedInformation() ),
  splitWidget_( 9, 7 ),
  text_( { 120.0f, 120.0f }, { 9.0f, 9.0f } ),
  panel_( modeStack, Panel::IdMainMode )
{
  text_.setText( modeStack.getLanguagePath(), "news" );
  for( InfoIterator it = infoMap().begin(); it != infoMap().end(); ++it )
    text_.setEvent( std::string( "link:" ) + it->first, new EventLink( modeStack, it->first ) );

  splitWidget_.setLeftChild( &text_ );
  splitWidget_.setRightChild( &panel_ );

  panel_.setButton( 6, new Dune::Ash::ButtonWidget( new EventNextMode( *this ), modeStack.icon3x2( 6 ) ) );
}


MainMode::~MainMode ()
{
  text_.deleteEvents();
}


void MainMode::setLanguage ()
{
  panel_.setLanguage();
  text_.setText( modeStack().getLanguagePath(), "news" );

  text_.deleteEvents();
  for( InfoIterator it = infoMap().begin(); it != infoMap().end(); ++it )
    text_.setEvent( std::string( "link:" ) + it->first, new EventLink( modeStack(), it->first ) );

  // inject events into panel_ text_ once there is access to it
}


void MainMode::draw () 
{
  splitWidget_.draw();
}
