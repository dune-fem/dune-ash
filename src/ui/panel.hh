// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef PANEL_HH
#define PANEL_HH

#include <dune/ash/widget/button.hh>
#include <dune/ash/widget/grid.hh>
#include <dune/ash/widget/multiplesplit.hh>
#include <dune/ash/widget/slider.hh>
#include <dune/ash/widget/widget.hh>
#include <dune/ash/widget/floatingtext.hh>

#include "titlewidget.hh"
#include "fileparser.hh"


// Panel
// -----

class Panel
: public Dune::Ash::Widget
{
  typedef Panel ThisType;
  
  typedef FrontendModeStack::InfoMap InfoMap;
  typedef InfoMap::const_iterator InfoIterator;

  struct EventRestart;
  struct EventBack;
  struct EventLink;

public:
  enum ModeId { IdMainMode = 0, IdVolcanoMode = 1, IdInputMode = 2, IdDiffusionMode = 3, IdVisMode = 4 };

  Panel ( FrontendModeStack &modeStack, ModeId modeId, int extraHeight = 0 );
  ~Panel ();

  void draw () { split_.draw(); }

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { split_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { split_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { split_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks );

  void setLanguage ();

  // Button an slider sets
  Dune::Ash::Widget* setButton ( int pos, Dune::Ash::Widget* button );
  Dune::Ash::Widget* getButton ( int pos );
  void setExtraContent ( Dune::Ash::Widget *extraContent );

  Dune::Ash::Event* setTextEvent ( const std::string &id, Dune::Ash::Event* event );

private:
  // prohibit copying and assignment
  Panel ( const ThisType & );
  const ThisType &operator= ( const ThisType & );

  const InfoMap &infoMap () const { return modeStack_.infoMap(); }

  FrontendModeStack &modeStack_;
  Dune::Ash::MultipleSplitWidget< Dune::Ash::Vertical > split_;
  Dune::Ash::GridWidget bottomButtonArray_;
  Dune::Ash::FloatingTextWidget text_;

  TitleWidget title_;
  FileParser parser_;

  FileParser::TextMap textMap_;
  std::size_t modeId_;
};

#endif // #ifndef PANEL_HH
