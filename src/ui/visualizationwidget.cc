// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <dune/common/fvector.hh>

#include <dune/fem/io/parameter.hh>

#include "gridvisualization.hh"
#include "visualizationwidget.hh"



// Implementation of VisualizationWidget
// -------------------------------------

VisualizationWidget::VisualizationWidget ( const Dune::Ash::Texture &mapTexture,
                                           const Dune::Ash::BillBoard &iconVolcano )
: BaseType( mapTexture, iconVolcano ),
  gridPart_( nullptr ),
  windFieldVisualizer_( nullptr ),
  solutionTexture_( nullptr ),
  gridWarpFactor_( 1.0 ),
  vectorFieldColorScaling_( 15.0 * Dune::Fem::Parameter::getValue< double >( "windfield.amplifiction" ) )
{}


void VisualizationWidget::draw ()
{
  BaseType::draw();

  solutionDisplayList_();

  if( gridPart_ )
  {
    Dune::Ash::glColor( 0.0, 0.0, 0.0 );
    Dune::Ash::GridVisualization::drawGrid( *gridPart_, gridWarpFactor_ );
  }

  if( windFieldVisualizer_ )
    windFieldVisualizer_->visualizeVectorField( vectorFieldColorScaling_ );

}


void VisualizationWidget::showSolution ( const Dune::Ash::Texture *solutionTexture )
{
  if( solutionTexture != solutionTexture_ )
  {
    const Dune::FieldVector< GLfloat, 3 > lowerLeft{ 0.0f, 1.0f, 0.0f };
    const Dune::FieldVector< GLfloat, 3 > upperRight{ 1.0f, 0.0f, 0.0f };
    const Dune::FieldVector< GLfloat, 4 > color{ 0.0f, 0.0f, 0.0f, 1.0f };
    solutionDisplayList_.compile( Dune::Ash::BillBoard( lowerLeft, upperRight, *solutionTexture, color ) );
  }
  solutionTexture_ = solutionTexture;
}
