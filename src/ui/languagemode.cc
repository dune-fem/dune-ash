// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/widget/button.hh>

#include "languagemode.hh"

using Dune::Ash::Rectangle;
using Dune::Ash::ButtonWidget;

static std::vector< bool > drawLine ()
{
  std::vector< bool > drawLine;
  drawLine.push_back( false );
  drawLine.push_back( false );
  drawLine.push_back( false );
  drawLine.push_back( false );
  return drawLine;
}

static std::vector< int > splitting ()
{
  std::vector< int > splitting;
  splitting.push_back( 1 );
  splitting.push_back( 2 );
  splitting.push_back( 6 );
  splitting.push_back( 1 );
  
  return splitting;
}

struct LanguageMode::EventClose
: public Dune::Ash::Event
{
  explicit EventClose ( FrontendModeStack &modeStack, std::size_t lang )
  : modeStack_( modeStack ),
    lang_( lang )
  {}

  void executeEvent () 
  {
    modeStack_.setLanguage( lang_ );
    modeStack_.pop(); 
  }

private:
  FrontendModeStack &modeStack_;
  const std::size_t lang_;
};


// Implementation of LanguageMode
// --------------------------
LanguageMode::LanguageMode( FrontendModeStack &modeStack )
: FrontendMode( modeStack ),
  father_( nullptr ),
  shrinkWidget_( 12.0f / 32.0f, 5.0f / 18.0f ),
  text_( 120.0f, { 6.0f, 0.5f } ),
  split_( splitting(), drawLine() ),
  languageArray_( 4, 1, { 1.0f, 1.0f, 1.0f }, { 0.08f, 0.2f } ),
  placeholder_( Rectangle( { 1.0f, 1.0f, 1.0f, 1.0f } ) ),
  parser_( "panel" )
{
  setLanguage();

  shrinkWidget_.setChild( &split_ );

  split_.setChild( 0, &placeholder_ );
  split_.setChild( 1, &text_ );
  split_.setChild( 2, &languageArray_ );
  split_.setChild( 3, &placeholder_ );

  const std::size_t offset = modeStack.languages() > 2 ? 0u : 1u;
  for( std::size_t i = 0; i< modeStack.languages(); ++i )
    languageArray_.setChild( i + offset, 0, new ButtonWidget( new EventClose( modeStack, i ), modeStack.flag( i ) ) );
}

LanguageMode::~LanguageMode ()
{
  for( std::size_t v = 0; v < languageArray_.size()[ 1 ]; ++v )
  {
     for( std::size_t h = 0; h < languageArray_.size()[ 0 ]; ++h )
    {
      Widget *widget = languageArray_.setChild( h, v, nullptr );

      if( !widget )
        continue;

      Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( widget );
      if( button )
        delete button->setEvent( nullptr );

      delete widget;
    }
  }
}

void LanguageMode::setLanguage ()
{
  parser_( modeStack().getLanguagePath(), textMap_ );
  std::istringstream input( textMap_[ "macros"] + textMap_[ "language" ] );
  text_.setText( modeStack().getLanguagePath(), input );
}

void LanguageMode::initializeMode ()
{
  father_ = modeStack().father( this );
}

void LanguageMode::draw ()
{
  if( father_ )
    father_->draw();
  shrinkWidget_.draw();
}

void LanguageMode::tick ( Ticks ticks )
{
  if( father_ )
    father_->tick( ticks );
  shrinkWidget_.tick( ticks );
}
