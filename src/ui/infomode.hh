#ifndef INFOMODE_HH
#define INFOMODE_HH

#include <list>
#include <string>

#include <dune/ash/widget/button.hh>
#include <dune/ash/widget/collection.hh>
#include <dune/ash/widget/floatingtext.hh>
#include <dune/ash/widget/shrink.hh>

#include "frontendmode.hh"



// InfoMode
// --------

class InfoMode
: public FrontendMode
{
  typedef InfoMode ThisType;
  typedef FrontendMode BaseType;

  typedef FrontendModeStack::InfoMap InfoMap;
  typedef InfoMap::const_iterator InfoIterator;

  struct EventClose;
  struct EventBack;
  struct EventLink;

public:
  InfoMode ( FrontendModeStack &modeStack, const std::string &panel = std::string( "maininfo" ) ); // alternative way to get an default parameter from the file might be better
  ~InfoMode ();

  void initializeMode ();

  void draw ();

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { shrinkWidget_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks );

  void setLanguage ();

  void setPanel( const std::string &panel );

private:
  InfoMode ( const InfoMode& );
  InfoMode& operator= ( const InfoMode& );

  const InfoMap &infoMap () const { return modeStack().infoMap(); }

  FrontendMode *father_;
  Dune::Ash::ShrinkWidget<> shrinkWidget_;
  Dune::Ash::CollectionWidget collection_;
  Dune::Ash::ButtonWidget closeButton_, backButton_;
  Dune::Ash::FloatingTextWidget floatingText_;
  std::string curPanel_;
  std::list< std::string > history_;
};

#endif // #ifndef INFOMODE_HH
