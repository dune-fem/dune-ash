// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/image.hh>
#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/stringvector.hh>

#include "../common/filemanager.hh"
#include "fileparser.hh"
#include "frontendmode.hh"


// Implementation of FrontendModeStack
// -----------------------------------

FrontendModeStack::FrontendModeStack ( SharedInformation *sharedInformation )
  : topWidget_( *this ),
    sharedInformation_( sharedInformation ),
    exitStatus_( StatusError )
{
  const std::vector< std::string > &paths = Dune::Fem::Parameter::getValue< Dune::Ash::StringVector >( "ash.languagepaths" );

  for( const auto& lang : Dune::Fem::Parameter::getValue< Dune::Ash::StringVector >( "ui.languages", { "en", "de" } ) )
  {
    const auto pos = std::find_if( paths.begin(), paths.end(),
        [ &lang ] ( const std::string &p ) { return Dune::Fem::fileExists( p + "/" + lang + "/flag.png" ); } );
    if( pos == paths.end() )
      DUNE_THROW( Dune::IOError, "Language '" + lang + "' not found." );
    languagePaths_.push_back( *pos + "/" + lang );
  }

  setLanguage( 0 );

  Dune::Ash::loadImage( Dune::Fem::Parameter::getValue< std::string >( "ui.map" ), mapTexture_ );
  Dune::Ash::loadImage( Ash::FileManager::absoluteImageName( "equation.png" ), equationTexture_ );
  Dune::Ash::loadImage( Ash::FileManager::absoluteTextureName( "icons.png" ), iconsTexture_ );

  auto functor = [ this ] ()
  {
    Dune::Ash::Texture flagTexture;
    for( std::size_t i = 0; i < languagePaths_.size(); ++i )
    {
      std::string filename = languagePaths_[ i ] + "/flag.png";
      const std::array< GLsizei, 2 > size = Dune::Ash::loadImage( filename, flagTexture );

      typedef Dune::FieldVector< GLfloat, 3 > Position;
      const GLfloat factor = 0.125f / static_cast< GLfloat >( std::max( size[ 0 ], size[ 1 ] ) );
      const Position extent = Position{ factor * static_cast< GLfloat >( size[ 0 ] ), factor * static_cast< GLfloat >( size[ 1 ] ), 0.0f };
      const Position center = { GLfloat( i % 4 )*0.25f + 0.125f, GLfloat( i / 4 )*0.25f + 0.125f, 0.0f };
      Dune::Ash::BillBoard( center - extent, center + extent, flagTexture )();
    }
  };

  flagsTexture_.load( GL_RGBA, {{ 256, 256 }}, 0, functor );
}


void FrontendModeStack::clear ()
{
  typedef StackType::iterator ModeIterator;
  for( ModeIterator it = stack_.begin(); it != stack_.end(); ++it )
    garbage_.push( *it );
  stack_.clear();
}


void FrontendModeStack::setLanguage ( std::size_t language )
{
  currentLanguage_ = language;

  FileParser parser( "info" );
  parser( getLanguagePath(), infoMap_ );

  typedef StackType::iterator ModeIterator;
  for( ModeIterator it = stack_.begin(); it != stack_.end(); ++it )
    (*it)->setLanguage();
}


FrontendMode *FrontendModeStack::father ( FrontendMode *mode )
{
  typedef StackType::const_reverse_iterator ModeIterator;
  for( ModeIterator it = stack_.rbegin(); it != stack_.rend(); ++it )
  {
    if( *it == mode )
      return (++it == stack_.rend() ? nullptr : *it);
  }
  return nullptr;
}


void FrontendModeStack::collectGarbage ()
{
  while( !garbage_.empty() )
  {
    delete garbage_.top();
    garbage_.pop();
  }
}



// Implementation of FrontendModeStack::TopWidget
// ----------------------------------------------

void FrontendModeStack::TopWidget::draw ()
{
  modeStack_.top().draw();
}


void FrontendModeStack::TopWidget::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
{
  modeStack_.top().mouseMove( x, dx, ticks );
}


void FrontendModeStack::TopWidget::mouseButtonDown ( const Position &x, Ticks ticks )
{
  modeStack_.top().mouseButtonDown( x, ticks );
}


void FrontendModeStack::TopWidget::mouseButtonUp ( const Position &x, Ticks ticks )
{
  modeStack_.top().mouseButtonUp( x, ticks );
}


void FrontendModeStack::TopWidget::tick ( Ticks ticks )
{
  modeStack_.collectGarbage();
  modeStack_.top().tick( ticks );
}
