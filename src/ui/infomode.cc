// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <sstream>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/widget/button.hh>


#include "infomode.hh"

using Dune::Ash::Rectangle;
using Dune::Ash::ButtonWidget;


// Event Implementations
// ---------------------

struct InfoMode::EventClose
: public Dune::Ash::Event
{
  explicit EventClose ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent () { modeStack_.pop(); }

private:
  FrontendModeStack &modeStack_;   
};


struct InfoMode::EventBack
: public Dune::Ash::Event
{
  explicit EventBack ( InfoMode &mode )
  : mode_( mode )
  {}

  void executeEvent () 
  {
    mode_.history_.pop_back();
    assert( !mode_.history_.empty() );
    mode_.setPanel( mode_.history_.back() );
  }

private:
  InfoMode &mode_;   
};


struct InfoMode::EventLink
: public Dune::Ash::Event
{
  EventLink ( InfoMode &mode, const std::string &panel )
  : mode_( mode ),
    panel_( panel )
  {}

  void executeEvent () 
  {
    mode_.history_.push_back( panel_ );
    mode_.setPanel( panel_ );
  }

private:
  InfoMode &mode_;
  std::string panel_;
};



// Implementation of InfoMode
// --------------------------

// recalculate the right sizes for everything

InfoMode::InfoMode ( FrontendModeStack &modeStack, const std::string &panel )
: FrontendMode( modeStack ),
  father_( nullptr ),
  shrinkWidget_( 12.0f / 16.0f, 7.0f / 9.0f ),
  closeButton_( new EventClose( modeStack ), modeStack.icon( 1 ) ),
  backButton_( new EventBack( *this ), modeStack.icon3x2( 5 ) ),
  floatingText_( 120.0f, { 12.0f, 7.0f } ),
  curPanel_( panel )
{
  history_.push_back( panel );

  shrinkWidget_.setChild( &collection_ );
  collection_.addChild( { 0.0, 0.0 }, { 1.0, 1.0 }, &floatingText_ );
  collection_.addChild( { 139.0 / 144.0, 1.0 / 84.0 }, { 1.0 / 36.0, 1.0 / 21.0 }, &closeButton_ );

  setLanguage();
}


InfoMode::~InfoMode ()
{
  floatingText_.deleteEvents();

  delete closeButton_.setEvent( nullptr );
  delete backButton_.setEvent( nullptr );
}


void InfoMode::initializeMode ()
{
  father_ = modeStack().father( this );
}


void InfoMode::draw ()
{
  if( father_ )
    father_->draw();
  shrinkWidget_.draw();
}


void InfoMode::tick ( Ticks ticks )
{
  if( father_ )
    father_->tick( ticks );
  shrinkWidget_.tick( ticks );
}


void InfoMode::setPanel( const std::string &panel )
{
  InfoIterator pos = infoMap().find( panel );
  if( pos != infoMap().end() )
  {
    curPanel_ = pos->first;
    std::istringstream input( pos->second );
    floatingText_.setText( modeStack().getLanguagePath(), input );

    collection_.removeChild( &backButton_ );
    if( history_.size() > 1 )
      collection_.addChild( { 134.0 / 144.0, 1.0 / 84.0 }, { 1.0 / 36.0, 1.0 / 21.0 }, &backButton_ );
  }
  else
    std::cerr << "Error: Trying to display non-existing info entry '" << panel << "'." << std::endl;
}


void InfoMode::setLanguage()
{
  if( infoMap().find( curPanel_ ) == infoMap().end() )
    curPanel_ = infoMap().begin()->first;
  setPanel( curPanel_ );

  floatingText_.deleteEvents();
  for( InfoIterator it = infoMap().begin(); it != infoMap().end(); ++it )
    floatingText_.setEvent( std::string( "link:" ) + it->first, new EventLink( *this, it->first ) );
}
