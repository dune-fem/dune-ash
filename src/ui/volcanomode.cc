// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <dune/ash/widget/button.hh>

#include "inputmode.hh"
#include "volcanomode.hh"

// VolcanoMode::EventNextMode
// --------------------------

struct VolcanoMode::EventNextMode
: public Dune::Ash::Event
{
  explicit EventNextMode ( VolcanoMode &mode )
  : mode_( mode )
  {}

  void executeEvent ()
  {
    mode_.sharedInformation_->volcanoPosition = mode_.volcanoWidget_.volcanoPosition();
//    mode_.modeStack().pop();
    mode_.modeStack().push( new InputMode( mode_.modeStack() ) );
  }

private:
  VolcanoMode &mode_;
};



// Implementation of VolcanoMode
// -----------------------------

VolcanoMode::VolcanoMode ( FrontendModeStack &modeStack )
: FrontendMode( modeStack ),
  sharedInformation_( modeStack.sharedInformation() ),
  splitWidget_( 9, 7 ),
  volcanoWidget_( modeStack.mapTexture(), modeStack.icon( 0 ) ),
  panel_( modeStack, Panel::IdVolcanoMode )
{
  splitWidget_.setLeftChild( &volcanoWidget_ );
  splitWidget_.setRightChild( &panel_ );

  volcanoWidget_.setVolcanoPosition( sharedInformation_->volcanoPosition );

  panel_.setButton( 6, new Dune::Ash::ButtonWidget( new EventNextMode( *this ), modeStack.icon3x2( 6 ) ) );
}


void VolcanoMode::cleanUp ()
{
  sharedInformation_->volcanoPosition = { 0.16, 0.12 };
}


void VolcanoMode::setLanguage ()
{ 
  panel_.setLanguage();
}
