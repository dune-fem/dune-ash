// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <fstream>
#include <string>
#include <iostream>

#include <dune/ash/image.hh>

#include "../common/filemanager.hh"
#include "exitmode.hh"
#include "infomode.hh"
#include "languagemode.hh"
#include "titlewidget.hh"



// TitleWidget::EventExit
// ----------------------

struct TitleWidget::EventExit
: public Dune::Ash::Event
{
  explicit EventExit ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent ()
  {
    if( Dune::Fem::Parameter::getValue< bool >( "ui.askonexit", false ) )
    {
      modeStack_.push( new ExitMode( modeStack_ ) );
    }
    else
    {
      modeStack_.setExitStatus( FrontendModeStack::StatusRestart );
      modeStack_.clear(); 
    }
  }

private:
  FrontendModeStack &modeStack_;   
};



// TitleWidget::EventLanguage
// --------------------------

struct TitleWidget::EventLanguage
: public Dune::Ash::Event
{
  explicit EventLanguage ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent () { modeStack_.push( new LanguageMode( modeStack_ ) ); }

private:
  FrontendModeStack &modeStack_;   
};



// TitleWidget::EventInfo
// ----------------------

struct TitleWidget::EventInfo
: public Dune::Ash::Event
{
  explicit EventInfo ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent () { modeStack_.push( new InfoMode( modeStack_ ) ); }

private:
  FrontendModeStack &modeStack_;   
};



// Implementation of TitleWidget
// -----------------------------

TitleWidget::TitleWidget ( FrontendModeStack &modeStack ) 
: split_( 15, 6 ),
  buttonArray_( 3, 1, Dune::Ash::GridWidget::Color( 1 ), { 0.05f, 0.1f } ),
  controlArray_( 0.6666, 0.6666, 0.0f ),
  buttonInfo_( new EventInfo( modeStack ), modeStack.icon( 7 ) ),
  buttonLanguage_( new EventLanguage( modeStack ), modeStack.icon( 10 ) ),
  buttonExit_( new EventExit( modeStack ), modeStack.icon( 1 ) )
{
  Dune::Ash::loadImage( Ash::FileManager::absoluteImageName( "dune_ash.png" ), titleTexture_ );
  title_ = Dune::Ash::BillBoard( { 0.05f, 0.05f + 0.9f *  1.0f / 32.0f, 0.0f  }, { 0.95f, 0.05f + 0.9f * 31.0f / 32.0f, 0.0f }, titleTexture_ ); 

  split_.setLeftChild( &title_ );
  split_.setRightChild( &buttonArray_ );

  controlArray_.setChild( &buttonExit_ );

  if( Dune::Fem::Parameter::getValue< bool >( "ui.allowquit", true ) )
  {
    buttonArray_.setChild( 0, 0, &buttonLanguage_ );
    buttonArray_.setChild( 1, 0, &buttonInfo_ );
    buttonArray_.setChild( 2, 0, &controlArray_ );
  }
  else
  {
    buttonArray_.setChild( 1, 0, &buttonLanguage_ );
    buttonArray_.setChild( 2, 0, &buttonInfo_ );
  }
}


TitleWidget::~TitleWidget ()
{
  delete buttonExit_.setEvent( nullptr );
  delete buttonLanguage_.setEvent( nullptr );
  delete buttonInfo_.setEvent( nullptr );
}
