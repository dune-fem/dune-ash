// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef VOLCANOMODE_HH
#define VOLCANOMODE_HH

#include <dune/ash/widget/split.hh>

#include "frontendmode.hh"
#include "panel.hh"
#include "volcanowidget.hh"
#include "sharedinfo.hh"


// VolcanoMode
// -----------

class VolcanoMode
: public FrontendMode
{
  typedef VolcanoMode ThisType;
  typedef FrontendMode BaseType;

  struct EventNextMode;

public:
  VolcanoMode ( FrontendModeStack &modeStack );

  void cleanUp ();

  void draw () { splitWidget_.draw(); }

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { splitWidget_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks ) { splitWidget_.tick( ticks ); }

  void setLanguage ();
  
private:
  SharedInformation *sharedInformation_;
  Dune::Ash::SplitWidget< Dune::Ash::Horizontal > splitWidget_;
  VolcanoWidget volcanoWidget_;
  Panel panel_;
};

#endif // VOLCANOMODE_HH
