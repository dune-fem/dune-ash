WARNING: This file is outdated.

INFO
  This project was developed in June/July 2011 for the ``Wissenschaftsmarkt''
  (Science Market) in Freiburg. 
  - more information about the background of the program and the equations are
    to be found in the documentation pdf file created by
    ../../doc/latex/dune-ash-howto.tex

    outline: what does the program do
    - first a surface is loaded where arrows can be drawn by the user and the
      diffusion parameter is determined; this happens on the local server; the
      resulting discrete data is written to some binary output file
    - secondly the shell script callInterpolation.sh calls the velocityfieldfacility
      executable where the velocity field is computed (input: binary file
      which will be read); this again is written in a binary file
    - thirdly the shell script callBackend.sh makes the advdiffequationsolver
      read from that data and use that information as input for the actual
      advection-diffusion-equation. The results are written again
    - finally these results are read by the userinterface and visualized

1. INSTALLATION AND CONFIGURATION
    - download the module dune-ash from the server:
      svn checkout https://dune.mathematik.uni-freiburg.de/svn/dune-ash/trunk dune-ash
      (make sure that the needed modules \dune-common, dune-grid, dune-istl,
      dune-fem are existent, uptodate and configured)
    - and configure:
      ./dune-common/bin/dunecontrol --opts=config.opts --only dune-ash all

2. HOW TO EDIT THE SERVER AND NUMBER OF PROCESSES
    - in the directory src/userinterface there's a file server.config.template .
      Insert the wanted information (instead of the "<>"- templates) and save
      the file to server.config . By default, the own server is used.
    - remark: you can use your own server as well as remote servers. You will
      have to check at any rate whether all executables exist and if the program
      finds the  server and the paths. -->
    - do this with ./check.sh
    (- if there are problems with these connections that can't be solved,
      please contact: theresa.strauch@mathematik.uni-freiburg.de )

3. CALL OF EXECUTABLE
    START NEW SESSION:
    - if everything has worked alright until now, you can call the program
      with ./userinterface
    LOAD EXISTING SESSION
    - optionally you can pass a former session number as an argument:
      ./userinterface <number> , then the velocity field calculated in that
      session will be loaded and visualized (if existent)

4. STRUCTURE OF THE PROGRAM
    - the userinterface which contains the executable is the caller for everything
      needed elsewhere
    - the "velocityfieldfacility" is responsible for creating the velocity
      field. In order to do that the heat equation is applied to smoothen out
      the discrete information to a continuous one which will be usable as an
      input for the advdiffequationsolver
    - the "advdiffequationsolver" contains the actual solver for the
      advection-diffusion-PDE and uses a formerly implemented Discontinuous
      Galerkin method (polynomial degree can be edited in
      userinterface/Makefile.am under POLORDER= ); here parallel computing will
      shorten computation times; this happens on the remote server if any is
      used
    - reading and writing functions are to be found in
      dune/fem/serialouputmap.hh; data resulting of parallel processes can
      be read into one global and thus makes possible to switch between
      parallel and serial data

5. OUTPUT
    - session data is to be found in the folder "sessions"
    - of the velocity field in the repository session<number> under
      session<number>.xdr(_<process number>), where the part before the brackets
      indicates the masterfile which gives the names of the files containing
      the actual information, one for each process
    - of the finished computation in the same directory in the subdir
      advdiffequationsolver-session/data

