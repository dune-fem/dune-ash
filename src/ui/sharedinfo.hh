// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef SHAREDINFO_HH
#define SHAREDINFO_HH

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>

#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/spgrid/grid.hh>
#include <dune/grid/io/file/dgfparser/gridptr.hh>
#include <dune/grid/common/gridinfo.hh>

#include <dune/grid/spgrid.hh>

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/gridpart/levelgridpart.hh>
#include <dune/fem/space/finitevolume.hh>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/visualizer/vectorfield.hh>

#include "../common/filemanager.hh"
#include "../common/discretizationtraits.hh"
#include "../windfield/includes.hh"


class SharedInformation
{
public:
  typedef DiscretizationTraits::GridType GridType;
  typedef DiscretizationTraits::GridPartType GridPartType;

  static const int dimension = GridPartType::dimension;

  typedef DiscretizationTraits::SolutionVisualizerType SolutionVisualizerType;
  typedef std::vector< SolutionVisualizerType * > AshSampleVectorType;
  typedef std::vector< Dune::Ash::Texture * > AshTextureVectorType;

  typedef Dune::SPGrid< typename GridPartType::ctype, dimension > WindFieldGridType;
  typedef Dune::Fem::LevelGridPart< WindFieldGridType > WindFieldGridPartType;
  typedef typename DiscretizationTraits::WindFieldSpaceType::FunctionSpaceType WindFieldFunctionSpaceType;
  typedef Dune::Fem::FiniteVolumeSpace< WindFieldFunctionSpaceType, WindFieldGridPartType, 0 > WindFieldSpaceType;
  typedef Dune::Fem::AdaptiveDiscreteFunction< WindFieldSpaceType > WindFieldType;
  typedef Dune::Ash::VectorFieldVisualizer< WindFieldType > WindFieldVisualizerType;

  struct WindFieldLevelContext
  {
    WindFieldLevelContext ()   
    {}

    WindFieldLevelContext ( GridType &grid, int level )
    : gridPart( new GridPartType( grid, level ) ),
      dfSpace( new WindFieldSpaceType( *gridPart ) ),
      dFunction( new WindFieldType( name( level ), *dfSpace ) )
    {}

    static std::string name ( int level )
    {
      std::ostringstream s;
      s << "level " << level << " wind field";
      return s.str();
    }

    Dune::shared_ptr< GridPartType > gridPart;
    Dune::shared_ptr< WindFieldSpaceType > dfSpace;
    Dune::shared_ptr< WindFieldType > dFunction;
    Dune::shared_ptr< WindFieldVisualizerType > visualizer;
  };

  typedef std::vector< WindFieldLevelContext > WindFieldVectorType;

  // public memeber
  std::string session;
  Dune::FieldVector< double, 2 > volcanoPosition;
  double diffusion;
  bool xdr_vector_field_valid_;

  Dune::GridPtr< GridType > gridPtr_;
  GridPartType *gridPart_;

  WindFieldGridType windFieldGrid_;
  WindFieldVectorType *windFieldVector_;

  AshSampleVectorType *ashSampleVector_;
  AshTextureVectorType ashTextures_;

  Dune::Ash::DisplayList *arrowList_;

  explicit SharedInformation ();

  ~SharedInformation();

  std::size_t nextWindFieldDisplayLevel();
  std::size_t windFieldDisplayLevel() const { return windFieldDisplayLevel_; }
  void incrementWindFieldDisplayLevel() { windFieldDisplayLevel_ = nextWindFieldDisplayLevel(); }

  std::size_t nextMeshDisplayLevel();
  std::size_t meshDisplayLevel() const { return meshDisplayLevel_; }
  void incrementMeshDisplayLevel() { meshDisplayLevel_ = nextMeshDisplayLevel(); }


  // write intermediat parameters 
  void writeParameters ( std::ostream &stream ) const
  {
    Dune::FieldVector< double, 2 > vp;
    vp[ 0 ] = volcanoPosition[ 0 ];
    vp[ 1 ] = 1 - volcanoPosition[ 1 ];
    stream << "ash.volcano.position: " << vp << std::endl;
    stream << "ash.diffusion: " << diffusion << std::endl;
  }

  // initialing methods
  // ------------------

  // init arrowList
  void initializeArrowList ();

  // init Grid
  void initializeGrid ();

  // init windfield
  void initializeWindField (); 
  
  // init arrowList
  void initializeAshSamples ();

  // load AshSample from File
  void loadAshSampleFromFile ( SolutionVisualizerType *visualizer );

  void screenShot ( std::size_t frame ) const;
 
  // reset all samples
  void resetSamples ();
  
  // reset computed quantities
  void reset();

private:
  double maxConcentration_;

  std::size_t windFieldDisplayLevel_;
  std::size_t meshDisplayLevel_;
};

#endif // #ifndef SHAREDINFO_HH
