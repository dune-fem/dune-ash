// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <sstream>

#include <dune/fem/io/io.hh>

#include "infomode.hh"
#include "panel.hh"
#include "sharedinfo.hh"


static std::vector< bool > drawLine ( int extraHeight )
{
  std::vector< bool > drawLine;
  drawLine.push_back( false );
  drawLine.push_back( true );
  drawLine.push_back( extraHeight > 0 );
  drawLine.push_back( true );
  drawLine.push_back( false );
  return drawLine;
}


static std::vector< int > splitting ( int extraHeight )
{
  std::vector< int > splitting;
  splitting.push_back( 2 );                 // titleWidget
  splitting.push_back( 23 - extraHeight );  // floatingText
  splitting.push_back( extraHeight );       // ExtraContent 
  splitting.push_back( 2 );                 // buttonArray
  
  return splitting;
}

static const std::vector< std::string > modeName()
{
  std::vector< std::string > name;
  name.push_back( "main" );
  name.push_back( "volcano" );
  name.push_back( "input" );
  name.push_back( "diffusion" );
  name.push_back( "visual" );

  return name;
}


// Panel::EventRestart
// -------------------

struct Panel::EventRestart
: public Dune::Ash::Event
{
  explicit EventRestart ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  virtual void executeEvent () 
  {
    modeStack_.popAll();
  }

private:
  FrontendModeStack &modeStack_;
};



// Panel::EventBack
// ----------------

struct  Panel::EventBack
: public Dune::Ash::Event
{
  explicit EventBack ( FrontendModeStack &modeStack )
  : modeStack_( modeStack )
  {}

  void executeEvent () { modeStack_.pop(); }

private:
  FrontendModeStack &modeStack_;
};



// Panel::EventLink
// ----------------

struct Panel::EventLink
: public Dune::Ash::Event
{
  EventLink ( FrontendModeStack &modeStack, const std::string &panel )
  : modeStack_( modeStack ),
    panel_( panel )
  {}

  void executeEvent () 
  {
    modeStack_.push( new InfoMode( modeStack_, panel_ ) );
  }

private:
  FrontendModeStack &modeStack_;
  std::string panel_;
};



// Implementation of  Panel
// ------------------------

Panel::Panel ( FrontendModeStack &modeStack, ModeId mode, int extraHeight )
: modeStack_( modeStack ),
  split_( splitting( extraHeight ), drawLine( extraHeight ), 0.01 ),
  bottomButtonArray_( 7, 1, Dune::Ash::GridWidget::Color( 1 ), { 0.025, 0.1 } ),
  text_( 120.0f, { 7.0f, float( 23 - extraHeight )/ 3.0f } ),
  title_( modeStack ),
  parser_( "panel" ),
  modeId_( mode )
{
  setLanguage();

  split_.setChild( 0, &title_ );
  split_.setChild( 1, &text_ );
  split_.setChild( 3, &bottomButtonArray_ );

  if( !modeStack.empty() )
  {
    setButton( 0, new Dune::Ash::ButtonWidget( new EventRestart( modeStack ), modeStack.icon3x2( 15 ) ) );
    setButton( 1, new Dune::Ash::ButtonWidget( new EventBack( modeStack ), modeStack.icon3x2( 5 ) ) );
  }
}


Panel::~Panel ()
{
  text_.deleteEvents();

  for( std::size_t v = 0; v < bottomButtonArray_.size()[ 1 ]; ++v )
  {
    for( std::size_t h = 0; h < bottomButtonArray_.size()[ 0 ]; ++h )
    {
      Widget *widget = bottomButtonArray_.setChild( h, v, nullptr );

      if( !widget )
        continue;

      Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( widget );
      if( button )
        delete button->setEvent( nullptr );

      delete widget;
    }
  }
}


void Panel::tick ( Ticks ticks )
{
  split_.tick( ticks );
}


Dune::Ash::Widget* Panel::setButton ( int pos, Dune::Ash::Widget *button )
{
  assert( pos < 7 );
  return (pos >= 0 ? bottomButtonArray_.setChild( pos, 0, button ) : nullptr);
}


Dune::Ash::Widget* Panel::getButton ( int pos )
{
  assert( pos < 7 );
  return (pos >= 0 ? bottomButtonArray_.getChild( pos, 0 ) : nullptr);
}


void Panel::setExtraContent ( Dune::Ash::Widget* extraContent )
{
  split_.setChild( 2, extraContent );
}


void Panel::setLanguage ()
{
  parser_( modeStack_.getLanguagePath(), textMap_ );
  std::istringstream input( textMap_[ modeName()[ modeId_ ] ] );
  text_.setText( modeStack_.getLanguagePath(), input );

  text_.deleteEvents();
  for( InfoIterator it = infoMap().begin(); it != infoMap().end(); ++it )
    text_.setEvent( std::string( "link:" ) + it->first, new EventLink( modeStack_, it->first ) );
}


Dune::Ash::Event *Panel::setTextEvent ( const std::string &id, Dune::Ash::Event *event ) 
{
  return text_.setEvent( id, event ); 
}
