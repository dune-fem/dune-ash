// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef INPUTWIDGET_HH
#define INPUTWIDGET_HH

#include <list>
#include <utility>

#include <dune/ash/widget/grid.hh>

#include "volcanowidget.hh"


// InputWidget
// -----------

class InputWidget
: public VolcanoDisplayWidget
{
  typedef InputWidget ThisType;
  typedef VolcanoDisplayWidget BaseType;

public:
  typedef std::list< std::pair< Position, Position > > MousePath;

  InputWidget ( const Dune::Ash::Texture &mapTexture,
                const Dune::Ash::BillBoard &iconVolcano );

  void undo () { if( !mousePaths_.empty() ) mousePaths_.pop_back(); }

  void clearMousePaths () { mousePaths_.clear(); }
  void getMousePaths ( std::list< MousePath > &mousePaths ) const { mousePaths = mousePaths_; }
  void setMousePaths ( const std::list< MousePath > &mousePaths ) { mousePaths_ = mousePaths; }

  void draw ();

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

  void mouseButtonDown ( const Position &x, Ticks ticks );
  void mouseButtonUp ( const Position &x, Ticks ticks );

private:
  static bool timeForTriangle ( const Position &p, const Position &d, const Position &ltp );
  static void drawTriangle ( const Position &p, const Position &d );

  void drawMousePath ( const MousePath &path ) const;

  bool mouseButtonDown_;
  Ticks eventTimeOld_;
  Position lastPos_;
  std::list< MousePath > mousePaths_;
  MousePath currentMousePath_;
  const double length_scale_;
};

#endif // #ifndef INPUTWIDGET_HH
