// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef VOLCANOWIDGET_HH
#define VOLCANOWIDGET_HH

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/opengl/texture.hh>
#include <dune/ash/widget/display.hh>
#include <dune/ash/widget/grid.hh>


// VolcanoDisplayWidget
// --------------------

class VolcanoDisplayWidget
: public Dune::Ash::DisplayWidget
{
  typedef VolcanoDisplayWidget ThisType;
  typedef Dune::Ash::DisplayWidget BaseType;

public:
  explicit VolcanoDisplayWidget ( const Dune::Ash::Texture &mapTexture,
                                  const Dune::Ash::BillBoard &iconVolcano );

  void draw ();

  void setVolcanoPosition ( const Position &position ) { posVolcano_ = position; }
  const Position &volcanoPosition () const { return posVolcano_; }

private:
  Dune::Ash::DisplayList iconVolcano_;
  Position posVolcano_;
};



// VolcanoWidget
// -------------

class VolcanoWidget
: public VolcanoDisplayWidget
{
  typedef VolcanoWidget ThisType;
  typedef VolcanoDisplayWidget BaseType;

public:
  explicit VolcanoWidget ( const Dune::Ash::Texture &mapTexture,
                           const Dune::Ash::BillBoard &iconVolcano );

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks );

  void mouseButtonDown ( const Position &x, Ticks ticks );
  void mouseButtonUp ( const Position &x, Ticks ticks );

private:
  bool mouseButtonDown_;
};

#endif // #ifndef VOLCANOWIDGET_HH
