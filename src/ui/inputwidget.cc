// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <string>
#include <iostream>

#include <dune/ash/io.hh>

#include "../common/filemanager.hh"
#include "inputwidget.hh"


// Implementation of InputWidget
// -----------------------------

InputWidget::InputWidget ( const Dune::Ash::Texture &mapTexture,
                           const Dune::Ash::BillBoard &iconVolcano )
: BaseType( mapTexture, iconVolcano ),
  mouseButtonDown_( false ),
  lastPos_( 0 ),
  length_scale_( 1e7 )
{}


bool InputWidget::timeForTriangle ( const Position &p, const Position &d, const Position &ltp )
{
  const double distance  = (p - ltp).two_norm();
  const int steps = 5;
  const double max_speed = 40.0;
  
  const double current_speed = d.two_norm();
  return (current_speed >= max_speed) || (distance > 0.2 / std::ceil( current_speed * steps / max_speed ));
}


void InputWidget::drawTriangle ( const Position &p, const Position &d )
{
  const double alpha = 0.02 / d.two_norm();
  glBegin( GL_TRIANGLES );
  glColor3b(0, 0, 0);
  glVertex2f( p[ 0 ] + alpha*d[ 0 ], (p[ 1 ] + alpha*d[ 1 ]) );
  glVertex2f( p[ 0 ] + 0.5*alpha*d[ 1 ], (p[ 1 ] - 0.5*alpha*d[ 0 ]) );
  glVertex2f( p[ 0 ] - 0.5*alpha*d[ 1 ], (p[ 1 ] + 0.5*alpha*d[ 0 ]) );
  glEnd();
}


void InputWidget::drawMousePath ( const MousePath &path ) const
{
  if( path.empty() )
    return;

  glLineWidth( 3.0 );

  glBegin( GL_LINE_STRIP );
  glColor3b( 0, 0, 0 );
  for( MousePath::const_iterator it = path.begin(); it != path.end(); ++it )
    glVertex2f( (it->first)[ 0 ], (it->first)[ 1 ] );
  glEnd();

  MousePath::const_iterator it = path.begin();
  assert( it != path.end() );
  Position ltp = it->first;

  for( ; it != path.end(); ++it )
  {
    if( !timeForTriangle( it->first, it->second, ltp ) )
      continue;
    ltp = it->first;

    drawTriangle( it->first, it->second );
  }
  drawTriangle( path.back().first, path.back().second );
}


void InputWidget::draw ()
{
  BaseType::draw();

  glEnable( GL_POINT_SMOOTH );
  glEnable( GL_LINE_SMOOTH );

  // just temporarily, we redraw the users mouse paths...
  typedef std::list< MousePath >::iterator PathIterator;
  for( PathIterator it = mousePaths_.begin(); it != mousePaths_.end(); ++it )
    drawMousePath( *it );

  drawMousePath( currentMousePath_ );

  glDisable( GL_POINT_SMOOTH );
  glDisable( GL_LINE_SMOOTH );
}


void InputWidget::mouseButtonDown ( const Position &x, Ticks ticks )
{
  mouseButtonDown_ = true;

  // save time of initial position
  eventTimeOld_ = ticks;

  // buffer initial position of a path in order to compute direction
  lastPos_ = x;
}


void InputWidget::mouseButtonUp ( const Position &x, Ticks ticks )
{
  mouseButtonDown_ = false;

  mousePaths_.push_back( currentMousePath_ );
  currentMousePath_.clear();
}


void InputWidget::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
{
  // compute time difference between last position and current position (in microseconds)
  Ticks td = ticks - eventTimeOld_;

  // save for possible future use
  eventTimeOld_ = ticks;

  // Choose delta t for input ( less data to handle ... ).
  // deltaT is measured in microseconds....
  const Ticks deltaT = 1000;

  if( mouseButtonDown_ && (td > deltaT) )
  {
    // compute direction
    Position d( x - lastPos_ );
    d *= length_scale_ / double( td );

    currentMousePath_.push_back( std::make_pair( lastPos_, d ) );

    // buffer current position on path in order to compute direction
    lastPos_ = x;
  }
}
