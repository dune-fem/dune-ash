// vim: set expandtab ts=8 sw=2 sts=2:
#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <SDL.h>

#include <cassert>
#include <sys/time.h>

#include <dune/ash/cursor.hh>
#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/opengl/texture.hh>
#include <dune/ash/widget/shrink.hh>
#include <dune/ash/widget/widget.hh>

#include "frontendmode.hh"
#include "sharedinfo.hh"

/*
 * Helper class that frees resources on destruction
 */
struct SDLHelper
{
  SDLHelper ()
    : SDLInitialized_( false )
  {}

  bool initializeSDL ()
  {
    if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
    {
      char *errorMsg = SDL_GetError();
      std::cerr << "Error in SDL_Init: " << errorMsg << std::endl;
      SDL_Quit();

      SDLInitialized_ = false;
      return false;
    }

    SDLInitialized_ = true;
    return true;
  }

  ~SDLHelper ()
  {
    if ( SDLInitialized_ )
      SDL_Quit();
  }

private:
  bool SDLInitialized_;
};



/*
 * This models the main window. Window management and user input are - on a low level -
 * done here.
 */
class MainWindow
{
  typedef Dune::Ash::Widget::Ticks Ticks;
  typedef Dune::Ash::Widget::Position Position;

public:
  MainWindow ();
  ~MainWindow ();

  int width () const { return screen()->w; }
  int height () const { return screen()->h; }

  SDL_Surface *screen () const
  {
    assert( screen_ );
    return screen_;
  }

  Dune::Ash::Widget &activeWidget () const { return *activeWidget_; }
  void setActiveWidget ( Dune::Ash::Widget &widget ) { activeWidget_ = &widget; }

  // used to initialize the window. Returns true if initialization went well, false otherwise
  bool initializeWindow ();

  // this performs the main loop of the GUI program
  int run ();

  const FrontendModeStack &modeStack () const { return *modeStack_; }
  FrontendModeStack &modeStack () { return *modeStack_; }

private:
  // forbidden std methods
  MainWindow ( const MainWindow& );
  MainWindow& operator=( const MainWindow& );

  /*
   * private methods
   */
  Ticks ticksSinceEpoch () const
  {
    timeval tv;
    gettimeofday( &tv, 0 );
    return 1000000*static_cast< Ticks >( tv.tv_sec )
           + static_cast< Ticks >( tv.tv_usec );
  }

  // This method is called when the userinterface quits
  void actionOnQuit () const;

  // compiles several parts of the collage
  void compileVersionList ();

  /*
   * private data fields
   */
  Dune::Ash::MouseCursor *cursor_;
  FrontendModeStack *modeStack_;
  SDLHelper SDLHelper_;
  SDL_Surface *screen_;
  double fps_; // frames per second
  double timePerFrame_; // 1/fps, in microseconds
  SharedInformation sharedInformation_;

  // Note: The shrink widget is a quick hack until the mode stack is separated
  //       from the main window
  Dune::Ash::ShrinkWidget< Dune::Ash::Center > *shrinkWidget_;

  Dune::Ash::Widget *activeWidget_;
  Dune::Ash::Texture *versionTexture_;
  Dune::Ash::DisplayList *versionList_;
};

#endif // MAINWINDOW_HH
