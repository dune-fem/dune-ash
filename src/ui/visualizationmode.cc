// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>

#include <dune/common/timer.hh>

#include <dune/fem/serialoutputmap.hh>

#include <dune/ash/opengl/opengl.hh>
#include <dune/ash/parallel/mutex.hh>
#include <dune/ash/textures/arrow.hh>
#include <dune/ash/textures/gridfunctor.hh>

#include "../common/filemanager.hh"
#include "../common/discretizationtraits.hh"
#include "visualizationmode.hh"


// ButtonIcons
// -----------

typedef Dune::Ash::Arrow Arrow;
typedef Dune::Ash::GridFunctor< Arrow > ArrowGrid;
typedef Dune::Ash::DeformFunctor< ArrowGrid > ArrowIcon;
static ArrowIcon arrowIcon( std::size_t level ) {
  return ArrowIcon(
    ArrowGrid( Arrow(0.2), level, { 0.0f, 0.0f, 1.0f, 1.0f }, { 0.5f, 0.5f, 0.0f } ),
    { 1.0f / 6.0f, 0.0f, 0.0f },
    { 5.0f / 6.0f, 1.0f, 0.0f } );
}

typedef Dune::Ash::RectangleOutline Mesh;
typedef Dune::Ash::GridFunctor< Mesh > MeshGrid;
typedef Dune::Ash::DeformFunctor< MeshGrid > MeshIcon;
static MeshIcon meshIcon( std::size_t level ) {
  return MeshIcon(
    MeshGrid( Mesh( { 0.3f, 0.3f, 0.3f, 1.0f } ),
      level + 1, { 0.0f, 0.0f, 0.0f, 1.0f } ),
    { 1.0f / 6.0f, 0.0f, 0.0f },
    { 5.0f / 6.0f, 1.0f, 0.0f } );
}

// Helper Functions
// ----------------

static std::string ashSampleFileName ( int index )
{
  std::ostringstream stream;
  stream << "adr.solution_" << index;
  return stream.str();
}



// VisualizationMode::PullThread
// -----------------------------

struct VisualizationMode::PullThread
: public Dune::Ash::Thread
{
  typedef DiscretizationTraits::GridPartType GridPartType;
  typedef DiscretizationTraits::SolutionType SolutionType;
  typedef DiscretizationTraits::SolutionSpaceType SolutionSpaceType;
  typedef DiscretizationTraits::SolutionVisualizerType SolutionVisualizerType;

  PullThread ()
  : gridPart( nullptr ),
    delay( Dune::Fem::Parameter::getValue< std::size_t >( "ui.playback.polldelay", 1000 ) ),
    numAshSamplesFound( 0 )
  {
    cells[ 0 ] = cells[ 1 ] = Dune::Fem::Parameter::getValue< int >( "ui.concentration.textureresolution", 1080 );
  }

  ~PullThread ()
  {
    for( std::vector< SolutionVisualizerType * >::iterator it = ashFilesToAdd.begin(); it != ashFilesToAdd.end(); ++it )
      delete *it;
  }

protected:
  void main ();

public:
  void setSession ( const std::string &session ) 
  {
    command = Ash::FileManager::glueScript() + " sync_adr_solution \"" + session + "\"";
    dataPath = session + "/adrdata/";
    endToken = session + "/adr.done";
  }

  std::string dataPath;
  GridPartType *gridPart;
  std::vector< SolutionVisualizerType * > ashFilesToAdd;
  Dune::Ash::Mutex commandMutex, sampleMutex;
  bool done;

private:
  std::string command, endToken;
  std::size_t delay;
  std::size_t numAshSamplesFound;
  std::array<int,2> cells;
};



// Implementation of VisualizationMode::PullThread
// -----------------------------------------------

void VisualizationMode::PullThread::main ()
{
  SolutionSpaceType solutionSpace( *gridPart );
  std::size_t numAshSampleFiles = 0;

  // pull new ash samples forever, until this thread is canceled or until 'adr.done' is found
  for( done = false; !done; )
  {
    if( numAshSamplesFound < numAshSampleFiles )
    {
      const std::string fileName = ashSampleFileName( numAshSamplesFound );
      // create the discrete function
      SolutionType solution( "ash concentration", solutionSpace );
      Dune::Fem::SerialOutputMap< SolutionType > map( solution );
      std::cout << "Loading discrete function from file '" << fileName << ".xdr." << std::endl;
      map.read( dataPath, fileName );

      // create the visualizer for it

      SolutionVisualizerType *visualizer = new SolutionVisualizerType( solution, cells );

      // add visualizer to list
      sampleMutex.wait();
      ashFilesToAdd.push_back( visualizer );
      sampleMutex.release();

      ++numAshSamplesFound;
    }
    else
    {
      SDL_Delay( delay );

      commandMutex.wait();
      const int err = system( command.c_str() );
      commandMutex.release();
      if( err == -1 ) 
        std::cerr << "Command '" << command << "' returned an error." << std::endl;

      // check if 'adr.done' has been set
      const bool haveEndToken = Dune::Fem::fileExists( endToken );

      // Look up the newest ash sample
      numAshSampleFiles = 0;
      while( Dune::Fem::fileExists( dataPath + ashSampleFileName( numAshSampleFiles ) ) )
        ++numAshSampleFiles;

      // if not haveEndToken, don't trust the last sample to be fully written
      if( !haveEndToken && (numAshSampleFiles > 0) )
        --numAshSampleFiles;

      if( haveEndToken && (numAshSamplesFound == numAshSampleFiles) )
        done = true;
    }
  }
  gridPart = nullptr;
  std::cout << "server has finished." << std::endl;
}



// VisualizationMode::EventDisplayGrid
// -----------------------------------

struct VisualizationMode::EventDisplayGrid
: public Dune::Ash::Event
{
  explicit EventDisplayGrid ( VisualizationMode &mode )
  : mode_( mode )
  {
    setMesh();
  }

  virtual void executeEvent ()
  {
    mode_.sharedInformation_->incrementMeshDisplayLevel();
    setMesh();    
  }

private:
  void setMesh ()
  {
    const std::size_t currentLevel = mode_.sharedInformation_->meshDisplayLevel();

    if( currentLevel > 0 )
    {
      mode_.visualizationWidget_.showGridPart( mode_.sharedInformation_->gridPart_, 1.0 );
    }
    else
      mode_.visualizationWidget_.showGridPart( nullptr );

    Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( mode_.panel_.getButton( 3 ) );
    if( button )
      button->setIcon( meshIcon( mode_.sharedInformation_->nextMeshDisplayLevel() ) );    
  }

  VisualizationMode &mode_;
};



// VisualizationMode::EventWindFieldResolution
// -------------------------------------------

struct VisualizationMode::EventWindFieldResolution
: public Dune::Ash::Event
{
  explicit EventWindFieldResolution ( VisualizationMode &mode )
  : mode_( mode )
  {
    setWindField();
  }

  virtual void executeEvent ()
  {
    mode_.sharedInformation_->incrementWindFieldDisplayLevel();
    setWindField();
  }

private:
  void setWindField ()
  {
    const std::size_t currentLevel = mode_.sharedInformation_->windFieldDisplayLevel();

    if( currentLevel > 0 )
      mode_.visualizationWidget_.showWindField( mode_.sharedInformation_->windFieldVector_->operator[]( currentLevel-1 ).visualizer.get() );
    else
      mode_.visualizationWidget_.showWindField( nullptr );

    Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( mode_.panel_.getButton( 2 ) );
    if( button )
      button->setIcon( arrowIcon( mode_.sharedInformation_->nextWindFieldDisplayLevel() ) );    
  }

  VisualizationMode &mode_;
};



// VisualizationMode::EventPlayBack
// --------------------------------

struct VisualizationMode::EventPlayBack
: public Dune::Ash::Event
{
  explicit EventPlayBack ( VisualizationMode &mode )
  : mode_( mode )
  {}

  virtual void executeEvent () 
  {
    if( mode_.slider_.position() == 100 )
      mode_.slider_.setPosition( 0 );
    mode_.tickSamples_ = !mode_.tickSamples_;
    mode_.togglePlayPause();
  }

private:
  VisualizationMode &mode_;
};



// VisualizationMode::EventStop
// ----------------------------

struct VisualizationMode::EventStop
: public Dune::Ash::Event
{
  explicit EventStop ( VisualizationMode &mode )
  : mode_( mode )
  {}

  virtual void executeEvent () 
  {
    mode_.tickSamples_ = false;
    mode_.togglePlayPause( true );
    mode_.slider_.setPosition( 0 );
  }

private:
  VisualizationMode &mode_;
};



// VisualizationMode::EventLoop
// ----------------------------

struct VisualizationMode::EventLoop
: public Dune::Ash::Event
{
  explicit EventLoop ( VisualizationMode &mode )
  : mode_( mode )
  {}

  virtual void executeEvent () 
  {
    mode_.setLoop( !mode_.loopSamples_ );
  }

private:
  VisualizationMode &mode_;
};



// VisualizationMode::EventScreenShot
// ----------------------------------

struct VisualizationMode::EventScreenShot
: public Dune::Ash::Event
{
  typedef DiscretizationTraits::SolutionVisualizerType SolutionVisualizerType;

  explicit EventScreenShot ( VisualizationMode &mode )
  : mode_( mode )
  {}

  virtual void executeEvent () 
  {
    std::size_t frame = mode_.slider_.position();
    mode_.sharedInformation_->screenShot( frame );
  }

private:
  VisualizationMode &mode_;
};



// Implementation of VisualizationMode
// -----------------------------------

VisualizationMode::VisualizationMode ( FrontendModeStack &modeStack )
: FrontendMode( modeStack ),
  splitWidget_( 9, 7 ),
  visualizationWidget_( modeStack.mapTexture(), modeStack.icon( 0 ) ),
  panel_( modeStack, Panel::IdVisMode, 2 ),
  slider_( std::make_pair( 0, 100 ), { 0.1f, 0.1f } ),
  tickCounter_( 0 ),
  ticksPerSecond_( Dune::Fem::Parameter::getValue< double >( "ui.playback.framespersecond", 12.0 ) ),
  framesPerSecond_( Dune::Fem::Parameter::getValue< double >( "ui.screen.framespersecond", 60.0 ) ),
  sharedInformation_( modeStack.sharedInformation() ),
  pullThread_( new PullThread() ),
  tickSamples_( false ),
  backendStarted_( false )
{
  splitWidget_.setLeftChild( &visualizationWidget_ );
  splitWidget_.setRightChild( &panel_ );

  slider_.setActiveRange( std::make_pair( 0, 0 ) );
  panel_.setExtraContent( &slider_ );

  visualizationWidget_.setVolcanoPosition( sharedInformation_->volcanoPosition );

  sharedInformation_->initializeArrowList();
  sharedInformation_->initializeGrid(); 
  sharedInformation_->initializeWindField(); 
  sharedInformation_->initializeAshSamples(); 

  startPullThread();

  setLanguage();

  panel_.setButton( 3, new Dune::Ash::ButtonWidget( new EventDisplayGrid( *this ), meshIcon( sharedInformation_->nextMeshDisplayLevel() ) ) );
  panel_.setButton( 2, new Dune::Ash::ButtonWidget( new EventWindFieldResolution( *this ), arrowIcon( sharedInformation_->nextWindFieldDisplayLevel() ) ) );
  panel_.setButton( 4, new Dune::Ash::ButtonWidget( new EventPlayBack( *this ), modeStack.icon3x2( 3 ) ) );
  panel_.setButton( 5, new Dune::Ash::ButtonWidget( new EventStop( *this ), modeStack.icon3x2( 4 ) ) );

  setLoop( false );
}


VisualizationMode::~VisualizationMode ()
{
  // cancel the pull thread softly
  pullThread_->done = true;
  pullThread_->wait();
  delete pullThread_;

  sharedInformation_->resetSamples(); 

  // Kill the backend server (if it is still running)
  std::stringstream ss;
  ss << Ash::FileManager::glueScript() << " stop_adrsolver \"" << sharedInformation_->session << "\"";
  const int err = system( ss.str().c_str() );
  if( err != 0 )
  {
    std::cerr << "Error: Unable to kill the backend when exiting visualisation mode..." << std::endl;
    abort();
  }
}


void VisualizationMode::cleanUp ()
{
  // clear diffusion info and generated ash clouds
  sharedInformation_->diffusion = -1;

  if( sharedInformation_->ashSampleVector_ )
  {
    for( size_t i=0; i < sharedInformation_->ashSampleVector_->size(); ++i )
      delete (*sharedInformation_->ashSampleVector_)[ i ];

    delete sharedInformation_->ashSampleVector_;
    sharedInformation_->ashSampleVector_ = nullptr;
  }
}


void VisualizationMode::draw ()
{
  loadPendingAshSample();

  if( tickSamples_ )
  {
    for( tickCounter_ += ticksPerSecond_; tickCounter_ >= framesPerSecond_; tickCounter_ -= framesPerSecond_ )
      slider_.incPosition();
    if( slider_.position() == 100 )
    {
      if( !loopSamples_ )
      {
        tickSamples_ = false;
        togglePlayPause( true );
      }
      else
        slider_.setPosition( 0 );
    }
  }
  if( slider_.position() < sharedInformation_->ashTextures_.size() )
    visualizationWidget_.showSolution( sharedInformation_->ashTextures_[ slider_.position() ] );
  else
    visualizationWidget_.showSolution( nullptr );

  splitWidget_.draw();
}


void VisualizationMode::initializeMode ()
{
  if( !backendStarted_ )
    startBackend();
}


void VisualizationMode::setLanguage ()
{ 
  panel_.setLanguage();

  // inject events into panel_ text_ once there is access to it
  panel_.setTextEvent( std::string( "event:wind" ), new EventWindFieldResolution( *this ) );
  panel_.setTextEvent( std::string( "event:mesh" ), new EventDisplayGrid( *this ) );
  panel_.setTextEvent( std::string( "event:screenshot" ), new EventScreenShot( *this ) );
}


void VisualizationMode::togglePlayPause ( bool notToPause )
{
  Dune::Ash::Widget *widget = panel_.getButton( 4 );
  assert( widget && dynamic_cast< Dune::Ash::ButtonWidget * >( widget ) );
  Dune::Ash::ButtonWidget *button = static_cast< Dune::Ash::ButtonWidget * >( widget );
  button->setIcon( modeStack().icon3x2( tickSamples_ && !notToPause ? 3 : 2 ) );
}


void VisualizationMode::setLoop ( bool loop )
{
  loopSamples_ = loop;
  Dune::Ash::Widget *widget = panel_.getButton( 6 );
  int icon = (loopSamples_ ? 8 : 9);
  if( widget )
  {
    assert( dynamic_cast< Dune::Ash::ButtonWidget * >( widget ) );
    Dune::Ash::ButtonWidget *button = static_cast< Dune::Ash::ButtonWidget * >( widget );
    button->setIcon( modeStack().icon3x2( icon ) );
  }
  else
    panel_.setButton( 6, new Dune::Ash::ButtonWidget( new EventLoop( *this ), modeStack().icon3x2( icon ) ) );
}


void VisualizationMode::loadPendingAshSample ()
{
  // Add pending ash cloud samples... Remember that the order is relevant,
  // so we add the 'oldest' elements first
  if( !pullThread_->ashFilesToAdd.empty() && pullThread_->sampleMutex.acquire() )
  {
    // note that size may have changed between checking for emptyness and acquiring the mutex
    const std::size_t pendingAshFiles = pullThread_->ashFilesToAdd.size();
    for( std::size_t i = 0; i < pendingAshFiles; ++i )
    {
      sharedInformation_->loadAshSampleFromFile( pullThread_->ashFilesToAdd[ i ] );
      slider_.setActiveRange( std::make_pair( 0, sharedInformation_->ashTextures_.size()-1 ) );
    }
    pullThread_->ashFilesToAdd.clear();
    pullThread_->sampleMutex.release();
  }
}


void VisualizationMode::startPullThread ()
{
  assert( sharedInformation_->gridPart_ );
  pullThread_->gridPart = sharedInformation_->gridPart_;
  pullThread_->setSession( sharedInformation_->session ); 
  pullThread_->run();
}


void VisualizationMode::startBackend ()
{
  std::ofstream paramStream( sharedInformation_->session + "/adr.param" );
  sharedInformation_->writeParameters( paramStream );
  paramStream.close();

  std::stringstream ss;
  ss << Ash::FileManager::glueScript() << " start_adrsolver \"" << sharedInformation_->session << "\"";
  pullThread_->commandMutex.wait();
  const int err = system( ss.str().c_str() );
  pullThread_->commandMutex.release();
  if( err < 0 ) 
    std::cerr << "Command '" << ss.str() << "' returned an error\n";
  backendStarted_ = true;
  tickSamples_ = true;
}
