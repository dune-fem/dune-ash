// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <iomanip>

#include <dune/ash/textures/arrow.hh>
#include <dune/ash/textures/gridfunctor.hh>
#include <dune/ash/text/fontmanager.hh>
#include <dune/ash/widget/button.hh>
#include <dune/ash/widget/event.hh>

#include "diffusionmode.hh"
#include "visualizationmode.hh"


// Button Icons
// ------------

typedef Dune::Ash::Arrow Arrow;
typedef Dune::Ash::GridFunctor< Arrow > ArrowGrid;
typedef Dune::Ash::DeformFunctor< ArrowGrid > ArrowIcon;
static ArrowIcon arrowIcon( std::size_t level ) {
  return ArrowIcon(
    ArrowGrid( Arrow( 0.2 ), level, { 0.0f, 0.0f, 1.0f, 1.0f }, {0.5f, 0.5f, 0.0f} ),
    { 1.0f / 6.0f, 0.0f, 0.0f },
    { 5.0f / 6.0f, 1.0f, 0.0f } );
}

typedef Dune::Ash::RectangleOutline Mesh;
typedef Dune::Ash::GridFunctor< Mesh > MeshGrid;
typedef Dune::Ash::DeformFunctor< MeshGrid > MeshIcon;
static MeshIcon meshIcon( std::size_t level ) {
  return MeshIcon(
    MeshGrid( Mesh( { 0.3f, 0.3f, 0.3f, 1.0f } ),
      level + 1, { 0.0f, 0.0f, 0.0f, 1.0f} ), 
    { 1.0f / 6.0f, 0.0f, 0.0f },
    { 5.0f / 6.0f, 1.0f, 0.0f } );
}



// DiffusionMode::EventDisplayGrid
// -------------------------------

struct DiffusionMode::EventDisplayGrid
: public Dune::Ash::Event
{
  explicit EventDisplayGrid ( DiffusionMode &mode )
  : mode_( mode )
  {
    setMesh();
  }

  virtual void executeEvent ()
  {
    mode_.sharedInformation_->incrementMeshDisplayLevel();
    setMesh();    
  }

private:
  void setMesh ()
  {
    const std::size_t currentLevel = mode_.sharedInformation_->meshDisplayLevel();

    if( currentLevel > 0 )
    {
      mode_.visualizationWidget_.showGridPart( mode_.sharedInformation_->gridPart_, 1.0 );
    }
    else
      mode_.visualizationWidget_.showGridPart( nullptr );

    Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( mode_.panel_.getButton( 3 ) );
    if( button )
      button->setIcon( meshIcon( mode_.sharedInformation_->nextMeshDisplayLevel() ) );    
  }

  DiffusionMode &mode_;
};



// DiffusionMode::EventWindFieldResolution
// ---------------------------------------

struct DiffusionMode::EventWindFieldResolution
: public Dune::Ash::Event
{
  explicit EventWindFieldResolution ( DiffusionMode &mode )
  : mode_( mode )
  {
    setWindField();
  }

  virtual void executeEvent ()
  {
    mode_.sharedInformation_->incrementWindFieldDisplayLevel();
    setWindField();
  }

private:
  void setWindField ()
  {
    const std::size_t currentLevel = mode_.sharedInformation_->windFieldDisplayLevel();

    if( currentLevel > 0 )
      mode_.visualizationWidget_.showWindField( mode_.sharedInformation_->windFieldVector_->operator[]( currentLevel-1 ).visualizer.get() );
    else
      mode_.visualizationWidget_.showWindField( nullptr );

    Dune::Ash::ButtonWidget *button = dynamic_cast< Dune::Ash::ButtonWidget * >( mode_.panel_.getButton( 2 ) );
    if( button )
      button->setIcon( arrowIcon( mode_.sharedInformation_->nextWindFieldDisplayLevel() ) );
  }

  DiffusionMode &mode_;
};



// DiffusionMode::EventNextMode
// ----------------------------

struct DiffusionMode::EventNextMode
: Dune::Ash::Event
{
  explicit EventNextMode ( DiffusionMode &mode )
  : mode_( mode )
  {}

  void executeEvent ()
  {
    mode_.sharedInformation_->diffusion = mode_.diffusion( mode_.slider_.position() );

    FrontendModeStack &modeStack = mode_.modeStack();
    modeStack.push( new VisualizationMode( modeStack ) );
  }

private:
  DiffusionMode &mode_;
};


// Implementation of DiffusionMode
// -------------------------------

DiffusionMode::DiffusionMode ( FrontendModeStack &modeStack )
: BaseType( modeStack ),
  font_( Dune::Ash::FontManager::loadFont( Dune::Ash::FontManager::defaultFont() ) ),
  split_( 9, 7 ),
  extraSplit_( 2, 1 ),
  visualizationWidget_( modeStack.mapTexture(), modeStack.icon( 0 ) ),
  panel_( modeStack, Panel::IdDiffusionMode, 3 ),
  slider_( std::make_pair( 0, 100 ), { 0.1f, 0.1f } ),
  textBar_( 0.8f, 1.0f, 0.0f ),
  textGrid_( 3, 1 ),
  coefficient_( *font_, 5.30f, " ", Dune::Ash::AlignCenter ),
  right_( 120.0f, { 53.0f / 30.0f, 10.0f / 30.0f } ),
  left_( 120.0f, { 53.0f / 30.0f, 10.0f / 30.0f } ),
  sharedInformation_( modeStack.sharedInformation() ),
  parser_( "panel" ),
  diffMin_( Dune::Fem::Parameter::getValue< double >( "ui.diff.min", 1e-4 ) ),
  diffMax_( Dune::Fem::Parameter::getValue< double >( "ui.diff.max", 1e-2 ) )
{
  sharedInformation_->initializeArrowList();
  sharedInformation_->initializeGrid();
  sharedInformation_->initializeWindField();

  setLanguage();

  split_.setLeftChild( &visualizationWidget_ );
  split_.setRightChild( &panel_ );

  visualizationWidget_.setVolcanoPosition( sharedInformation_->volcanoPosition );

  slider_.setPosition( 50 );  

  extraSplit_.setLeftChild( &slider_ );
  extraSplit_.setRightChild( &textBar_ );

  textBar_.setChild( &textGrid_ );

  textGrid_.setChild( 0, 0, &left_ );
  textGrid_.setChild( 1, 0, &coefficient_ );
  textGrid_.setChild( 2, 0, &right_ );

  panel_.setExtraContent( &extraSplit_ );

  panel_.setButton( 3, new Dune::Ash::ButtonWidget( new EventDisplayGrid( *this ), meshIcon( sharedInformation_->nextMeshDisplayLevel() ) ) );
  panel_.setButton( 2, new Dune::Ash::ButtonWidget( new EventWindFieldResolution( *this ), arrowIcon( sharedInformation_->nextWindFieldDisplayLevel() ) ) );
  panel_.setButton( 6, new Dune::Ash::ButtonWidget( new EventNextMode( *this ), modeStack.icon3x2( 6 ) ) );
}


DiffusionMode::~DiffusionMode ()
{
  delete font_;
}


void DiffusionMode::cleanUp ()
{
  delete sharedInformation_->windFieldVector_;
  sharedInformation_->windFieldVector_ = nullptr;
}



void DiffusionMode::setLanguage ()
{
  parser_( modeStack().getLanguagePath(), textMap_ );

  std::istringstream right( textMap_[ "high" ] );
  right_.setText( modeStack().getLanguagePath(), right );

  std::istringstream left( textMap_[ "low" ] );
  left_.setText( modeStack().getLanguagePath(), left );

  panel_.setLanguage();

  // inject events into panel_ text_ once there is access to it
  panel_.setTextEvent( std::string( "event:wind" ), new EventWindFieldResolution( *this ) );
  panel_.setTextEvent( std::string( "event:mesh" ), new EventDisplayGrid( *this ) );
}


void DiffusionMode::tick ( Ticks ticks )
{ 
  std::ostringstream coeffStream;
  coeffStream  << std::scientific
               << std::setprecision( 3 )
               << diffusion( slider_.position() );  

  coefficient_.setText( coeffStream.str() );

  split_.tick( ticks );
}

