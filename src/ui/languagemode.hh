#ifndef LANGUAGEMODE_HH
#define LANGUAGEMODE_HH

#include <dune/ash/widget/shrink.hh>
#include <dune/ash/widget/grid.hh>
#include <dune/ash/widget/multiplesplit.hh>
#include <dune/ash/widget/display.hh>
#include <dune/ash/widget/floatingtext.hh>

#include "frontendmode.hh"
#include "fileparser.hh"

// ComputeWindFieldMode
// --------------------

class LanguageMode
: public FrontendMode
{
  typedef LanguageMode ThisType;
  typedef FrontendMode BaseType;

  struct EventClose;

public:
  LanguageMode ( FrontendModeStack &modeStack );
  ~LanguageMode ();

  void initializeMode ();

  void draw ();

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { shrinkWidget_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks );

  void setLanguage ();

private:
  LanguageMode ( const LanguageMode& );
  LanguageMode& operator= ( const LanguageMode& );

  FrontendMode *father_;
  Dune::Ash::ShrinkWidget<> shrinkWidget_;
  Dune::Ash::FloatingTextWidget text_;
  Dune::Ash::MultipleSplitWidget< Dune::Ash::Vertical > split_;
  Dune::Ash::GridWidget languageArray_;
  Dune::Ash::DisplayWidget placeholder_;  // filler for content

  FileParser parser_;
  FileParser::TextMap textMap_;
};

#endif // #ifndef LANGUAGEMODE_HH
