// vim: set expandtab ts=2 sw=2 sts=2: 
#ifndef GRIDVISUALIZATION_HH
#define GRIDVISUALIZATION_HH

#include <cassert>

#include <dune/ash/opengl/opengl.hh>

namespace Dune
{

  namespace Ash
  {

    // GridVisualization
    // -----------------

    struct GridVisualization
    {
      /*
       * Draw the grid. For an explanation of barycentricWarpFactor see void drawCell ( ... )
       */
      template< class GridPart >
      static void drawGrid ( const GridPart &gridPart, double barycentricWarpFactor = 1.0 )
      {
        typedef typename GridPart::template Codim< 0 >::IteratorType Iterator;
        const Iterator end = gridPart.template end< 0 >();
        for( Iterator it = gridPart.template begin< 0 >(); it != end; ++it )
          drawElement( it->geometry(), barycentricWarpFactor );
      }

      /*
       * Draw a cell. Each corner is "warped" with respect to the barycenter of the cell. Factors
       * smaller than 1. make the drawn cell smaller...
       */
      template< class Geometry >
      static void drawElement ( const Geometry &geometry, double barycentricWarpFactor = 1.0 )
      {
        const int corners = geometry.corners();
        assert( (corners == 3) || (corners == 4) );

        typename Geometry::GlobalCoordinate corner[ 4 ];
        corner[ 0 ] = geometry.corner( 0 );
        corner[ 1 ] = geometry.corner( 1 );
        corner[ 2 ] = geometry.corner( 2 );
        corner[ 3 ] = geometry.corner( 3 );

        // warp the corner coordinates towards the middle
        typename Geometry::GlobalCoordinate center( corner[ 0 ] );
        for( int i = 1; i < corners; ++i )
          center += corner[ i ];
        center *= 0.25;
        for( int i = 0; i < corners; ++i )
          corner[ i ].axpy( 1. - barycentricWarpFactor, center - corner[ i ] );

        glLineWidth( 1. );
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

        glBegin( corners == 4 ? GL_QUADS : GL_TRIANGLES );
        {
          /* 
           * We have to respect the ordering of corners - a simple for loop (i.e. corners 0,1,2,3)
           * in conjunction with GL_LINE_LOOP would not result in a square being drawn.
           */
          glVertex( corner[ 0 ] );
          glVertex( corner[ 1 ] );
          if( corners == 4 )
            glVertex( corner[ 3 ] );
          glVertex( corner[ 2 ] );
        }
        glEnd();

        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
      }
    };

  } // namespace Ash

} // namespace Dune

#endif // #ifndef GRIDVISUALIZATION_HH
