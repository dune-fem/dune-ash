// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef VISUALIZATIONWIDGET_HH
#define VISUALIZATIONWIDGET_HH

#include <dune/ash/opengl/texture.hh>

#include "sharedinfo.hh"
#include "volcanowidget.hh"

#include "../common/discretizationtraits.hh"


// VisualizationWidget
// -------------------

class VisualizationWidget
: public VolcanoDisplayWidget
{
  typedef VisualizationWidget ThisType;
  typedef VolcanoDisplayWidget BaseType;

public:
  typedef DiscretizationTraits::GridPartType GridPartType;
  typedef DiscretizationTraits::SolutionVisualizerType SolutionVisualizerType;

  typedef SharedInformation::WindFieldVisualizerType WindFieldVisualizerType;

  explicit VisualizationWidget ( const Dune::Ash::Texture &mapTexture,
                                 const Dune::Ash::BillBoard &iconVolcano );

  void draw ();

  void showGridPart ( const GridPartType *gridPart, double warpFactor = 1.0 )
  {
    gridPart_ = gridPart;
    gridWarpFactor_ = warpFactor;
  }

  void showSolution ( const Dune::Ash::Texture *solutionTexture );
  void showWindField ( const WindFieldVisualizerType *windFieldVisualizer ) { windFieldVisualizer_ = windFieldVisualizer; }

private:
  // prohibit copying and assignment
  VisualizationWidget ( const ThisType & );
  const ThisType &operator= ( const ThisType & );

  const GridPartType *gridPart_;
  const WindFieldVisualizerType *windFieldVisualizer_;
  const Dune::Ash::Texture *solutionTexture_;
  Dune::Ash::DisplayList solutionDisplayList_;
  double gridWarpFactor_;
  double vectorFieldColorScaling_;
};

#endif // #ifndef VISUALIZATIONWIDGET_HH
