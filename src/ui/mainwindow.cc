// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <iostream>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/fem/io/io.hh>

#include <dune/ash/image.hh>
#include <dune/ash/widget/empty.hh>

#include "../common/filemanager.hh"
#include "mainmode.hh"
#include "mainwindow.hh"


// Implementation of MainWindow
// ----------------------------

MainWindow::MainWindow ()
  : cursor_( nullptr ),
    modeStack_( nullptr ),
    screen_( nullptr ),
    fps_( Dune::Fem::Parameter::getValue< double >( "ui.screen.framespersecond", 60.0 ) ),
    timePerFrame_( 1000000. / fps_ ),
    shrinkWidget_( nullptr ),
    activeWidget_( &Dune::Ash::emptyWidget ),
    versionTexture_( nullptr ),
    versionList_( nullptr )
{}


MainWindow::~MainWindow ()
{
  delete versionList_;
  delete versionTexture_;
  delete modeStack_;
  delete shrinkWidget_;
  delete cursor_;
}


bool MainWindow::initializeWindow ()
{
  if ( !SDLHelper_.initializeSDL() )
    return false;

  // some SDL-specific OpenGL initializations
  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
  SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
  SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
  SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
  SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );

  const std::string screenModi[] = { "fullscreen", "window" };
  const int screenMode = Dune::Fem::Parameter::getEnum( "ui.screen.mode", screenModi, 0 );

  const SDL_VideoInfo *videoInfo = SDL_GetVideoInfo();
  assert( videoInfo );

  Uint32 flags = SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL;
  const int bitsPerPixel = Dune::Fem::Parameter::getValue< int >( "ui.screen.bitsperpixel", videoInfo->vfmt->BitsPerPixel );

  int windowWidth, windowHeight;
  if( screenMode == 0 )
  {
    flags |= SDL_FULLSCREEN;
    windowWidth = Dune::Fem::Parameter::getValue< int >( "ui.screen.width", videoInfo->current_w );
    windowHeight = Dune::Fem::Parameter::getValue< int >( "ui.screen.height", videoInfo->current_h );
  }
  else 
  {
    windowWidth = Dune::Fem::Parameter::getValue< int >( "ui.screen.width", 1024 );
    windowHeight = Dune::Fem::Parameter::getValue< int >( "ui.screen.height", 576 );
  }

  screen_ = SDL_SetVideoMode( windowWidth, windowHeight, bitsPerPixel, flags );

  if( !screen_ )
  {
    std::cerr << "Error: Unable to initialize video: " << SDL_GetError() << std::endl;
    return false;
  }

  if( Dune::Fem::Parameter::exists( "ui.screen.cursor" ) )
  {
    const std::string cursorFileName = Dune::Fem::Parameter::getValue< std::string >( "ui.screen.cursor" );
    cursor_ = new Dune::Ash::MouseCursor( Ash::FileManager::dataPath() + "/" + cursorFileName );
    Dune::Ash::setCursor( *cursor_ );
  }

  SDL_WM_SetCaption( "Simulation of an Ash Cloud Over Europe", "dune-ash" );

  // Global OpenGL setup. Setup the view so that we can draw on it like on a canvas,
  // i.e. no 3rd dimension involved (z=0 everywhere)
  glViewport( 0, 0, width(), height() ); // set the area in which OpenGL may draw
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  glOrtho( 0, 1, 1, 0, 0, 1 );
#if DUNE_ASH_GLCLEAR
  glClearColor( 1.0, 1.0, 1.0, 1.0 );
#endif // #if DUNE_ASH_GLCLEAR

  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glEnable( GL_BLEND );

  // create shrink widget
  const GLfloat shrinkWidth = std::min( 1.0f, (16.0f * GLfloat( height() )) / (9.0f * GLfloat( width() )) );
  const GLfloat shrinkHeight = std::min( 1.0f, (9.0f * GLfloat( width() )) / (16.0f * GLfloat( height() )) );
  shrinkWidget_ = new Dune::Ash::ShrinkWidget< Dune::Ash::Center >( shrinkWidth, shrinkHeight, 0.0f );
  setActiveWidget( *shrinkWidget_ );

  // create session
  std::string &session = sharedInformation_.session;
  session = Dune::Fem::executeCommand( Ash::FileManager::glueScript() + " create_session" );
  session = session.substr( 0, session.find_first_of( '\n' ) );

  // create initial mode
  modeStack_ = new FrontendModeStack( &sharedInformation_ );
  modeStack().push( new MainMode( modeStack() ) );
  shrinkWidget_->setChild( &modeStack().topWidget() );

  compileVersionList();

  return true;
}

int MainWindow::run ()
{
  Ticks lastDrawn = 0.;
  Ticks currentTick;
  const Ticks timeoutTicks = (Ticks) 1000000 * Dune::Fem::Parameter::getValue<Ticks>( "ui.timeout",  Ticks( 100ul*365ul*24ul*3600ul )  );

  Ticks eventTick = ticksSinceEpoch();

  while( true )
  {
    /*
     * Event handling
     */
    SDL_Event event;


    // SDL_WaitEvent( &event );
    if ( SDL_PollEvent( &event ) ) // only handle events that have not already been processed
    {

      eventTick = ticksSinceEpoch();
      /* obtain time of event as soon as possible and pass it to InputMode
      *
      * The event handler in the FrontendMode instance gets the microseconds since the Epoche
      * as second parameter. David: this should be sufficient for all of our timing needs, I
      * think.
      */

      // react on several events
      switch ( event.type )
      {
      case SDL_QUIT:
        modeStack().clear();
        modeStack().setExitStatus( FrontendModeStack::StatusRestart );
        break;

      case SDL_MOUSEBUTTONDOWN:
        if( event.button.button == SDL_BUTTON_LEFT )
        {
          Position x;
          x[ 0 ] = double( event.button.x ) / double( width() );
          x[ 1 ] = double( event.button.y ) / double( height() );
          activeWidget().mouseButtonDown( x, eventTick );
        }
        break;

      case SDL_MOUSEBUTTONUP:
        if( event.button.button == SDL_BUTTON_LEFT )
        {
          Position x;
          x[ 0 ] = double( event.button.x ) / double( width() );
          x[ 1 ] = double( event.button.y ) / double( height() );
          activeWidget().mouseButtonUp( x, eventTick );
        }
        break;

      case SDL_MOUSEMOTION:
        {
          const double screenWidth = width();
          const double screenHeight = height();
          Position x, dx;
          x[ 0 ] = double( event.motion.x ) / screenWidth;
          x[ 1 ] = double( event.motion.y ) / screenHeight;
          dx[ 0 ] = double( event.motion.xrel ) / screenWidth;
          dx[ 1 ] = double( event.motion.yrel ) / screenHeight;
          activeWidget().mouseMove( x, dx, eventTick );
          break;
        }

      case SDL_KEYDOWN:
        if( event.key.keysym.sym == SDLK_ESCAPE )
        {
          modeStack().clear();
          modeStack().setExitStatus( FrontendModeStack::StatusRestart );
        }
        break;
      }
    }

    currentTick = ticksSinceEpoch();

    // exit after timeoutTicks  minutes
    if( currentTick - eventTick > timeoutTicks )
      modeStack().clear();

    if( modeStack().empty() )
      break;

    // perform a tick in the active widget
    activeWidget().tick( currentTick );

    // draw the frame, but only if enough time has passed. Sleep otherwise.
    if ( (currentTick - lastDrawn) > static_cast< Ticks >( timePerFrame_ ) )
    {
      glMatrixMode( GL_MODELVIEW );
      glLoadIdentity();

      glDisable( GL_TEXTURE_2D );
      glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

#if DUNE_ASH_GLCLEAR
      glClear( GL_COLOR_BUFFER_BIT );
#else // #if DUNE_ASH_GLCLEAR
      Dune::Ash::Rectangle()();
#endif // #else // #if DUNE_ASH_GLCLEAR

      activeWidget().draw();
      if( versionList_ )
        (*versionList_)();

      SDL_GL_SwapBuffers();
      lastDrawn = currentTick;
    }
    else
      SDL_Delay( 1 );
  }
  
  // This point is after the main event loop of the main window, so its the right place
  // to call 'actionOnQuit'.
  actionOnQuit();

  // return an appropriate exit status
  return modeStack().exitStatus();
}


void MainWindow::actionOnQuit () const 
{
  // Kill the backend server (if it is still running)
  if( !sharedInformation_.session.empty() )
  {
    std::stringstream ss;
    ss << Ash::FileManager::glueScript() << " destroy_session \"" << sharedInformation_.session << "\"";
    int ret = system( ss.str().c_str() );
    if( ret != 0 )
      DUNE_THROW( Dune::Exception, "Error: Could not destroy session." );
  }
}


void MainWindow::compileVersionList ()
{
  if( Dune::Fem::Parameter::getValue< bool >( "ui.screen.showversion", false ) )
  {
    // render text using default font
    const Dune::Ash::TrueTypeFont *font
      = Dune::Ash::FontManager::loadFont( Dune::Ash::FontManager::defaultFont() );

    versionTexture_ = new Dune::Ash::Texture();
    const GLfloat aspect = font->render( "v. " DUNE_ASH_VERSION, *versionTexture_ );
    delete font;

    // calculate coordinates for version text
    Dune::FieldVector< GLfloat, 3 > upperLeft, lowerRight;
    lowerRight[ 0 ] = (GLfloat( 9 ) / GLfloat( 16 )) * (GLfloat( 1079 ) / GLfloat( 1080 ));
    lowerRight[ 1 ] = GLfloat( 1079 ) / GLfloat( 1080 );
    lowerRight[ 2 ] = 1.0f;
    upperLeft[ 0 ] = lowerRight[ 0 ] - aspect*(GLfloat( 24 ) / GLfloat( 1080 ));
    upperLeft[ 1 ] = lowerRight[ 1 ] - GLfloat( 24 ) / GLfloat( 1080 );
    upperLeft[ 2 ] = 1.0f;

    // create display list
    typedef typename Dune::Ash::BillBoard::Color Color;
    Dune::Ash::BillBoard drawer( upperLeft, lowerRight, *versionTexture_, Color{ 0, 0, 0, 1 } );
    versionList_ = new Dune::Ash::DisplayList( drawer );
  }
}
