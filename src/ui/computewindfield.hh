#ifndef COMPUTEWINDFIELD_HH
#define COMPUTEWINDFIELD_HH

#include <dune/ash/parallel/thread.hh>
#include <dune/ash/widget/event.hh>
#include <dune/ash/widget/grid.hh>
#include <dune/ash/widget/shrink.hh>
#include <dune/ash/widget/floatingtext.hh>

#include "frontendmode.hh"
#include "fileparser.hh"

// ComputeWindFieldThread
// ----------------------

struct ComputeWindFieldThread
: public Dune::Ash::Thread
{
  explicit ComputeWindFieldThread ( const std::string &session );

protected:
  void main ();

private:
  std::string command_;
};



// ComputeWindFieldMode
// --------------------

class ComputeWindFieldMode
: public FrontendMode
{
  typedef ComputeWindFieldMode ThisType;
  typedef FrontendMode BaseType;

public:
  ComputeWindFieldMode ( FrontendModeStack &modeStack, Dune::Ash::Event *eventDone = nullptr );
  ~ComputeWindFieldMode ();

  void initializeMode ();

  void draw ();

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { shrinkWidget_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks );

  void setLanguage ();

private:
  ComputeWindFieldMode ( const ComputeWindFieldMode& );
  ComputeWindFieldMode& operator= ( const ComputeWindFieldMode& );

  ComputeWindFieldThread thread_;
  Dune::Ash::Event *eventDone_;
  FrontendMode *father_;
  Dune::Ash::ShrinkWidget<> shrinkWidget_;
  Dune::Ash::GridWidget gridWidget_;
  Dune::Ash::FloatingTextWidget text_;

  FileParser parser_;
  FileParser::TextMap textMap_;
};

#endif // #ifndef COMPUTEWINDFIELD_HH
