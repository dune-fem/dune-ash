// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef TITLEWIDGET_HH
#define TITLEWIDGET_HH

#include <dune/ash/widget/button.hh>
#include <dune/ash/widget/split.hh>
#include <dune/ash/widget/shrink.hh>
#include <dune/ash/widget/grid.hh>
#include <dune/ash/widget/display.hh>

#include "frontendmode.hh"



// TitleWidget
// -----------

class TitleWidget
: public Dune::Ash::Widget
{
  struct EventExit;
  struct EventInfo;
  struct EventLanguage;

public:
  TitleWidget ( FrontendModeStack &modeStack );
  ~TitleWidget ();

  void draw () { split_.draw(); }

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { split_.mouseMove(x, dx, ticks); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { split_.mouseButtonDown(x, ticks); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { split_.mouseButtonUp(x, ticks); }

private:
  Dune::Ash::SplitWidget< Dune::Ash::Horizontal >  split_;
  Dune::Ash::Texture titleTexture_;
  Dune::Ash::DisplayWidget title_;
  Dune::Ash::GridWidget buttonArray_;
  Dune::Ash::ShrinkWidget< Dune::Ash::TopRight > controlArray_;
  Dune::Ash::ButtonWidget buttonInfo_, buttonLanguage_, buttonExit_;
};

#endif // #ifndef TITLEWIDGET_HH
