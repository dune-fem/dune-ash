// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <string>
#include <iostream>

#include <SDL.h>
#include <SDL_opengl.h>
#include <sys/time.h>

#include <dune/ash/io.hh>
#include <dune/ash/widget/event.hh>

#include "../common/filemanager.hh"
#include "inputmode.hh"
#include "diffusionmode.hh"
#include "computewindfield.hh"
#include "volcanomode.hh"



// InputMode::StartSimulation
// --------------------------

struct InputMode::ComputeWindField
: public Dune::Ash::Event
{
  explicit ComputeWindField ( InputMode &inputMode )
  : inputMode_( &inputMode )
  {}

  void executeEvent () { inputMode_->startSimulation(); }

private:
  InputMode *inputMode_;
};




// InputMode::ClearMousePath
// -------------------------

struct InputMode::ClearMousePath  
: Dune::Ash::Event
{
  explicit ClearMousePath ( InputWidget &inputWidget )
  : inputWidget_( &inputWidget )
  {}

  void executeEvent () { inputWidget_->clearMousePaths(); }

private:
  InputWidget *inputWidget_;
};



// InputMode::WindFieldComputed
// ----------------------------

struct InputMode::WindFieldComputed
: Dune::Ash::Event
{
  explicit WindFieldComputed ( InputMode &mode )
  : mode_( mode )
  {}

  void executeEvent ()
  {
    mode_.sharedInformation_->xdr_vector_field_valid_ = true;
    mode_.modeStack().push( new DiffusionMode( mode_.modeStack() ) );
  }

private:
  InputMode &mode_;
};



// Implementation of InputMode
// ---------------------------

InputMode::InputMode ( FrontendModeStack &modeStack )
: FrontendMode( modeStack ),
  splitWidget_( 9, 7 ),
  inputWidget_( modeStack.mapTexture(), modeStack.icon( 0 ) ),
  panel_( modeStack, Panel::IdInputMode ),
  diffusion_coef_( 1.0 ),
  perturbation_coef_( 0.0 ),
  field_written_( false ),
  sharedInformation_( modeStack.sharedInformation() )
{
  panel_.setTextEvent( std::string( "event:clear" ), new ClearMousePath( inputWidget_ ) );  

  splitWidget_.setLeftChild( &inputWidget_ );
  splitWidget_.setRightChild( &panel_ );

  inputWidget_.setVolcanoPosition( sharedInformation_->volcanoPosition );

  panel_.setButton( 2, new Dune::Ash::ButtonWidget( new ClearMousePath( inputWidget_ ), modeStack.icon3x2( 13 ) ) );
  panel_.setButton( 6, new Dune::Ash::ButtonWidget( new ComputeWindField( *this ), modeStack.icon3x2( 6 ) ) );
}


InputMode::~InputMode () {}


void InputMode::cleanUp ()
{
  sharedInformation_->xdr_vector_field_valid_ = false;
}


void InputMode::setLanguage ()
{ 
  panel_.setLanguage();

  // inject events into panel_ text_ once there is access to it
  panel_.setTextEvent( std::string( "event:clear" ), new ClearMousePath( inputWidget_ ) );
  
}

void InputMode::startSimulation ()
{
  std::list< InputWidget::MousePath > mousePaths;
  inputWidget_.getMousePaths( mousePaths );
  writeField( mousePaths, sharedInformation_->session + "/mousepaths", true );

  modeStack().push( new ComputeWindFieldMode( modeStack(), new WindFieldComputed( *this ) ) );
}
