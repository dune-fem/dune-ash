// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <iostream>

#include <dune/common/exceptions.hh>

#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/mpimanager.hh>

#include "../common/filemanager.hh"
#include "mainwindow.hh"


// main
// ----

int main ( int argc, char** argv )
try
{
  Dune::Fem::MPIManager::initialize( argc, argv );
  Dune::Fem::Parameter::append( argc, argv );

  Ash::FileManager::appendParameterFile( "general.param" );
  Ash::FileManager::appendParameterFile( "ui.param" );
  Ash::FileManager::appendParameterFile( "windfield.param" );

  MainWindow mainWindow;

  if( !mainWindow.initializeWindow() )
    return 0x10;

  return mainWindow.run();
}
catch( const Dune::Exception &e )
{
  std::cerr << e << std::endl;
  return 0x11;
}
catch( ... )
{
  std::cerr << "Internal error (unhandled expection)." << std::endl;
  return 0x12;
}
