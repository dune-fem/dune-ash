// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <dune/fem/space/common/restrictprolongfunction.hh>
#include <dune/fem/io/io.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/fem/writerutilities.hh>

#include <dune/ash/glue/gridpart.hh>
#include <dune/ash/glue/l2projection.hh>
#include <dune/ash/textures/arrow.hh>
#include <dune/ash/textures/linearalphamapper.hh>
//#include <dune/ash/textures/cloudcolormapper.hh>

#include "sharedinfo.hh"


// Implementation of SharedInformation
// -----------------------------------

SharedInformation::SharedInformation()
: session( "" ),
  volcanoPosition{ 0.16, 0.12 },
  diffusion( -1. ),
  xdr_vector_field_valid_( false ),
  gridPtr_( Ash::FileManager::macroGridFile() ),
  gridPart_( nullptr ),
  windFieldGrid_( gridPtr_->domain(), Dune::Fem::Parameter::getValue< WindFieldGridType::MultiIndex >( "ui.windfield.resolution" ) ),
  windFieldVector_( nullptr ),
  ashSampleVector_( nullptr ),
  arrowList_( nullptr ),
  maxConcentration_( Dune::Fem::Parameter::getValue< double >( "ui.concentration.max", 0.1 ) ),
  windFieldDisplayLevel_( 0 ),
  meshDisplayLevel_( 0 )
{
  const int windFieldGridLevels = Dune::Fem::Parameter::getValue< int >( "ui.windfield.levels", 2 );
  windFieldGrid_.globalRefine( windFieldGridLevels-1 );
}


SharedInformation::~SharedInformation()
{
  // delete textures
  for( AshTextureVectorType::iterator it = ashTextures_.begin(); it != ashTextures_.end(); ++it )
    delete *it;

  delete windFieldVector_;

  if( ashSampleVector_ )
  {
    for ( size_t i=0; i < ashSampleVector_->size(); ++i )
      delete (*ashSampleVector_)[ i ];
  }

  delete gridPart_;
  delete arrowList_;
}


void SharedInformation::initializeArrowList ()
{
  if ( arrowList_ )
    return;

  arrowList_ = new Dune::Ash::DisplayList( Dune::Ash::Arrow() );
}


void SharedInformation::initializeAshSamples () 
{
  if( ashSampleVector_ )
    return;

  ashSampleVector_ = new AshSampleVectorType();
  ashSampleVector_->clear();
}


void SharedInformation::initializeWindField () 
{
  if( windFieldVector_ )
    return;

  initializeGrid();
  if( xdr_vector_field_valid_ )
  {
    DiscretizationTraits::WindFieldSpaceType windFieldSpace( *gridPart_ );
    DiscretizationTraits::WindFieldType windField( "wind field", windFieldSpace );

    // read wind field from file
    Dune::Fem::SerialOutputMap< DiscretizationTraits::WindFieldType > map( windField );
    map.read( session, "windfield.xdr" );

    windFieldVector_ = new WindFieldVectorType();
    const int maxLevel = windFieldGrid_.maxLevel();
    windFieldVector_->resize( maxLevel + 1 );
    for( int level = 0; level <= maxLevel; ++level )
      (*windFieldVector_)[ level ] = WindFieldLevelContext( windFieldGrid_, level );

    // project wind field onto finest level
    typedef Dune::Ash::GridPartGlue< GridPartType, WindFieldGridPartType > Glue;
    Glue glue( *gridPart_, *(*windFieldVector_)[ maxLevel ].gridPart );
    Dune::Ash::GluedL2Projection< Glue > projection( glue );
    projection( windField, *(*windFieldVector_)[ maxLevel ].dFunction );

    // restrict wind field to coarser levels
    Dune::Fem::RestrictFunction< Dune::Fem::ConstantLocalRestrictProlong< WindFieldSpaceType > > restrictFunction;
    for( int level = maxLevel; level > 0; --level )
      restrictFunction( *(*windFieldVector_)[ level ].dFunction, *(*windFieldVector_)[ level-1 ].dFunction );

    // create wind field visualizers
    for( int level = 0; level <= maxLevel; ++level )
      (*windFieldVector_)[ level ].visualizer = Dune::make_shared< WindFieldVisualizerType >( *arrowList_, *(*windFieldVector_)[ level ].dFunction );

    windFieldDisplayLevel_ = 1;
  }
}

std::size_t SharedInformation::nextWindFieldDisplayLevel()
{
  if( windFieldVector_ )
    return (windFieldDisplayLevel_ + 1) % (windFieldVector_->size() + 1);
  else
    return 0;
}

void SharedInformation::initializeGrid ()
{
  if( gridPart_ )
    return;

  const int refinements = Dune::Fem::Parameter::getValue< int >( "ash.refinements", 0 );
  GridType &grid = *gridPtr_;
  for( int i = 0; i < refinements; ++i )
    Dune::Fem::GlobalRefine::apply( grid, 1 );
  gridPart_ = new GridPartType( grid, grid.maxLevel() );

  meshDisplayLevel_ = 0;
}

std::size_t SharedInformation::nextMeshDisplayLevel()
{
  if( gridPart_ )
    return 1 - meshDisplayLevel_;
  else
    return 0;
}

void SharedInformation::loadAshSampleFromFile ( SolutionVisualizerType *visualizer )
{
  // append it to ashSampleVector_
  ashSampleVector_->push_back( visualizer );

  // generate the texture
  typedef Dune::Ash::LinearAlphaMapper< double > ColorMapper;
  //typedef Dune::Ash::CloudColorMapper< double > ColorMapper;
  const ColorMapper colorMapper( 0.0, maxConcentration_ );
  Dune::Ash::Texture *texture = new Dune::Ash::Texture();
  visualizer->loadTexture( colorMapper, *texture );
  ashTextures_.push_back( texture );
}

void SharedInformation::screenShot ( std::size_t frame ) const
{
  if( !ashSampleVector_ || (frame >= ashSampleVector_->size()) )
  {
    std::cerr << "Screen shot: No frame selected." << std::endl;
    return;
  }

  if( !Dune::Fem::Parameter::exists( "ui.screenshot.filename" ) )
  {
    std::cerr << "Screen shot: No file name specified in parameter file." << std::endl;
    return;
  }

  const std::string fileName
    = Dune::Fem::Parameter::getValue< std::string >( "ui.screenshot.filename" );
  try
  {
    typedef Dune::Ash::LinearAlphaMapper< double > ColorMapper;
    //typedef Dune::Ash::CloudColorMapper< double > ColorMapper;
    const ColorMapper colorMapper( 0.0, maxConcentration_ );
    (*ashSampleVector_)[ frame ]->saveBitmap( colorMapper, fileName );
    std::cerr << "Screen shot: Saved as '" << fileName << "'." << std::endl;
  }
  catch( const Dune::IOError &error )
  {
    std::cerr << "Screen shot: Error writing '" << fileName << "'." << std::endl;
  }
}


void SharedInformation::resetSamples ()
{
  for( AshTextureVectorType::iterator it = ashTextures_.begin(); it != ashTextures_.end(); ++it )
    delete *it;
  ashTextures_.clear();

  if( ashSampleVector_ )
  {
    for ( size_t i=0; i < ashSampleVector_->size(); ++i )
      delete (*ashSampleVector_)[ i ];

    delete ashSampleVector_;
    ashSampleVector_ = nullptr;
  }
}


void SharedInformation::reset ()
{
  // bump all generated info
  volcanoPosition = { 0.16, 0.12 };

  delete windFieldVector_;
  windFieldVector_ = nullptr;

  diffusion = -1.;
  windFieldDisplayLevel_ = 0;
  meshDisplayLevel_ = 0;

  for( AshTextureVectorType::iterator it = ashTextures_.begin(); it != ashTextures_.end(); ++it )
    delete *it;
  ashTextures_.clear();

  if( ashSampleVector_ )
  {
    for ( size_t i=0; i < ashSampleVector_->size(); ++i )
      delete (*ashSampleVector_)[ i ];

    delete ashSampleVector_;
    ashSampleVector_ = nullptr;
  }
}
