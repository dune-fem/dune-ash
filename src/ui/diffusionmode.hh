// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef DIFFUSIONMODE_HH
#define DIFFUSIONMODE_HH

#include <dune/ash/widget/multiplesplit.hh>
#include <dune/ash/widget/slider.hh>
#include <dune/ash/widget/shrink.hh>
#include <dune/ash/widget/grid.hh>
#include <dune/ash/widget/text.hh>
#include <dune/ash/widget/floatingtext.hh>

#include "panel.hh"
#include "frontendmode.hh"
#include "visualizationwidget.hh"
#include "sharedinfo.hh"
#include "fileparser.hh"

// DiffusionMode
// -------------

class DiffusionMode
: public FrontendMode
{
  typedef DiffusionMode ThisType;
  typedef FrontendMode BaseType;

  double diffusion ( std::size_t sliderPos ) const  
  {
    double a = std::log( diffMax_ / diffMin_ ) / 100 ;
    return diffMin_ * std::exp( a * sliderPos );
  }

  struct EventNextMode;
  struct EventWindFieldResolution;
  struct EventDisplayGrid;

public:
  DiffusionMode ( FrontendModeStack &modeStack );
  ~DiffusionMode ();

  void cleanUp ();

  void draw () { split_.draw(); }

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { split_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { split_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { split_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks );

  void setLanguage ();

private:
  DiffusionMode ( const ThisType & );
  const ThisType& operator= ( const ThisType & );

  Dune::Ash::TrueTypeFont *font_;
  Dune::Ash::SplitWidget< Dune::Ash::Horizontal > split_;
  Dune::Ash::SplitWidget< Dune::Ash::Vertical > extraSplit_;
  VisualizationWidget visualizationWidget_;
  Panel panel_;
  Dune::Ash::SliderWidget< Dune::Ash::Horizontal > slider_;
  Dune::Ash::ShrinkWidget<> textBar_;
  Dune::Ash::GridWidget textGrid_;
  Dune::Ash::TextWidget coefficient_;
  Dune::Ash::FloatingTextWidget right_, left_;

  SharedInformation *sharedInformation_;
  FileParser parser_;
  FileParser::TextMap textMap_;

  double diffMin_; 
  double diffMax_; 
};

#endif // #ifndef DIFFUSIONMODE_HH
