#ifndef EXITMODE_HH
#define EXITMODE_HH

#include <dune/ash/widget/shrink.hh>
#include <dune/ash/widget/grid.hh>
#include <dune/ash/widget/split.hh>

#include "frontendmode.hh"

// ComputeWindFieldMode
// --------------------

class ExitMode
: public FrontendMode
{
  typedef ExitMode ThisType;
  typedef FrontendMode BaseType;

  struct EventRestart;
  struct EventShutdown;
  struct EventClose;

public:
  ExitMode ( FrontendModeStack &modeStack );
  ~ExitMode ();

  void initializeMode ();

  void draw ();

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { shrinkWidget_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { shrinkWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks );

private:
  ExitMode ( const ExitMode& );
  ExitMode& operator= ( const ExitMode& );

  FrontendMode *father_;
  Dune::Ash::ShrinkWidget<> shrinkWidget_;
  Dune::Ash::SplitWidget< Dune::Ash::Vertical > split_;
  Dune::Ash::GridWidget controlArray_;
  Dune::Ash::GridWidget buttonArray_;
};

#endif // #ifndef EXITMODE_HH
