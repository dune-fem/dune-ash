// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef VISUALIZATIONMODE_HH
#define VISUALIZATIONMODE_HH

#include <vector>
#include <utility>
#include <cmath>

#include <GL/gl.h>

#include <dune/ash/opengl/displaylist.hh>
#include <dune/ash/parallel/thread.hh>
#include <dune/ash/widget/slider.hh>
#include <dune/ash/widget/split.hh>

#include "frontendmode.hh"
#include "sharedinfo.hh"
#include "panel.hh"
#include "visualizationwidget.hh"


/*
 * class VisualizationMode
 */
class VisualizationMode
: public FrontendMode
{
  typedef VisualizationMode ThisType;
  typedef FrontendMode BaseType;

  struct EventDisplayGrid;
  struct EventWindFieldResolution;
  struct EventPlayBack;
  struct EventStop;
  struct EventLoop;
  struct EventScreenShot;

  struct PullThread;

public:
  VisualizationMode ( FrontendModeStack &modeStack );

  ~VisualizationMode ();

  void initializeMode ();
  void cleanUp ();

  void draw ();

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { splitWidget_.mouseMove( x, dx, ticks ); }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonDown( x, ticks ); }
  void mouseButtonUp ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks ) { splitWidget_.tick( ticks );  }

  void setLanguage ();

private:
  // forbidden methods
  VisualizationMode ( const VisualizationMode& );
  VisualizationMode& operator= ( const VisualizationMode& );

  void startPullThread ();

  void startBackend ();

  void loadPendingAshSample ();

  void togglePlayPause ( bool notToPause = false );
  void setLoop ( bool loop );

  /*
   * private data members
   */
  Dune::Ash::SplitWidget< Dune::Ash::Horizontal > splitWidget_;
  VisualizationWidget visualizationWidget_;
  Panel panel_;
  Dune::Ash::SliderWidget< Dune::Ash::Horizontal > slider_;

  double tickCounter_, ticksPerSecond_, framesPerSecond_;

  SharedInformation *sharedInformation_;

  PullThread *pullThread_;

  std::size_t currentAshSample_;
  bool tickSamples_, loopSamples_;

  bool backendStarted_;
};

#endif // VISUALIZATIONMODE_HH
