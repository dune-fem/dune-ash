// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include <dune/fem/io/parameter.hh>

#include <dune/ash/image.hh>
#include <dune/ash/io.hh>
#include <dune/ash/widget/text.hh>

#include "../common/filemanager.hh"
#include "computewindfield.hh"



// ComputeWindFieldThread
// ----------------------

ComputeWindFieldThread::ComputeWindFieldThread ( const std::string &session )
: command_( Ash::FileManager::glueScript() + " compute_windfield \"" + session + "\"" )
{}


void ComputeWindFieldThread::main ()
{
  const int returnValue = system( command_.c_str() );
  if( returnValue != 0 )
  {
    std::cout << "Unable to execute '" << command_ << "'." << std::endl;
    abort();
  }
}



// Implementation of ComputeWindFieldMode
// --------------------------------------

ComputeWindFieldMode::ComputeWindFieldMode ( FrontendModeStack &modeStack, Dune::Ash::Event *eventDone )
: FrontendMode( modeStack ),
  thread_( modeStack.sharedInformation()->session ),
  eventDone_( eventDone ),
  father_( nullptr ),
  shrinkWidget_( 12.0f / 32.0f , 5.0f / 18.0f ),
  gridWidget_( 1, 5 ),
  text_( 120.0f, { 6.0f, 0.5f } ),
  parser_( "panel" )
{
  setLanguage();
  
  shrinkWidget_.setChild( &gridWidget_ );

  gridWidget_.setChild( 0, 2, &text_ );
}


ComputeWindFieldMode::~ComputeWindFieldMode ()
{}

void ComputeWindFieldMode::setLanguage()
{
  parser_( modeStack().getLanguagePath(), textMap_ );
  std::istringstream input( textMap_[ "computing" ] );
  text_.setText( modeStack().getLanguagePath(), input );
}

void ComputeWindFieldMode::initializeMode ()
{
  father_ = modeStack().father( this );
  thread_.run();
}


void ComputeWindFieldMode::draw ()
{
  if( father_ )
    father_->draw();
  shrinkWidget_.draw();
}


void ComputeWindFieldMode::tick ( Ticks ticks )
{
  if( father_ )
    father_->tick( ticks );

  shrinkWidget_.tick( ticks );

  if( thread_.done() )
  {
    modeStack().pop();
    if( eventDone_ )
      eventDone_->executeEvent();
  }
}
