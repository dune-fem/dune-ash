// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef MAINMODE_HH
#define MAINMODE_HH

#include <dune/ash/widget/split.hh>
#include <dune/ash/widget/floatingtext.hh> 

#include "frontendmode.hh"
#include "panel.hh"
#include "sharedinfo.hh"


// MainMode
// --------

class MainMode
: public FrontendMode
{
  typedef MainMode ThisType;
  typedef FrontendMode BaseType;

  typedef FrontendModeStack::InfoMap InfoMap;
  typedef InfoMap::const_iterator InfoIterator;

  struct EventNextMode;
  struct EventLink;

public:
  MainMode ( FrontendModeStack &modeStack );
  ~MainMode ();

  void draw ();

  void mouseMove ( const Position &x, const Position &dx, Ticks ticks ) { splitWidget_.mouseMove( x, dx, ticks );  }

  void mouseButtonDown ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonDown( x, ticks ); }

  void mouseButtonUp ( const Position &x, Ticks ticks ) { splitWidget_.mouseButtonUp( x, ticks ); }

  void tick ( Ticks ticks ) { splitWidget_.tick( ticks ); }

  void setLanguage ();

private:
  const InfoMap &infoMap () const { return modeStack().infoMap(); }

  SharedInformation *sharedInformation_;
  Dune::Ash::SplitWidget< Dune::Ash::Horizontal > splitWidget_;
  Dune::Ash::FloatingTextWidget text_;
  Panel panel_;
};

#endif // MAINMODE_HH
