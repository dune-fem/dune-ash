// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#include "volcanowidget.hh"



// Implementation of VolcanoWidget
// -------------------------------

VolcanoDisplayWidget::VolcanoDisplayWidget ( const Dune::Ash::Texture &mapTexture,
                                             const Dune::Ash::BillBoard &iconVolcano )
: BaseType( Dune::Ash::BillBoard( mapTexture ) ),
  iconVolcano_( iconVolcano ),
  posVolcano_{ 0.5f, 0.5f }
{}


void VolcanoDisplayWidget::draw ()
{
  BaseType::draw();

  glMatrixMode( GL_MODELVIEW );
  glPushMatrix();

  Dune::Ash::glTranslate( volcanoPosition() );
  Dune::Ash::glScale( 0.04f, 0.04f, 1.0f );
  Dune::Ash::glTranslate( -0.5f, -0.5f, 0.0f );

  iconVolcano_();

  glPopMatrix();
}



// Implementation of VolcanoWidget
// -------------------------------

VolcanoWidget::VolcanoWidget ( const Dune::Ash::Texture &mapTexture,
                               const Dune::Ash::BillBoard &iconVolcano )
: BaseType( mapTexture, iconVolcano ),
  mouseButtonDown_( false )
{}


void VolcanoWidget::mouseButtonDown ( const Position &x, Ticks ticks )
{
  mouseButtonDown_ = true;
  setVolcanoPosition( x );
}


void VolcanoWidget::mouseButtonUp ( const Position &x, Ticks ticks )
{
  if( mouseButtonDown_ )
    setVolcanoPosition( x );
  mouseButtonDown_ = false;
}


void VolcanoWidget::mouseMove ( const Position &x, const Position &dx, Ticks ticks )
{
  if( mouseButtonDown_ )
    setVolcanoPosition( x );
}
