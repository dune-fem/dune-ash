// vim: set expandtab ts=2 sw=2 sts=2:
#ifndef ASH_COMMON_FILEMANAGER_HH
#define ASH_COMMON_FILEMANAGER_HH

#include <string>

#include <dune/fem/io/io.hh>
#include <dune/fem/io/parameter.hh>

namespace Ash
{

  /*
   * This class manages all data files requiered by Ash. Our primary use for this class are
   * images and parameter files. All paths to data files can be set in a file called
   * 'dune-ash.paths.params'. This file is lookes for in "." and in some other generic
   * directories...
   */
  struct FileManager
  {
    // Returns the absolute path of 'basename' in the image directory. If the image directory
    // is e.g. /foo/bar and the image is by_buzz.png, this returns /foo/bar/by_buzz.png...
    static std::string absoluteImageName ( const std::string &basename )
    {
      return dataPath() + "/images/" + basename;
    }

    static std::string absoluteTextureName ( const std::string &basename )
    {
      return dataPath() + "/textures/" + basename;
    }

    // appends the parameter file 'paramFile' from the parameter file directory
    static void appendParameterFile ( const std::string &fileName );

    // Returns the absolute path of the macro grid file.
    static std::string macroGridFile ();

    static std::string dataPath ();
    static std::string glueScript ();
  };

} // namespace Ash

#endif // #ifndef ASH_COMMON_FILEMANAGER_HH
