#ifndef DISCRETIZATIONTRAITS_HH
#define DISCRETIZATIONTRAITS_HH

#include <dune/grid/spgrid.hh>

#include <dune/fem/gridpart/levelgridpart.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/discontinuousgalerkin.hh>
#include <dune/fem/space/lagrange.hh>

#include <dune/ash/visualizer/discretefunction.hh>

struct DiscretizationTraits
{
  typedef Dune::SPGrid< double, 2 > GridType;

  typedef Dune::Fem::LevelGridPart< GridType > GridPartType;

  typedef Dune::Fem::DiscontinuousGalerkinSpace< Dune::Fem::FunctionSpace< double, double, 2, 1 >, GridPartType, 1 > SolutionSpaceType;
  typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< SolutionSpaceType > SolutionType;
//  typedef Dune::Fem::AdaptiveDiscreteFunction< SolutionSpaceType > SolutionType;

  typedef Dune::Fem::LagrangeDiscreteFunctionSpace< Dune::Fem::FunctionSpace< double, double, 2, 2 >, GridPartType, 2 > WindFieldSpaceType;
  typedef Dune::Fem::AdaptiveDiscreteFunction< WindFieldSpaceType > WindFieldType;

  typedef Dune::Ash::DiscreteFunctionVisualizer< SolutionType > SolutionVisualizerType;
};

#endif // #ifndef DISCRETIZATIONTRAITS_HH
