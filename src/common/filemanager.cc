// vim: set expandtab ts=2 sw=2 sts=2:
#include <config.h>

#ifndef SYSCONFDIR
#error "Missing preprocessor define: SYSCONFDIR"
#endif // #ifndef SYSCONFDIR

#ifndef BINDIR
#error "Missing preprocessor define: BINDIR"
#endif // #infdef BINDIR

#include "filemanager.hh"

namespace Ash
{

  // Static Variables
  // ----------------

  static std::string glueScript;



  // Implementation of FileManager
  // -----------------------------

  void FileManager::appendParameterFile ( const std::string &fileName )
  {
    std::vector< std::string > candidates;
    if( !Dune::Fem::Parameter::exists( "ash.configpath" ) )
    {
      candidates.push_back( std::string( "." ) );
      candidates.push_back( std::string( "./config" ) );
      candidates.push_back( std::string( SYSCONFDIR ) );
    }
    else
      candidates.push_back( Dune::Fem::Parameter::getValue< std::string >( "ash.configpath" ) );

    typedef std::vector< std::string >::const_iterator Iterator;
    for( Iterator it = candidates.begin(); it != candidates.end(); ++it )
    {
      std::string absFileName = *it + "/" + fileName;
      if( Dune::Fem::fileExists( absFileName ) )
        return Dune::Fem::Parameter::append( absFileName );
    }
  }


  std::string FileManager::macroGridFile ()
  {
    return dataPath() + "/" + Dune::Fem::Parameter::getValue< std::string >( "ash.macrogrid" );
  }


  std::string FileManager::dataPath ()
  {
    return Dune::Fem::Parameter::getValue< std::string >( "ash.datapath" );
  }


  std::string FileManager::glueScript ()
  {
    if( !Ash::glueScript.empty() )
      return Ash::glueScript;

    std::vector< std::string > candidates;
    if( !Dune::Fem::Parameter::exists( "ash.glue" ) )
    {
      candidates.push_back( std::string( "./dune-ash-glue" ) );
      candidates.push_back( std::string( SYSCONFDIR ) + "/dune-ash-glue" );
      candidates.push_back( std::string( BINDIR ) + "/dune-ash-glue" );
    }
    else
      candidates.push_back( Dune::Fem::Parameter::getValue< std::string >( "ash.glue" ) );

    typedef std::vector< std::string >::const_iterator Iterator;
    for( Iterator it = candidates.begin(); it != candidates.end(); ++it )
    {
      if( Dune::Fem::fileExists( *it ) )
        return (Ash::glueScript = *it);
    }

    std::cerr << "Error: Unable to locate glue script." << std::endl;
    std::cerr << "       Candidates are:" << std::endl;
    for( Iterator it = candidates.begin(); it != candidates.end(); ++it )
      std::cerr << "       " << *it << std::endl;
    abort();
  }

} // namespace Ash
